---
layout: default
styles:
    - href: "style/par-tag.css"
---

{% assign allTags =  site.notices | map: 'tags' | uniq | sort_natural %}
{% assign notices_mises_en_avant = site.notices | where: "mis_en_avant", "oui" %}

<h1><span>Notices avec le tag : <span class="tag title-tag"></span></span></h1>

{% for tag in allTags -%}

    {%- comment -%}
        Du "capture" au "assign noticeCount", l'objectif est de trouver le nombre de posts avec
        un certain tag
        Je n'ai pas trouvé mieux en liquid que de générer une chaine de caractère dans un "capture"
        avec un for et un if puis de mesurer la taille de la chaine générée
    {%- endcomment -%}

    {% capture counter -%}
        {%- for notice in site.notices -%}
            {%- if notice.tags contains tag -%}x{%- endif -%}
        {%- endfor -%}
    {%- endcapture %}

    {%- assign noticeCount = counter | strip | remove: " " | size -%}

    {% capture featuredCounter -%}
        {%- for notice in notices_mises_en_avant -%}
            {%- if notice.tags contains tag -%}x{%- endif -%}
        {%- endfor -%}
    {%- endcapture %}

    {%- assign featuredCount = featuredCounter | strip | remove: " " | size -%}

    <section data-tag="{{ tag | escape }}" hidden>
        <h2>{{ noticeCount }} résultat{% if noticeCount > 1 %}s{% endif %}</h2>

        {%- if featuredCount > 0 -%}
            <div class="mis-en-avant">
                <h3 class="mis-en-avant__titre">
                    <em class="mis-en-avant__titre__encart">
                        Utilisé par les membres de Framasoft
                    </em>
                </h3>
                <ul class="liste-notices liste-notices--mis-en-avant">
                    {%- for notice in notices_mises_en_avant -%}
                        {%- if notice.tags contains tag -%}
                            <li class="liste-notices__item">
                                <div class="card h-100">
                                    <div class="row h-100 no-gutters">
                                        <div class="d-flex align-items-center justify-content-center col-lg-3">
                                            <a class="result-link" href="{{ notice.url | relative_url }}">
                                                <img class="liste-notices__image" src="{{ notice.logo.src | relative_url }}">
                                            </a>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="card-body h-100 d-flex flex-column">
                                                <h3 class="card-title h4">
                                                    <a class="result-link" href="{{ notice.url | relative_url }}">
                                                        {{ notice.nom }}
                                                    </a>
                                                </h3>
                                                <div class="card-text">{{ notice.description_courte }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        {%- endif -%}
                    {%- endfor -%}
                </ul>
            </div>
        {%- endif -%}

        <ul class="liste-notices">
            {%- for notice in site.notices -%}
                {%- if notice.mis_en_avant != "oui" and notice.tags contains tag -%}
                    <li class="liste-notices__item">
                        <div class="card h-100">
                            <div class="row h-100 no-gutters">
                                <div class="d-flex align-items-center justify-content-center col-lg-3">
                                    <a class="result-link" href="{{ notice.url | relative_url }}">
                                        <img class="liste-notices__image" src="{{ notice.logo.src | relative_url }}">
                                    </a>
                                </div>
                                <div class="col-lg-9">
                                    <div class="card-body h-100 d-flex flex-column">
                                        <h3 class="card-title h4">
                                            <a class="result-link" href="{{ notice.url | relative_url }}">
                                                {{ notice.nom }}
                                            </a>
                                        </h3>
                                        <div class="card-text">{{ notice.description_courte }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                {%- endif -%}
            {%- endfor -%}
        </ul>
    </section>
{%- endfor -%}

<style>
    details{
        margin-bottom: 1rem;
    }

    summary > * {
        display: inline;
    }
</style>

<script>
document.addEventListener('DOMContentLoaded', e => {
    const tag = new URLSearchParams(location.search).get('tag')
    console.log('tag', tag)

    if(tag){
        const tagSection = document.querySelector(`section[data-tag="${tag}"]`)
        console.log('tagSection', tagSection)
        tagSection.removeAttribute('hidden')

        for(const el of document.querySelectorAll(`.tag`)){
            el.textContent = tag
        }
    }
})
</script>
