---
nom: "MuPDF"
date_creation: "Lundi, 17 juillet, 2017 - 18:20"
date_modification: "Jeudi, 20 mai, 2021 - 16:39"
logo:
    src: "images/logo/MuPDF.png"
site_web: "https://mupdf.com"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "English"
description_courte: "Lecteur (ou plutôt moteur) de PDF minimaliste."
createurices: ""
alternative_a: "Adobe Acrobat Reader"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bureautique"
    - "lecteur pdf"
    - "pdf"
lien_wikipedia: "https://en.wikipedia.org/wiki/MuPDF"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mupdf"
---

Sous GNU/Linux, MuPDF est une alternative légère et sérieuse aux lecteurs PDFs fournis avec les environnements lourds (Gnome, KDE...).
Sous Windows, MuPDF est une version très épurée qui permet effectivement de visualiser les documents PDF, et n'offre que quelques raccourcis claviers pour permettre son usage. Limitation : il ne permet pas de lancer une impression. Click droit sur la barre de titre, puis "About MuPDF" pour une liste des raccourcis claviers. MuPDF est embarqué au sein de SumatraPDF, il est conseillé de se tourner vers ce dernier pour un logiciel plus fonctionnel.

