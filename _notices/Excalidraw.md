---
nom: "Excalidraw"
date_creation: "lundi, 25 décembre, 2023 - 22:04"
date_modification: "lundi, 25 décembre, 2023 - 22:04"
logo:
    src: "images/logo/Excalidraw.png"
site_web: "https://excalidraw.com/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un tableau blanc interactif collaboratif pour dessiner et faire des schémas"
createurices: ""
alternative_a: ""
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "schéma"
    - "tableau interactif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"

---

Excalidraw vous permet, de façon collaborative, comme sur un tableau blanc classique, de dessiner ensemble ce que vous souhaitez.

En plus, des outils vous permettent de réaliser de petits schémas facilement, et de rajouter des images. 
