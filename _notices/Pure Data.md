---
nom: "Pure Data"
date_creation: "Samedi, 26 janvier, 2019 - 12:18"
date_modification: "Samedi, 26 janvier, 2019 - 12:18"
logo:
    src: "images/logo/Pure Data.png"
site_web: "https://puredata.info/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Système de musique et de graphisme assisté par ordinateur en temps réel."
createurices: "Miller Puckette"
alternative_a: "Max For Live"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "multimédia"
    - "musique"
    - "enregistrement"
    - "live"
    - "mao"
    - "programmation audio"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Pure_Data"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pure-data"
---

Ce logiciel complètement libre en possibilités permet de faire pleins de choses très différentes.
Il gère le MIDI et l'audio multipiste.
Nativement il peut utiliser plusieurs cartes sons, ainsi que plusieurs périphériques MIDI.
Il est évidemment compatible avec QJack.
De manière générale, il permet de créer un synthétiseur personnalisé pour un clavier MIDI, faire un patch MIDI pour utiliser d'autres logiciels de MAO grâce à QJack.
On peut aussi faire de l'enregistrement sans limite, de la lecture.. MIDI et audio.
Il peut être utilisé lors de prestations en direct...
Il gère aussi la vidéo en plus de tout ça !

