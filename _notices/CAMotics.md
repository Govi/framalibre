---
nom: "CAMotics"
date_creation: "Lundi, 2 janvier, 2017 - 07:08"
date_modification: "Lundi, 4 mars, 2019 - 01:57"
logo:
    src: "images/logo/CAMotics.png"
site_web: "http://camotics.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Un logiciel qui permet de simuler/prévisualiser le GCODE d'une fraiseuse à commande numérique."
createurices: "Joseph Coffland"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "diy"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/camotics"
---

CAMotics est un logiciel qui peut simuler l'usinage CN 3 axes. Il s'agit d'un logiciel de simulation rapide, flexible et convivial pour le bricolage et la communauté Open-Source.
La fabrication à domicile est l'une des prochaines grandes révolutions technologiques. Tout comme le PC l'était il y a 30 ans. Il y a eu des avancées majeures dans l'impression 3D de bureau, mais l'adoption des CNC de bureau a été retardé malgré la disponibilité de machines CNC bon marché. L'une des principales raisons est l'absence de logiciel de simulation Open-Source et de CAM ( logiciel de conversion du modèle 3D vers la conversion des chemins d'outils ). La simulation CAM et la simulation CN présentent des problèmes de programmation très difficiles, comme en témoignent 30 ans de travaux universitaires sur ces sujets. Alors que la simulation d'impression 3D et la génération de chemin d'outil sont beaucoup plus faciles. Un tel logiciel est essentiel à l'utilisation d'un CNC.

