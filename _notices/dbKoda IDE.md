---
nom: "dbKoda IDE"
date_creation: "Vendredi, 6 mai, 2022 - 15:57"
date_modification: "Samedi, 14 mai, 2022 - 01:14"
logo:
    src: "images/logo/dbKoda IDE.jpg"
site_web: "http://www.dbkoda.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un outil de développement et d'administration de bases de données MongoDB."
createurices: "Guy Harrison"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "développement"
    - "sql"
    - "base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dbkoda-ide"
---

dbKoda permet de développer sa base de données MongoDB depuis un éditeur moderne (une interface Électron). Qu'il s'agisse de surveiller votre utilisation du stockage ou d'écrire des requêtes d'agrégation complexes, dbKoda dispose d'une suite d'outils sans cesse croissante pour aider les débutants et les experts à tirer le meilleur parti de leur base de données.
dbKoda est développé par Soutbank Software, une société de développement de logiciels basée à Melbourne qui vise à "développer des outils de base de données pour la prochaine génération de professionnels des bases de données".

