---
nom: "Ghost Commander"
date_creation: "Jeudi, 12 avril, 2018 - 10:20"
date_modification: "Lundi, 10 mai, 2021 - 14:32"
logo:
    src: "images/logo/Ghost Commander.png"
site_web: "https://sites.google.com/site/ghostcommander1/"
plateformes:
    - "Android"
langues:
    - "English"
description_courte: "Gestionnaire de fichier à deux panneaux libre !"
createurices: "zc2"
alternative_a: "ES File Explorer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "android"
    - "explorateur de fichier"
    - "client ftp"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ghost-commander"
---

De nombreuse fonctionnalités, une bonne ergonomie et un logiciel très léger.
Un peu moche mais activement développé et performant.
De nombreuse fonctionnalité :
partage samba, FTP.
Éditeur de texte simple.
Sélection multiple facile.
Et nombreuses autres

