---
nom: "Whoogle"
date_creation: "Mercredi, 28 avril, 2021 - 14:41"
date_modification: "Samedi, 7 août, 2021 - 14:11"
logo:
    src: "images/logo/Whoogle.png"
site_web: "https://github.com/benbusby/whoogle-search"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "English"
    - "Autres langues"
description_courte: "Le méta-moteur open-source qui imite Google, sans le tracking et les publicités."
createurices: "Ben Busby"
alternative_a: "Google Search, Ecogine, Startpage"
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
    - "métamoteur de recherche"
    - "recherche web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/whoogle"
---

Whoogle est un méta-moteur open-source qui imite l'interface de Google. D'ailleurs, il possède les mêmes résultats que si on allait rechercher Google. Il est divisé en plusieurs instances comme Searx.
Ses spécificités sont :
Une interface personnalisable via le CSS
La possibilité d'afficher des résultats de recherche qui ne nécessitent pas Javascript
La possibilité de l'utiliser avec Tor Browser
Mode sombre en option
Les !bangs similaires à Duckduckgo
Si on le souhaite, redirection des liens Youtube, Twitter, Reddit et Instagram  vers des fronts-ends plus respectueuses de la vie privée et open-source.
La facilité de l'installer sur un serveur.

