---
nom: "Texmaker"
date_creation: "Vendredi, 23 décembre, 2016 - 20:12"
date_modification: "mardi, 26 décembre, 2023 - 13:22"
logo:
    src: "images/logo/Texmaker.png"
site_web: "http://www.xm1math.net/texmaker/index_fr.html"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un éditeur pour vos documents LaTeX dans un environnement de bureau polyvalent."
createurices: "Pascal Brachet"
alternative_a: "WinEdt, Bakoma Tex, Scientific Workplace"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "latex"
    - "éditeur"
    - "traitement de texte"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Texmaker"
lien_exodus: ""
identifiant_wikidata: "Q1616139"
mis_en_avant: "oui"
redirect_from: "/content/texmaker"
---

TexMaker est un éditeur LaTeX complet avec coloration syntaxique, terminal intégré, affichage du rendu PDF et de nombreux raccourcis configurables. Un panneau latéral permet l'affichage de commandes, symboles, et un mode plan est disponible. Il permet d'éditer plusieurs fichiers simultanément (mode « maître ») et dispose d'assistants à la création de documents, ou d'éléments complexes tels les tableaux et divers environnements. La compilation est facile, accessible depuis la barre d'outils ou par des touches configurées au préalable. Les erreurs et autres alertes lors de la compilation s'affichent de manière interactive et pointent vers les parties des fichiers concernés.


