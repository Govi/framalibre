---
nom: "FindDuplicates"
date_creation: "Mercredi, 1 novembre, 2017 - 17:42"
date_modification: "Lundi, 10 mai, 2021 - 14:47"
logo:
    src: "images/logo/FindDuplicates.png"
site_web: "https://gitlab.com/pdea/FindDuplicates/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Détecter et supprimer des doublons dans un ou plusieurs répertoires."
createurices: "Florent Pichot"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "gestionnaire de fichiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/findduplicates"
---

Après avoir analysé le(s) répertoire(s) spécifié(s), ce logiciel affichera une interface pour vous permettre de choisir, pour chaque doublon, quel fichier vous voulez supprimer.
Vous pouvez trier les doublons par de multiples critères.
Vous pouvez sélectionner des doublon par pattern, anti-pattern ou à la souris.
Vous pouvez exclure ou supprimer (avec confirmation) les doublons.
Vous pouvez facilement créer des fichiers de configuration et lancer le logiciel en ligne de commande.
Ce logiciel est le complément idéal à des logiciels comme Beyond Compare, KDiff, etc. pour faire le ménage dans vos répertoires.
La version 2 de ce logiciel, écrite en python3, avec interface en PyQt5, a été grandement optimisée,
Une version binaire pour Windows est fournie.
La version 1 en python 2 avec wxPython est toujours disponible sous GitLab (BR_1.0).
J'avais besoin d'un tel logiciel, il n'existait pas. Je l'ai écrit et le partage ! :-)

