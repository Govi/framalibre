---
nom: "Kisslauncher"
date_creation: "Mercredi, 22 mars, 2017 - 10:51"
date_modification: "Mercredi, 12 mai, 2021 - 16:35"
logo:
    src: "images/logo/Kisslauncher.png"
site_web: "http://kisslauncher.com/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Un lanceur alternatif pour android"
createurices: "Sandoche & Neamar & Bénilde"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "lanceur d'applications"
    - "android"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/kisslauncher"
---

Lanceur alternatif se basant sur un système de recherche : très simple d'utilisation. Disponible sur F-droid et sur le play store, tous les deux à jour. Très léger sur la batterie et sur les ressources système. En développement actif.

