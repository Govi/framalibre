---
nom: "Pixel Dungeon"
date_creation: "Lundi, 15 janvier, 2018 - 04:37"
date_modification: "Samedi, 10 mars, 2018 - 01:33"
logo:
    src: "images/logo/Pixel Dungeon.png"
site_web: "http://pixeldungeon.watabou.ru/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Pixel Dungeon est un Roguelike traditionnel, entièrement composé de graphismes en pixel-art."
createurices: "watabou"
alternative_a: "Dungeons of Dredmor"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "tour par tour"
    - "pixel"
    - "jeux de rôle"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pixel-dungeon"
---

Dans Pixel Dungeon comme dans bon nombre d'autres roguelike, les différentes salles sont générées via un système aléatoire. Choisissez votre personnage parmi les 4 héros, buvez des potions, lisez des parchemins, et affrontez les 25 étages souterrains infestés de hordes de monstres en tout genres et de boss redoutables, pour obtenir la tant convoitée Amulette de Yendor ! Autant vous prévenir, la mort vous attend dans la grande majorité de vos aventures, mais à force de persévérance, vous finirez par réussir !
Pixel Dungeon est un jeu exigeant qui vous demandera patience et persévérance. Une gestion correcte de vos ressources et de vos déplacements deviendront la clé de votre réussite. Alchimie, malédiction d'objets, potions et augmentation d'objets sont de la partie et apportent au jeu richesse et stratégie.
Des versions modifiées telles que Shattered Pixel Dungeon ou Moonshine Pixel Dungeon sont également disponibles.

