---
nom: "FreedomBox"
date_creation: "Dimanche, 10 mars, 2019 - 13:06"
date_modification: "Jeudi, 17 août, 2023 - 21:07"
logo:
    src: "images/logo/FreedomBox.png"
site_web: "https://www.freedombox.org/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un petit serveur pour auto-héberger ses données."
createurices: "Eben Moglen"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "auto-hébergement"
    - "serveur"
    - "debian"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FreedomBox"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freedombox"
---

FreedomBox est un petit serveur tournant sous Debian qui s'apparente à YunoHost. Il dispose d'une interface d'administration accessible dans un navigateur et permet d'installer diverses applications tel que MediaWiki, Privoxy, Roundcube, Radicale, Searx, Diaspora, etc.

