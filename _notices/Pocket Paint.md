---
nom: "Pocket Paint"
date_creation: "Lundi, 19 juillet, 2021 - 18:56"
date_modification: "Lundi, 19 juillet, 2021 - 19:00"
logo:
    src: "images/logo/Pocket Paint.png"
site_web: "https://github.com/Catrobat/Paintroid"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Dessinez ou retouchez vos photos sur votre smartphone !"
createurices: "Projet Catrobat"
alternative_a: "Snapseed"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "création"
    - "retouche"
    - "manipulation d'images"
    - "dessin"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.catrobat.paintroid/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pocket-paint"
---

Pocket Paint (aussi connu sous le nom de Paintroid) est un logiciel issu du projet Catrobat et codé grâce à ce langage de programmation libre.
Pocket Paint vous permet de dessiner ou de retoucher des photos de manière simple et intuitive. Il est possible de partir d'une page blanche ou de charger une image pour créer un calque par dessus puis d'enregistrer le tout comme une nouvelle image. Avec toute une batterie d'outils de création picturale, ce logiciel n'occupera qu'une toute petite place dans votre smartphone.

