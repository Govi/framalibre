---
nom: "Moodle"
date_creation: "Samedi, 7 janvier, 2017 - 19:29"
date_modification: "Mercredi, 12 mai, 2021 - 15:52"
logo:
    src: "images/logo/Moodle.jpg"
site_web: "https://moodle.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Environnement d'apprentissage multi-activités en ligne."
createurices: "Martin Dougamias"
alternative_a: "dokeos"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "cms"
    - "lms"
    - "didactique"
    - "pédagogie"
    - "forum de discussion"
    - "wiki"
    - "évaluation"
    - "exerciseur"
    - "partage de fichiers"
    - "devoirs"
    - "scorm"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Moodle"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/moodle"
---

Moodle est un LMS (Learning Management System ou environnement d'apprentissage) permettant de créér, partager, animer des ressources didactiques et de formation.
A minima on pourra l'utiliser pour partager des supports de cours (sous différents formats), mais il permet surtout de crer des activités d'apprentissage riche privilégiant les interactions entre les utilisateurs (apprenants comme formateur).
Il intègre une dizaine d'activités (quiz, leçon, forum, wiki, devoir, sondage, atelier auto-évalué entre pairs …) et format de ressources (pdf, texte, audio-visuel, image, scorm…) par défaut qui peuvent être enrichi par des modules complémentaires (notamment pour proposer des activités spécifiques à certains domaines).

