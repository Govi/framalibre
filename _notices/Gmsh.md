---
nom: "Gmsh"
date_creation: "Lundi, 9 mars, 2020 - 10:47"
date_modification: "Lundi, 10 mai, 2021 - 14:20"
logo:
    src: "images/logo/Gmsh.png"
site_web: "http://gmsh.info/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Un mailleur et solveur par éléments finis libre et simple d'utilisation."
createurices: "Christophe Geuzaine, Jean-François Remacle"
alternative_a: "Matlab"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "éléments finis"
    - "cad"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Gmsh"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gmsh"
---

Gmsh est un logiciel libre permettant de générer des maillages de domaines et de résoudre des équations aux dérivées partielles sur un domaine par éléments finis.
L'interface permet de visualiser les maillages directement. Il est ensuite possible de les exporter dans un fichier de données afin de les utiliser dans d'autres logiciels de simulation.

