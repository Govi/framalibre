---
nom: "BlogoText"
date_creation: "Vendredi, 9 février, 2018 - 07:07"
date_modification: "Lundi, 10 mai, 2021 - 13:46"
logo:
    src: "images/logo/BlogoText.png"
site_web: "https://blogotext.org"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "BlogoText : un CMS qui fonctionne sans MySQL"
createurices: "Timo van Neerden, Mickaël 'Tiger-222' Schoentgen, RemRem"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "cms"
    - "microblog"
    - "blog"
    - "création de site web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/blogotext"
---

BlogoText n'est pas seulement un moteur de blog mais propose plutôt un portail dédié à l'écriture de contenu. En effet, l'interface administrateur propose, en plus des outils traditionnels dédiés au blog, un lecteur de flux RSS, un hébergeur de fichiers et un outil permettant de partager des liens.
L'avantage de ce CMS est qu'il est très léger et sera parfait pour s'occuper de la partie blog ou actualité de votre site web. BlogoText est écrit en PHP, exploite une base de données SQLite et à seulement besoin de 2Mo d'espace disque.

