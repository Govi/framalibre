---
nom: "Lstu"
date_creation: "Mardi, 13 décembre, 2016 - 12:35"
date_modification: "jeudi, 28 décembre, 2023 - 06:07"
logo:
    src: "images/logo/Lstu.png"
site_web: "https://lstu.fr"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un raccourcisseur d'URL simple et léger."
createurices: "Luc Didry"
alternative_a: "bit.ly"
licences:
    - "Licence publique f***-en ce que vous voulez (WTFPL)"
tags:
    - "url"
    - "anonymat"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q98732134"
mis_en_avant: "oui"
redirect_from: "/content/lstu"
---

Lstu est un raccourcisseur d'URL simple, léger, rapide et respectueux des données personnelles des utilisateurs/ices : la seule information enregistrée par le serveur lors de la visite d'un lien est un compteur de visite du lien.
Lstu permet en plus de choisir le texte de son lien raccourci.


