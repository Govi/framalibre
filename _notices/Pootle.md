---
nom: "Pootle"
date_creation: "Lundi, 30 janvier, 2017 - 23:00"
date_modification: "Samedi, 8 avril, 2017 - 20:51"
logo:
    src: "images/logo/Pootle.png"
site_web: "http://pootle.translatehouse.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
langues:
    - "Autres langues"
description_courte: "Logiciel d'aide à la traduction de logiciels et d'applications."
createurices: "David Fraser"
alternative_a: "Transifex"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "traduction"
lien_wikipedia: "https://en.m.wikipedia.org/wiki/Pootle"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pootle"
---

Ce logiciel est conçu pour faciliter la vie des traducteurs d'interfaces utilisateurs. Il permet d'avoir une vue d'ensemble du projet tout en apportant des fonctionnalités spécifiques à la traduction, telles que:
La traduction automatique
l'accès ou la création de glossaires
La mémorisation des mots utilisés
Ce logiciel propose une interface web qui permet la collaboration en ligne. Le projet pousse à l'installation sur des serveurs indépendants.

