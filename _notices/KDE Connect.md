---
nom: "KDE Connect"
date_creation: "Mercredi, 22 mars, 2017 - 23:17"
date_modification: "Samedi, 25 mars, 2017 - 14:42"
logo:
    src: "images/logo/KDE Connect.png"
site_web: "https://community.kde.org/KDEConnect"
plateformes:
    - "GNU/Linux"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Reliez votre smartphone Android à votre bureau Linux."
createurices: "KDE Community"
alternative_a: "AirDroid"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "android"
    - "linux"
    - "partage de fichiers"
    - "partage"
    - "utilitaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/kde-connect"
---

KDE Connect permet de créer une connexion entre votre smartphone et votre bureau Linux, le tout sans fil.
L'application supporte entre autre le partage d’URL, l'affichage de notifications et le transfert de fichiers.
Elle offre la possibilité d'utiliser votre appareil Android pour contrôler à distance votre bureau, ainsi que vos lecteurs audio.
L'intégration est nativement liée à l'environnement de bureau KDE, mais il est possible d'incorporer l'application à d'autres environnements comme Gnome.
L'application nécessite une connexion locale entre les deux appareils.

