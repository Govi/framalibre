---
nom: "MKVToolNix"
date_creation: "Samedi, 8 avril, 2017 - 15:43"
date_modification: "Lundi, 10 mai, 2021 - 14:39"
logo:
    src: "images/logo/MKVToolNix.png"
site_web: "https://www.bunkus.org/blog/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Manipulez des fichiers vidéos à la norme mkv."
createurices: "Moritz Bunkus"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "vidéo"
    - "audio"
    - "format"
    - "sous-titres"
lien_wikipedia: "https://en.wikipedia.org/wiki/MKVToolNix"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/mkvtoolnix"
---

MKVToolNix est une suite logicielle pour manipuler le format vidéo mkv. La norme mkv permet de multiplexer un grand nombre de pistes vidéos, audios, sous-titres, chapitres, polices de caractères, etc. Elle supporte la grande majorité des codecs audio/vidéo, normes de sous-titres. Elle incorpore un système de tags très complet et précis, pour ajouter des informations à chaque piste ou au fichier entier.
La suite comporte :
MKVMerge : programme console de multiplexage
MKVToolNix Gui : interface graphique pour MKVMerge. Incorpore un système de création de chapitres
MKVExtract : permet d'extraire des pistes/fichiers d'un fichier mkv
MKVInfo : liste l'ensemble des pistes ainsi que quelques détails supplémentaires
MKVPropEdit : modifier certaines métadonnées d'un fichier mkv

