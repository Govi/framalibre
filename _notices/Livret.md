---
nom: "Livret"
date_creation: "Samedi, 20 mai, 2017 - 12:24"
date_modification: "Jeudi, 7 juillet, 2022 - 12:28"
logo:
    src: "images/logo/Livret.png"
site_web: "https://github.com/brunetton/livret"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "Créer un livret à partir d'un fichier PDF."
createurices: "Bruno Duyé"
alternative_a: "Create Booklet"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/livret"
---

Livret est un petit programme libre écrit en Java permettant de créer des petits livres à partir d’un document au format PDF. Il crée, à partir d’un fichier source PDF, deux fichiers contenant les rectos et les versos prêts à être imprimés et agrafés.
Il nécessite Java.

