---
nom: "Lychee"
date_creation: "Jeudi, 8 février, 2018 - 20:37"
date_modification: "Mercredi, 12 mai, 2021 - 15:27"
logo:
    src: "images/logo/Lychee.png"
site_web: "https://lychee.electerious.com/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "English"
description_courte: "Lychee : une plateforme de gestion d'albums photos légère"
createurices: "Tobias Reich, electerious"
alternative_a: "Google Photos, Flickr"
licences:
    - "Licence MIT/X11"
tags:
    - "photothèque en ligne"
    - "photo"
    - "galerie photo"
    - "php"
    - "mysql"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lychee"
---

Lychee est un système open source développé en PHP / MySQL et Javascript vous permettant de gérer vos albums photos en ligne.
L'interface proposée est claire, rapide et va droit au but. Pas de paillettes ici, on va à l'essentiel ! On voit des albums et des photos !
Voici quelques fonctionnalités de Lychee :
Interface compatible mobiles / tablettes
 Upload multiple de photos
 Import des photos depuis un lien, un répertoire sur le serveur ou depuis Dropbox
 Création d'albums
 Partage d'albums via les réseaux sociaux ou un lien
 Possibilité de définir un album ou une photo publique ou privé
 Déplacement de photo dans un autre album
 Téléchargement d'un album ou d'une photo
 Notion de favoris
 Importe et affiche les infos EXIF et IPTC
 Raccourcis clavier

