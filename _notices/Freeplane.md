---
nom: "Freeplane"
date_creation: "Jeudi, 20 avril, 2017 - 18:37"
date_modification: "Mercredi, 12 mai, 2021 - 16:31"
logo:
    src: "images/logo/Freeplane.png"
site_web: "https://www.freeplane.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Pour créer des cartes heuristiques, des cartes mentales (mind mapping). Une référence dans cette catégorie."
createurices: "Volker Börchers, Dimitry Polivaev, Felix Natter"
alternative_a: "mindjet, MindMeister, Mindomo, imindmap"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "carte heuristique"
    - "mind mapping"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Freeplane"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freeplane"
---

La référence des logiciels de cartes mentales ou cartes heuristiques. La liste des fonctionnalités est tellement importante que cela dépasse le cadre de cette notice.
Pour découvrir plus avant Freeplane je vous conseille de l'installer puis d'ouvrir le fichier d'aide qui est une carte mentale très complète en français.
Freeplane est un fork de Freemind qu'il enrichi et améliore grandement.
Très pratique pour générer des cartes au même format que Freemind. Permet en outre d'exporter ses cartes au format svg pour les utiliser dans Sozi et réaliser de belles présentations.

