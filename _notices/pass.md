---
nom: "pass"
date_creation: "jeudi, 28 décembre, 2023 - 20:58"
date_modification: "jeudi, 28 décembre, 2023 - 20:58"


site_web: "https://www.passwordstore.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "pass est un gestionnaire de mots de passe simple pour la ligne de commande."
createurices: "Jason A. Donenfeld"
alternative_a: "lastpass, 1password"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "gestionnaire de mots de passe"
    - "chiffrement"
    - "mot de passe"
    - "authentification"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Pass_(logiciel)"
lien_exodus: ""
identifiant_wikidata: "Q25209352"
mis_en_avant: "non"

---

pass est un gestionaire de mots de passe simple pour la ligne de commande.
Chaque mot de passe est mis dans un fichier chiffré différent, dont le nom reflète le titre du site ou de la ressource concernée. Ces fichiers peuvent être organisés dans des dossiers, copiés d'un ordinateur à un autre, et en général manipulés avec des outils standards de gestion de fichiers en ligne de commande.
