---
nom: "KitScenarist"
date_creation: "Mardi, 12 mai, 2020 - 18:03"
date_modification: "Mercredi, 12 mai, 2021 - 15:52"
logo:
    src: "images/logo/KitScenarist.png"
site_web: "https://kitscenarist.ru/en/index.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Les bons plans reposent aussi sur de bons dialogues"
createurices: "Dimitry Novikov"
alternative_a: "Scrivener"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "script"
    - "écriture"
    - "nouvelle"
    - "roman"
    - "télévision"
    - "séries"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/kitscenarist"
---

KitScenarist est dédié à la création de script, de scénario pour l’audio-visuel et le cinéma. On peut en détourner l’usage pour écrire autre chose. Il intègre des outils de réflexion sur l’histoire et la constitution d’une documenthèque à portée de clic pour nourrir l’inspiration. D’autres outils servent à organiser les idées (cartes heuristiques, tableau pour punaiser et ordonner ses scènes.
Un éditeur non distractif (sans boutons et fonctionnalités superflues) facilite l’écriture en respectant les codifications de la profession pour indiquer le type de plan, les personnages de la scène, le lieu.
On pourra alors produire un document final qui donnera les indications nécessaires aux actrices, acteurs et toutes l’équipe de tournage.
KitScenarist exporte aussi dans un format pro audio-visuel pour intégrer le scenario à des applications de storyboarding (comme storyboarder, logiciel libre dédié).
Créer initialement par un Russe, l’application est aussi disponible en français.

