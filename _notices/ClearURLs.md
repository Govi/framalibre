---
nom: "ClearURLs"
date_creation: "Mercredi, 27 juillet, 2022 - 13:56"
date_modification: "Dimanche, 7 août, 2022 - 10:00"
logo:
    src: "images/logo/ClearURLs.png"
site_web: "https://gitlab.com/KevinRoebert/ClearUrls"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Une extension supprimant automatiquement les codes de surveillance des URL, afin de protéger votre vie privée."
createurices: "Kevin Röbert"
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "internet"
    - "vie privée"
    - "anonymat"
    - "add-on"
    - "extension"
    - "navigateur web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/clearurls"
---

• ClearURLs est une extension vous permettant de lutter contre les outils de surveillance des annonceurs, en supprimant automatiquement les éléments de suivi (directement depuis l'URL), afin de protéger votre vie privée lorsque vous naviguez sur Internet.
• Par exemple, lorsque vous faites des recherches sur Amazon (ou bien sur d'autres sites web du genre), le site web en question va vous fournir une URL plus ou moins longue (contenant des codes de surveillance) :
amazon.com/dp/exampleProduct/ref=sxin_0_pb?__mk_de_DE=ÅMÅŽÕÑ&keywords=tea&pf_rd_i=exampleProduct&pg_rd_p=50bbfd25-5ef7-41a2-68d6-74d854b30e30&ph_rd_r=0GMWD0YYKA7XFGX55ADP&qid=1517757263&rnid=2914120011
• Étant donné que ces codes de surveillance ne sont pas nécessaires au bon fonctionnement du site web, alors ils peuvent être supprimés sans problème, et c'est exactement ce que fait ClearURLs :
amazon.com/exampleProduct/dp/1903574031237
• ClearURLs fait partie des extensions recommandées par Mozilla Firefox.

