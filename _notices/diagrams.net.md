---
nom: "diagrams.net"
date_creation: "Lundi, 24 septembre, 2018 - 10:47"
date_modification: "Mercredi, 12 mai, 2021 - 16:32"
logo:
    src: "images/logo/diagrams.net.png"
site_web: "https://www.diagrams.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Logiciel de conception de schéma, diagramme, infographie, maquettage et prototypage"
createurices: "Gaudenz Alder, David Benson"
alternative_a: "Visio"
licences:
    - "Licence Apache (Apache)"
tags:
    - "création"
    - "schéma"
    - "diagramme"
    - "mind mapping"
    - "maquette"
    - "infographie"
    - "prototypage"
    - "carte heuristique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/diagramsnet"
---

(Anciennement Draw.io)
Très bonne application pour concevoir des schémas un vrai candidat à ceux qui cherche un remplaçant à Visio.
Moderne, simple et fluide cette application gagne à être connue.
Nécessite une bonne configuration car il s'agit d'une application JS/Electron.
Possibilité d'ajouter des bibliothèques et recherche de cliparts et d'éléments vectorisés sur site libre
Ajout et création de ses propres librairies et styles
Nombreux modèles (élégants)
Fonctions d'édition, d'alignement et de magnétisme puissante
Maquettage multi-pages avec possibilité de créer des hyperliens
Sauvegarde au format xml et export vers format matriciel, vectoriel, web et pdf
Bref, une tuerie pour organiser visuellement des informations et leurs relations

