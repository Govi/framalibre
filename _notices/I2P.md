---
nom: "I2P"
date_creation: "Mercredi, 15 mars, 2017 - 12:23"
date_modification: "Mercredi, 15 mars, 2017 - 13:16"
logo:
    src: "images/logo/I2P.png"
site_web: "https://geti2p.net/fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "I2P est une surcouche réseau d'anonymisation destinée à protéger les communications de la surveillance globale"
createurices: "Collectif i2p"
alternative_a: "NSA"
licences:
    - "Multiples licences"
tags:
    - "internet"
    - "chiffrement"
    - "réseau"
    - "anonymat"
    - "tunnel"
    - "vie privée"
lien_wikipedia: "https://fr.wikipedia.org/wiki/I2P"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/i2p"
---

I2P est une surcouche réseau d'anonymisation ; en quelque sorte, un réseau au sein du réseau. Il est destiné à protéger les communications contre la surveillance globale - dragnet surveillance -, et celle réalisée par des tiers tels que les fournisseurs d'accès à Internet.
I2P est utilisé par beaucoup de personnes soucieuses de leur confidentialité : des militants, des peuples opprimés, des journalistes, des lanceurs d'alertes, et aussi de simples utilisateurs d'Internet. Aucun réseau ne peut être "parfaitement anonyme". L'objectif continu de I2P est de rendre les attaques de plus en plus difficiles à mettre en place. La protection de l'anonymat qu'il procure s'améliorera à mesure que le réseau d'utilisateurs grandira et grâce aux recherches universitaires en cours.

