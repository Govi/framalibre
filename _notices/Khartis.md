---
nom: "Khartis"
date_creation: "Dimanche, 1 janvier, 2017 - 18:36"
date_modification: "Mercredi, 12 mai, 2021 - 15:57"
logo:
    src: "images/logo/Khartis.png"
site_web: "http://www.sciencespo.fr/cartographie/khartis/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Réaliser simplement des cartes statistiques en ligne."
createurices: ""
alternative_a: "Migratio, Philcarto, magrit"
licences:
    - "Licence MIT/X11"
tags:
    - "science"
    - "cartographie"
    - "carte géographique"
    - "statistique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/khartis"
---

Khartis est un logiciel qui s’utilise en ligne, développé par l'atelier de cartographie de Sciences Po.
Il permet à l’utilisateur de créer simplement une carte statistique en quelques étapes :
choix du fond de carte,
l'importation des données (ou l'utilisation des quelques données déjà présentes sur le site),
le choix et la configuration de la visualisation des données,
l'export éventuel (en svg ou en png).
Simple et efficace !

