---
nom: "GNU nano"
date_creation: "Mardi, 23 mai, 2017 - 00:43"
date_modification: "Vendredi, 7 juillet, 2017 - 18:14"
logo:
    src: "images/logo/GNU nano.png"
site_web: "https://www.nano-editor.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "Autres langues"
description_courte: "GNU nano est un éditeur de texte léger s’exécutant en console."
createurices: "Chris Allegretta"
alternative_a: "Pico"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "traitement de texte"
    - "texte"
    - "console"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GNU_nano"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gnu-nano"
---

GNU nano est un petit éditeur de texte et de code pour les systèmes Unix et ses dérivés. Il se veut simple à utiliser, idéal pour les débutants. Mais il peut aussi être très utile pour les utilisateurs plus expérimentés. Il s’exécute dans un terminal ( il n'a pas d'interface graphique ). Il a entre autres fonctionnalités: la coloration syntaxique, la recherche et le remplacement de texte, la possibilité d'éditer plusieurs fichiers ...
Ce programme fait officiellement  partie du projet GNU depuis février 2001.

