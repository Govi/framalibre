---
nom: "LyX"
date_creation: "Mercredi, 28 décembre, 2016 - 18:44"
date_modification: "Mardi, 3 janvier, 2017 - 14:35"
logo:
    src: "images/logo/LyX.png"
site_web: "https://www.lyx.org/WebFr.Home"
plateformes:
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Ce traitement de texte vous facilite la vie en privilégiant la structure de votre document et pas son look"
createurices: "Matthias Ettrich, Lars Gullik Bjønnes, Jean-Marc Lasgouttes"
alternative_a: "Microsoft Word"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "traitement de texte"
    - "latex"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LyX"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lyx"
---

LyX est un système de préparation de documents. Il est excellent pour écrire des articles scientifiques et techniques complexes avec des mathématiques, des références croisées, des bibliographies, des index, etc. Il excelle pour tous les documents de toutes tailles pour lesquels sont requises les fonctions habituelles d'un traitement de texte.
En pratique, il peut servir à préparer une lettre, une thèse, un rapport, un manuel ou un roman. En revanche, il ne convient pas pour une affiche où la maîtrise des positionnements est importante.
LyX encourage une rédaction fondée sur la structure de vos documents, et non pas sur leur affichage à l'écran. LyX vous laisse vous concentrer sur la rédaction, en laissant les détails de l'apparence au traitement logiciel. LyX formate automatiquement, suivant des règles prédéfinies, forçant ainsi la cohérence typographique. LyX produit un résultat imprimé de grande qualité grâce à l'usage sous-jacent de LaTeX, un moteur typographique éprouvé.

