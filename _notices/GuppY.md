---
nom: "GuppY"
date_creation: "Jeudi, 6 avril, 2017 - 01:34"
date_modification: "Mercredi, 29 janvier, 2020 - 20:21"
logo:
    src: "images/logo/GuppY.png"
site_web: "https://www.freeguppy.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "GuppY, le CMS/portail Web de référence sans base de données."
createurices: ""
alternative_a: ""
licences:
    - "Licence CECILL (Inria)"
tags:
    - "cms"
    - "portail"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Guppy_(portail)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/guppy"
---

GuppY est un logiciel de Gestion de Contenu complet, initié en 2004 par Laurent Duveau et Nicolas Alves.
Le logiciel est maintenu depuis par la "GuppY Team", avec le soutien d'une communauté réactive pour aider les utilisateurs à travers un forum d'entraide (français, anglais) très actif.
L'Asso FreeGuppY regroupe des utilisateurs et permet la promotion du logiciel.
La caractéristique de GuppY est d'être un logiciel sans base de données externe.

