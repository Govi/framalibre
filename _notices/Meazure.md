---
nom: "Meazure"
date_creation: "Dimanche, 21 juillet, 2019 - 18:03"
date_modification: "Mardi, 12 mai, 2020 - 21:40"
logo:
    src: "images/logo/Meazure.png"
site_web: "http://www.cthing.com/Meazure.asp"
plateformes:
    - "Windows"
langues:
    - "English"
description_courte: "Un outil pour mesurer ce qui est affiché à l'écran : tailles, distances, angles, couleurs"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "utilitaire"
    - "capture d'écran"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/meazure"
---

Meazure permet, comme son nom l'indique, de mesurer ce qui est affiché à l'écran.
Il est possible de mesurer des tailles, des distances, des angles, des rayons, des couleurs.
Les résultats sont affichés en pixels, centimètres, pouces, degrés, radians, RGB, HSL, #RRGGBB, etc.
L'outil comprend également une loupe, permet de faire des captures d'écran vers le presse-papier, d'afficher des règles ou une grille par dessus tout l'écran, et même d'enregistrer les mesures effectuées dans un fichier.

