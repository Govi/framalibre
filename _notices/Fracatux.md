---
nom: "Fracatux"
date_creation: "Jeudi, 27 avril, 2023 - 08:49"
date_modification: "Jeudi, 27 avril, 2023 - 12:05"
logo:
    src: "images/logo/Fracatux.png"
site_web: "https://achampollion.forge.aeif.fr/fracatux/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Fracatux est un logiciel libre permettant de représenter des fractions sous forme de barres."
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "mathématiques"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fracatux"
---

Fracatux est un logiciel libre permettant de représenter des fractions sous forme de barres, et d’effectuer avec différentes opérations. Il permet notamment de mettre en évidence les égalités de fractions, les additions de fractions avec le même dénominateur, et la correspondance entre les écritures fractionnaires, décimale et scientifique. Une ligne graduée (décimale ou fractionnaire) permet de situer ces mêmes fractions de façon ordinale.

