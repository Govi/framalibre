---
nom: "Belenios"
date_creation: "Lundi, 27 avril, 2020 - 00:00"
date_modification: "Lundi, 10 mai, 2021 - 12:37"
logo:
    src: "images/logo/Belenios.png"
site_web: "http://www.belenios.org/index.html"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Système de vote en ligne vérifiable"
createurices: "Stéphane Glondu, Véronique Cortier, Pierrick Gaudry, Alicia Filipiak"
alternative_a: "Balotilo, Loomio, Google Forms"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "vote"
    - "chiffrement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/belenios"
---

Belenios vise à fournir un système de vote facile à utiliser, garantissant la confidentialité et la vérifiabilité des votes. Il peut être utilisé pour de nombreux types d'élections (y compris les référendums), allant des conseils scientifiques aux associations sportives.
Confidentialité des votes : personne ne peut apprendre le vote d'un·e électeur·rice. La confidentialité des votes repose sur son chiffrement.
Vérifiabilité de bout en bout : chaque électeur·rice peut vérifier que son vote a été comptabilisé et seuls les électeur·rices ayant le droit de vote peuvent voter. La vérifiabilité de bout en bout repose sur le fait que l'urne est publique (les électeurs peuvent vérifier que leurs bulletins ont été reçus) et sur le fait que le décompte est vérifiable publiquement (tout le monde peut recompter les votes). De plus, les bulletins de vote sont signés par l'électeur·rice (seuls les électeur·rices éligibles peuvent voter).

