---
nom: "RadarWeather"
date_creation: "mercredi, 1 novembre, 2023 - 13:38"
date_modification: "mercredi, 1 novembre, 2023 - 13:38"
logo:
    src: "images/logo/RadarWeather.png"
site_web: "https://github.com/woheller69/weather#radarweather"
plateformes:
    - "Android"
langues:
    - "English"
description_courte: "Une application météo"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "météo"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.woheller69.weather/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Une application pour regarder la météo, tout simplement !

Vous pourrez y voir la météo actuelle et les prévisions jusqu'à 8 jours, les précipitations à 1h (et un radar de pluie), et même un widget avec mise à jour automatique de la position GPS en option.
