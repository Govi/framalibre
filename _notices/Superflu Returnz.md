---
nom: "Superflu Returnz"
date_creation: "jeudi, 4 janvier, 2024 - 19:51"
date_modification: "samedi, 6 janvier, 2024 - 02:56"
logo:
    src: "images/logo/Superflu Returnz.png"
site_web: "https://studios.ptilouk.net/superflu-riteurnz/"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Le jeu vidéo inutile du superhéros qui ne sert à rien"
createurices: "Simon \"Gee\" Giraudot"
alternative_a: ""
licences:
    - "Creative Commons (CC-By-Sa)"
tags:
    - "point & clic"
    - "Steam"
    - "jeu"
    - "jeu vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"

---

- Plongez dans un univers coloré dans le style d'un dessin animé 2D
- Parcourez tranquillement le village de Fochougny, un monde ouvert
- Résolvez les énigmes, trouvez des codes secrets, prenez des objets, combinez-les, parlez aux gens de Fochougny pour tenter d'en savoir plus
- Jouez à la souris dans la plus pure tradition des point-and-click, ou préférez la prise en main à la manette ou à l'écran tactile
- Appréciez les dialogues ciselés et l'humour omniprésent (qualité des blagues non-contractuelle)
- Détendez-vous sur un jeu où il est impossible de perdre, de mourir ou d'être coincé (mais où il est possible de vous arracher les cheveux sur certaines énigmes – les implants capillaires ne sont pas fournis)
- Jouez à votre rythme : avec ou sans indices pour vous aider dans votre progression
- Dialogues textuels disponibles en français, anglais et italien



