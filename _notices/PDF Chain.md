---
nom: "PDF Chain"
date_creation: "Lundi, 2 janvier, 2017 - 14:46"
date_modification: "Mercredi, 24 mai, 2017 - 10:47"
logo:
    src: "images/logo/PDF Chain.png"
site_web: "http://pdfchain.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "English"
description_courte: "Couteau suisse pour la manipulation de fichiers PDF."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "pdf"
    - "manipulation de pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pdf-chain"
---

PDF Chain permet de manipuler des fichiers PDF dans tous les sens:
assembler plusieurs fichiers en un seul
découper un fichier en plusieurs
imprimer une image de fond sur des pages
attacher des fichiers au document, ou les extraire
insérer un nom d'utilisateur et mot de passe
etc

