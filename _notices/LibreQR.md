---
nom: "LibreQR"
date_creation: "Mercredi, 25 mars, 2020 - 16:33"
date_modification: "Lundi, 29 août, 2022 - 19:36"
logo:
    src: "images/logo/LibreQR.png"
site_web: "https://code.antopie.org/miraty/libreqr"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Générateur de codes QR sur le Web."
createurices: "Miraty"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "php"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/libreqr"
---

Générez des codes QR sur le Web en choisissant leur contenu, taille, taille de la marge, taux de redondance et couleurs.
Toutes les couleurs de l'interface sont changeables avec un système de thème.
Écrit en PHP, sans JavaScript ni SQL.

