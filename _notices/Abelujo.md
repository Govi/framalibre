---
nom: "Abelujo"
date_creation: "Mardi, 27 décembre, 2016 - 21:59"
date_modification: "Vendredi, 5 août, 2022 - 13:34"
logo:
    src: "images/logo/Abelujo.png"
site_web: "http://abelujo.cc/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Abelujo, logiciel libre de gestion de librairies."
createurices: ""
alternative_a: "Librisoft"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "librairie"
    - "livres"
    - "collection"
    - "gestion du stock"
    - "dilicom"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/abelujo"
---

Abelujo est une application web qui permet de gérer le stock de librairies:
recherche bibliographique de qualité, par mots-clefs ou par isbn,
connexion au FEL à la demande de Dilicom et à Electre,
avec ou sans lecteur de code-barres,
menu caisse,
gestion des commandes clients,
historique des ventes, entrées et autres mouvements de stock,
exports aux formats txt, pdf ou csv,
commandes aux fournisseurs,
statistique (activité d'un rayon, meilleures ventes, paniers moyens, etc)
fonctionne pour des stocks français, belges, suisses et espagnols.
et plus encore.
Il existe une version de démonstration en ligne, et il existe un logiciel compagnon prêt à l'emploi qui permet de publier le catalogue en ligne: ABStock. L'équipe derrière le développement propose un service professionnel d'hébergement, et développe également des sites vitrines marchands sur-mesure.

