---
nom: "GPredict"
date_creation: "Samedi, 17 février, 2018 - 10:35"
date_modification: "Mercredi, 12 mai, 2021 - 15:10"
logo:
    src: "images/logo/GPredict.png"
site_web: "http://gpredict.oz9aec.net/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "English"
description_courte: "Visionner les satellites sur votre ordinateur"
createurices: "CSETE"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "satellite"
    - "espace"
    - "radio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gpredict"
---

Gpredict est un logiciel libre de tracking de satellite et de prédiction d'orbite. Gpredict peut traquer un nombre illimté de satellites pour visionner le résultats sur différentes données.
Possibilités
Tracking de Satellite
Prédiction d'orbitb
tracking Doppler
Contrôle du rotor de l'antenne

