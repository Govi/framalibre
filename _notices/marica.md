---
nom: "marica"
date_creation: "Mercredi, 2 décembre, 2020 - 11:42"
date_modification: "Lundi, 10 mai, 2021 - 14:30"
logo:
    src: "images/logo/marica.png"
site_web: "https://marica.fr"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Enregistrez l'ensemble des actions et des pièces des sinistres assurances et des dossier contentieux"
createurices: "Vincent Veyron"
alternative_a: "Legal suite"
licences:
    - "Licence CECILL (Inria)"
tags:
    - "métiers"
    - "statistiques"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/marica"
---

Ce logiciel est une application de gestion des dossiers de contentieux juridique et de sinistres d'assurances. Il est destiné aux personnes responsables de dossiers désirant formaliser le stockage des informations et faciliter l'édition de rapports et de statistiques. Le programme permet également la gestion des contrats
Les dossiers sont structurés selon le modèle classique des cabinets juridiques français : ils sont subdivisés en 'cotes' (État, Notes, Intervenants, Tiers, Agenda, Documents), qui permettent de classer de manière claire et uniforme l'ensemble des dossiers.
Le logiciel se paramètre très simplement pour l'adapter aux besoins du service, en modifiant les intitulés des champs de classement ainsi que leur contenu.
Il permet des recherches multi-critères ainsi qu'une recherche plein texte sur les contenu des documents enregistrés.
L'agenda fournit des alertes sur les événements à venir.

