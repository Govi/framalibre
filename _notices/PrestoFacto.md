---
nom: "PrestoFacto"
date_creation: "Jeudi, 20 octobre, 2022 - 14:13"
date_modification: "Mercredi, 9 août, 2023 - 14:31"
logo:
    src: "images/logo/PrestoFacto.png"
site_web: "https://www.philnoug.com/prestofacto"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
description_courte: "Application de gestion et de facturation des prestations journalières; cantine, garderie, activités, nuitées.."
createurices: "Philippe NOUGAILLON"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "école"
    - "association"
    - "cantine"
    - "facturation"
    - "comptabilité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/prestofacto"
---

PrestoFacto aide les petites organisations dans la gestion et la facturation de leurs prestations (cantine, garderie, activités périscolaires, hébergement à la nuitée, séjour en colonies de vacances, etc.).
Cette solution vous aidera dans l’organisation de votre association et vous fera gagner un temps précieux en simplifiant la gestion administrative et la facturation de vos prestations.
Une comptabilité simple par famille. Facturation des prestations basée sur le principe de la réservation.
Une facture par mois des prestations consommées, avec solde. Balance famille détaillée.
PrestoFacto est destiné aux petites collectivités, associations, mairies, écoles, centre de loisirs ...

