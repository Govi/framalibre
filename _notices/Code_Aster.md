---
nom: "Code_Aster"
date_creation: "Mardi, 11 septembre, 2018 - 15:40"
date_modification: "Vendredi, 21 septembre, 2018 - 10:04"
logo:
    src: "images/logo/Code_Aster.png"
site_web: "https://code-aster.org/spip.php?rubrique1"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Un logiciel de calcul aux éléments finis pour l'analyse des structures."
createurices: ""
alternative_a: "COMSOL Multiphysics"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "éléments finis"
    - "solveur"
    - "physique"
    - "mécanique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Code_Aster"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/codeaster"
---

Code_Aster (Analyses des Structures et Thermomécanique pour des Études et des Recherches) est un solveur utilisant la méthode des éléments finis développé par EDF et créé en 1989.
Il est spécialisé dans l'analyse des structures, et permet de modéliser de nombreux phénomènes liés à la mécanique et à ses couplages avec la thermique, l'acoustique. Un panorama complet du domaine couvert par le logiciel est présenté dans la plaquette Code_Aster.
De nombreux cas-test sont disponibles avec le logiciel ; une documentation détaillée et un forum sont accessibles en ligne.
La version de référence est la version GNU/Linux. Une version est disponible pour Windows, son implémentation est généralement moins avancée.
Code_Aster est le solveur intégré dans la plateforme Salome-Meca qui propose un environnement complet incluant modélisation de la géométrie, maillage et post-traitement des résultats.

