---
nom: "ekylibre"
date_creation: "Mercredi, 27 juin, 2018 - 11:42"
date_modification: "Samedi, 30 juin, 2018 - 09:49"
logo:
    src: "images/logo/ekylibre.png"
site_web: "https://ekylibre.com"
plateformes:
    - "GNU/Linux"
    - "Android"
langues:
    - "Français"
    - "English"
description_courte: "Gérer votre ferme simplement et complétement"
createurices: "Brice Texier, David Joulin"
alternative_a: "isamarge, atland, agreo"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "agriculture"
    - "gestion"
    - "erp"
    - "iot"
    - "cartographie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Ekylibre"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ekylibre"
---

Logiciel de gestion intégrée à destination des exploitations agricoles permettant de gérer tout les aspects de l'exploitation agricole : comptabilité, stocks, achats/ventes et traçabilité.
Une application mobile permettant de saisir les interventions au champ en mode déconnecté est également disponible.

