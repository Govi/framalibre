---
nom: "x4js"
date_creation: "Jeudi, 3 novembre, 2022 - 09:43"
date_modification: "Jeudi, 3 novembre, 2022 - 09:43"
logo:
    src: "images/logo/x4js.png"
site_web: "https://x4js.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Un framework typescript français pour développer vos applications."
createurices: "Etienne Cochard"
alternative_a: "extjs"
licences:
    - "Licence MIT/X11"
tags:
    - "développement"
    - "typescript"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/x4js"
---

x4js est un framework typescript. Il est tout petit. la version compilée, toutes options, fait 280Ko.
Toutes options, c'est une cinquantaine de contrôles prédéfinis (du bouton a la gridview) et la possibilité de créer les vôtres en 2mn.
x4js est surtout pensé Application, sur le cloud ou local avec Electron. Vous pouvez créer des sites web, mais il n'est pas fait pour ça. Enfin x4js est open-source licence MIT.
Les sources sont dispo sur github et vous pouvez créer votre projet grâce a l'utilitaire en ligne de commande (npm).

