---
nom: "OpenTURNS"
date_creation: "Mercredi, 3 novembre, 2021 - 15:24"
date_modification: "Vendredi, 5 août, 2022 - 13:44"
logo:
    src: "images/logo/OpenTURNS.png"
site_web: "https://openturns.github.io/www/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Bibliothèque Python pour la gestion des probabilités avec interface graphique Persalys"
createurices: ""
alternative_a: "UQLab"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "probabilités"
    - "incertitudes"
    - "python"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openturns"
---

OpenTURNS est une bibliothèque C++/Python dédiée au traitement des incertitudes. Elle permet de définir des variables et expériences aléatoire et de leur appliquer des méthodes numériques ou semi analytique. La documentation est très fournie.
Il existe une interface graphique pour Salome, Persalys, pour un couplage plus facile avec des codes de calcul scientifique.

