---
nom: "aMule"
date_creation: "Samedi, 18 mars, 2017 - 11:45"
date_modification: "Mercredi, 22 mars, 2017 - 11:27"
logo:
    src: "images/logo/aMule.png"
site_web: "http://www.amule.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "aMule est un client p2p utilisant les réseaux Kademlia et eDonkey2000 pour partager des fichiers."
createurices: ""
alternative_a: "SoulSeek"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "p2p"
    - "partage de fichiers"
    - "réseau"
lien_wikipedia: "https://fr.wikipedia.org/wiki/AMule"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/amule"
---

aMule est né d'un fork du logiciel eMule. Codé en Wx Widget il est multiplateforme. Comme eMule, aMule se connecte aux réseaux Kademlia et eDonkey2000 afin de partager des fichiers en utilisant la technologie p2p.
Central dans les années 2000 eMule est en perte de vitesse depuis plusieurs années. aMule est toujours en développement et reste tout à fait fonctionnel. Le logiciel ajoute plusieurs fonctionnalités à l'illustre eMule grâce aux proxys, un support d'un plus grand nombre de format, une possibilité de gérer le logiciel en ligne de commande, utilisation via des périphériques distants, une interface web et bien d'autres choses encore.

