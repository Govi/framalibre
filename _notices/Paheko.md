---
nom: "Paheko"
date_creation: "Mercredi, 28 décembre, 2016 - 13:00"
date_modification: "Jeudi, 13 juillet, 2023 - 12:09"
logo:
    src: "images/logo/Paheko.png"
site_web: "https://paheko.cloud/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
description_courte: "Gestion d'association : membres, compta pro mais simple, cotisations, site web, documents, etc."
createurices: "BohwaZ"
alternative_a: "Ciel Associations, helloasso, Assoconnect, Pep's Up"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "association"
    - "gestion"
    - "comptabilité"
    - "gestion de la relation client"
    - "crm"
    - "création site web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Paheko"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/paheko"
---

Gestionnaire d'association simple, léger, puissant et complet, à utiliser hors ligne ou en ligne, installé sur son serveur ou à utiliser directement sans installation en SaaS.
Permet de gérer la comptabilité (simple pour les débutant⋅e⋅s !), les membres, les cotisations, les inscriptions aux activités, les rappels par email, la messagerie entre membres, un site web simple personnalisable, le stockage et partage de documents (accessibles en WebDAV ou via les applis NextCloud et ownCloud), l'édition de documents partagés avec Collabora Online.
Mais aussi :
- reçus fiscaux
- gestion de caisse et de stocks
- réservation d'événements
- cartes de membres
- factures et devis
- etc.

