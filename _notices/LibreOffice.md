---
nom: "LibreOffice"
date_creation: "Dimanche, 8 novembre, 2015 - 11:40"
date_modification: "mardi, 26 décembre, 2023 - 12:55"
logo:
    src: "images/logo/LibreOffice.png"
site_web: "https://fr.libreoffice.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Une suite bureautique issue d'OpenOffice.org."
createurices: ""
alternative_a: "Microsoft Office, iWork"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "bureautique"
    - "traitement de texte"
    - "tableur"
    - "présentation"
    - "dessin vectoriel"
    - "base de données"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LibreOffice"
lien_exodus: ""
identifiant_wikidata: "Q10135"
mis_en_avant: "oui"
redirect_from: "/content/libreoffice"
---

LibreOffice est une suite bureautique issue d'OpenOffice.org, la suite libérée par Sun Microsystem. Elle est éditée par une fondation de droit allemand, The Document Foundation.
La suite est disponible pour les principaux systèmes d'exploitation sous deux versions : une destinée à la diffusion de masse en production, l'autre pour les utilisateurs plus avancés qui comporte les nouvelles fonctions qui méritent des tests supplémentaires en production.
Des versions pour Android et pour le nuage sont en cours de développement.
La communauté francophone est très active et apporte son concours aux utilisateurs, notamment par des listes de discussion.


