---
nom: "evQueue"
date_creation: "Jeudi, 9 novembre, 2017 - 12:03"
date_modification: "Vendredi, 10 novembre, 2017 - 11:02"
logo:
    src: "images/logo/evQueue.png"
site_web: "http://www.evqueue.net"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Ordonnanceur de tâches événementiel léger. Planificateur de tâches simples et composées (workflows)."
createurices: "coldsource, njean42, cris272"
alternative_a: "Dollar Universe, JAMS, Automate Schedule"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/evqueue"
---

evQueue est un ordonnanceur de tâches événementiel léger. Il permet la planification de tâches (remplacement de cron), mais également la gestion d’enchaînements complexes intégrant des boucles et des conditions (workflows). Le moteur permet d’utiliser la sortie d’une tâche pour instancier dynamiquement de nouvelles branches d’exécution.
L’objectif est d’extraire le flux de contrôle du code afin de donner une meilleure visibilité aux administrateurs système et aux développeurs. De plus, ce mode de fonctionnement assure la réutilisabilité du code avec le développement de briques élémentaires. La parallélisation intégrée des tâches via un système de files d’attente permet l’accélération des traitements intensifs en temps processeur, mais également le contrôle des ressources.
La version 2.0 propose une nouvelle interface de création de workflow en glisser‐déposer, ainsi que la haute disponibilité (mode cluster), et une prise en charge de git pour versionner tâches et workflows.

