---
nom: "Piped"
date_creation: "Samedi, 4 juin, 2022 - 16:09"
date_modification: "Samedi, 4 juin, 2022 - 20:22"
logo:
    src: "images/logo/Piped.png"
site_web: "https://piped.kavin.rocks/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Invidious, mais avec des fonctionnalités supplémentaires."
createurices: ""
alternative_a: "Youtube"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "vidéo"
    - "streaming"
    - "client youtube"
    - "bloqueur de publicité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/piped"
---

Piped fonctionne de la même manière qu'Invidious . Mais, il ajoute l'API de SponsorBlock qui permet d'éviter de visionnner les rappels d'interaction ou les placements de produits sur les vidéos.

