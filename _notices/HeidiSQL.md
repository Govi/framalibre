---
nom: "HeidiSQL"
date_creation: "Mardi, 7 novembre, 2017 - 11:28"
date_modification: "Lundi, 10 mai, 2021 - 13:55"
logo:
    src: "images/logo/HeidiSQL.png"
site_web: "https://www.heidisql.com/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Logiciel d'administration de base de données Microsoft SQL Server, MySQL/MariaDB et PostgreSQL"
createurices: "Ansgar Becker"
alternative_a: "SQL Server Management Studio (SSMS)"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "sql"
    - "postgresql"
    - "mysql"
    - "gestionnaire de base de données"
    - "mariadb"
lien_wikipedia: "https://fr.wikipedia.org/wiki/HeidiSQL"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/heidisql"
---

HeidiSQL  permet de gérer des bases de données Microsoft SQL Server, MySQL/MariaDB et PostgreSQL via une interface claire et complète. Il intègre la possibilité d'éditer les données, de modifier les bases et gérer les utilisateurs, de créer des tables et des vues, ainsi que des déclencheurs (triggers), et des événements planifiés. Enfin il supporte l'export en SQL de la structure et des données.
Il est également possible de l'emporter avec vous sur une clé USB par exemple avec sa version portable.

