---
nom: "Scribouilli"
date_creation: "lundi, 9 octobre, 2023 - 18:53"
date_modification: "vendredi, 22 décembre, 2023 - 17:35"
logo:
    src: "images/logo/Scribouilli.png"
site_web: "https://scribouilli.org/"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Créer votre petit site facilement !"
createurices: "L'Échappée Belle et cie"
alternative_a: "Wordpress, Wix"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "création de site web"
    - "site web statique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"

---

Scribouilli est un outil développé par un groupe d’ami·es pour aider les personnes qui veulent “juste créer un petit site” pour

- leur association
- leur usage personnel
- leur activité professionnelle
- ou ce qu’iels veulent !

Pour l’instant, Scribouilli permet d’avoir :

- un site de quelques pages,
- un site léger,
- qui s’affiche bien sur mobile,
- qui respecte les standards d’accessibilité,

Mais aussi :

- qui permet de mettre en forme son contenu (avec du Markdown)
- qui permet d’enrichir la mise en page avec du HTML




