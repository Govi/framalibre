---
nom: "Mageia"
date_creation: "Samedi, 31 décembre, 2016 - 17:34"
date_modification: "Mercredi, 12 mai, 2021 - 15:37"
logo:
    src: "images/logo/Mageia.png"
site_web: "https://www.mageia.org/fr/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Mageia est un système d’exploitation libre convivial stable et sécurisé basé sur GNU/Linux pour PC et serveurs"
createurices: "Association Mageia.Org"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "linux"
    - "distribution gnu/linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mageia"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mageia"
---

Mageia est un système d'exploitation basé sur Linux.
La philosophie : facilité d'utilisation, convivialité, stabilité et efficacité.
Mageia laisse le choix à ses utilisateurs :
le choix d'utiliser des pilotes libres ou des pilotes propriétaires,
le choix de son bureau : KDE, Gnome, Cinamon, Mate, Lxde, Xfce, Enlightenment,...
Mageia vous permet d'utiliser une grande bibliothèque de logiciels : LibreOffice, VLC, Gimp, LMMS, Steam, ...
Mageia est facilement paramétrable grâce à son Centre de Contrôle.
Mageia est gratuite. Parmi les distributions les plus populaires, elle compte de très nombreux utilisateurs.
Mageia, de par sa structure associative, est indépendante.
A vous de profiter du confort et de la liberté.
Si vous en avez besoin, vous trouverez de l'aide sur le site Mageia On Line, lien ci après.

