---
nom: "Garradin"
date_creation: "Mercredi, 28 décembre, 2016 - 13:00"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/Garradin.png"
site_web: "http://garradin.eu/"
plateformes: "GNU/Linux, BSD, Mac OS X, Windows, Android, FirefoxOS, Windows Mobile, Autre"
langues: "Français"
description_courte: "Gestion d'association : membres, compta à double entrée, wiki, cotisations, site web, fichiers, etc."
createurices: ""
licences: "Licence Publique Générale Affero (AGPL)"
tags:
    - "Métiers"
    - "association"
    - "but non-lucratif"
    - "gestion"
    - "comptabilité"
    - "Wiki"
    - "gestion de la relation client"
lien_wikipedia: ""
lien_exodus: ""
alternative_a: "Ciel Associations"
mis_en_avant: "oui"
---

Gestionnaire d'association simple, léger, puissant et complet, à utiliser hors ligne ou en ligne, installé sur son serveur ou à utiliser directement sans installation. Permet de gérer la compta, les membres, les cotisations, les rappels par email, la messagerie entre membres, et aussi intègre un wiki, un site web simple personnalisable avec des squelettes type SPIP, la gestion/stockage des fichiers, etc.

