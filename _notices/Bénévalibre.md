---
nom: "Bénévalibre"
date_creation: "Samedi, 25 avril, 2020 - 15:28"
date_modification: "Lundi, 10 août, 2020 - 16:53"
logo:
    src: "images/logo/Bénévalibre.png"
site_web: "https://benevalibre.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Un logiciel pour gérer et valoriser le bénévolat au sein de votre association."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "association"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/b%C3%A9n%C3%A9valibre"
---

Bénévalibre est un logiciel à destination des petites associations, avec pour objectif de gérer et valoriser le bénévolat.
Il peut en effet être utile pour une association de valoriser financièrement le temps donné par les bénévoles, par exemple pour refléter l'ampleur réelle des projets, mettre en lumière les coûts réels (le bénévolat ne gérant pas de coût), etc.
Le temps de bénévolat peut également être valorisé dans le cadre du Compte d'Engagement Citoyen (CEC) : le temps donné peut alors ouvrir droit à de la formation via le Compte Personnel d'Activité (CPA).
Il s'agit d'un logiciel accessible sur le web, qu'il faut héberger sur un serveur. Une instance est proposée librement à l'adresse https://app.benevalibre.org/.
Les bénévoles peuvent s'inscrire sur l'instance utilisé par le(s) association(s) auxquelles elles participent pour indiquer leur temps de bénévolat.

