---
nom: "gltron"
date_creation: "Mercredi, 1 janvier, 2020 - 16:55"
date_modification: "Mercredi, 12 mai, 2021 - 16:52"
logo:
    src: "images/logo/gltron.jpg"
site_web: "http://gltron.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Un jeu de combat de voitures où il faut emprisonner ses adversaires dans les murs qu'on laisse derrière soi."
createurices: "Andreas Umbach"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu de voiture"
lien_wikipedia: "https://en.wikipedia.org/wiki/GLtron"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gltron"
---

gltron est un jeu où ça va vite. Les voitures filent en avant dans une arène (en 3D), et laissent un mur de couleur derrière elles, selon le principe du snake. Le but est de surprendre l'adversaire pour qu'il s'y crashe dedans.  C'est un petit jeu addictif pour se défouler avec des potes.
De 1 à 4 joueurs, ordinateur ou humains.

