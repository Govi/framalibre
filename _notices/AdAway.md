---
nom: "AdAway"
date_creation: "Jeudi, 23 mars, 2017 - 19:20"
date_modification: "Vendredi, 24 mars, 2017 - 22:50"
logo:
    src: "images/logo/AdAway.png"
site_web: "https://adaway.org/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Partez à la chasse aux publicités et aux trackers !"
createurices: "Dominik Schürmann"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "android"
    - "utilitaire"
    - "bloqueur de publicité"
    - "vie privée"
lien_wikipedia: "https://en.wikipedia.org/wiki/AdAway"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/adaway"
---

AdAway vous permet de bloquer les publicités, trackers et domaines indésirables.
Contrairement à AdBlock Plus qui utilise un VPN, AdAway se sert du fichier hosts pour rediriger les requêtes. L'application se base sur des listes, et il est possible d'y ajouter les vôtres.
Elle permet aussi de gérer manuellement les domaines de votre choix en les plaçant dans votre white list/black list ainsi que de modifier leur redirection à l'aide d'un serveur web local.
Les droits root sont nécessaires pour écrire dans le fichier hosts.

