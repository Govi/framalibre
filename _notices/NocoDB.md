---
nom: "NocoDB"
date_creation: "samedi, 27 janvier, 2024 - 08:51"
date_modification: "samedi, 27 janvier, 2024 - 08:51"
logo:
    src: "images/logo/NocoDB.png"
site_web: "https://nocodb.com/"
plateformes:
    - "le web"
    - "Autre"
langues:
    - "English"
description_courte: "Créer une base de données comme un tableur sans connaissance du codage"
createurices: "NocoDB"
alternative_a: "Airtable"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "no-code"
    - "base de données"
    - "travail collaboratif"
    - "feuille de calcul"
    - "API"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q123282449"
mis_en_avant: "non"

---

NocoDB est une plateforme de base de données sans code qui permet aux équipes de collaborer et de créer des applications avec la facilité d'une interface de feuille de calcul familière et intuitive. Cela permet même aux non-développeurs ou aux utilisateurs professionnels de devenir des créateurs de logiciels.

NocoDB se connecte à n'importe quelle base de données relationnelle et la transforme en une interface de feuille de calcul intelligente ! Cela vous permet de créer des applications sans code en collaboration avec des équipes. NocoDB fonctionne actuellement avec les bases de données MySQL, PostgreSQL et SQLite (uniquement en Open Source).

Le magasin d'applications de NocoDB vous permet également de créer des flux de travail sur des vues en combinant Slack, Microsoft Teams, Discord, Twilio, Whatsapp, Email et toute autre API tierce. De plus, NocoDB fournit un accès programmatique aux API afin que vous puissiez créer des intégrations avec Zapier / Integromat et des applications personnalisées.
