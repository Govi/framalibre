---
nom: "PHPBoost"
date_creation: "Mercredi, 4 mai, 2016 - 22:05"
date_modification: "Mercredi, 26 avril, 2023 - 11:59"
logo:
    src: "images/logo/PHPBoost.png"
site_web: "https://www.phpboost.com/"
plateformes:
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "PHPBoost est un CMS opensource qui permet à tous de créer simplement un site personnalisable."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "php"
    - "forum de discussion"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/phpboost"
---

PHPBoost est un CMS opensource qui permet à tous de créer simplement un site personnalisable. Actuellement disponible dans sa version 6.0, PHPBoost  permet dès la fin de l’installation de disposer d'une trentaine de modules. Parmi eux vous disposerez d’un forum, d’un gestionnaire de pages et d’actualités, d’un wiki, de galeries photos …
La gestion des membres et des groupes rend très simple l'organisation d'une communauté (association, club...), notamment grâce à un système d’attribution des droits de lecture/écriture entièrement paramétrable. Enfin, pour permettre aux visiteurs et/ou membres de pouvoir participer au contenu du site, PHPBoost embarque un système de contribution complet.
L’administration permet de gérer en quelques clics le contenu du site, le positionnement des menus et bien plus !

