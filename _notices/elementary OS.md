---
nom: "elementary OS"
date_creation: "Mercredi, 10 juillet, 2019 - 10:59"
date_modification: "Mercredi, 10 juillet, 2019 - 10:59"
logo:
    src: "images/logo/elementary OS.png"
site_web: "https://elementary.io/fr/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Un système d’exploitation libre  orienté bureau et portab"
createurices: "Daniel Foré, Cody Garver, Cassidy James Blaede"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "distribution gnu/linux"
    - "ubuntu"
    - "debian"
    - "gtk"
    - "linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Elementary_OS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/elementary-os"
---

Un système d’exploitation libre  orienté bureau et portables, facile à installer et à maintenir, sécurisé, et  économe en ressources machine. elementary OS est basé sur les versions LTS (longue durée) de la distribution Ubuntu, tout en proposant son propre environnement graphique (Pantheon), et sa propre interprétation des logiciels de base (Mail, Fichier, Terminal, etc.). 
elementary OS est souvent abusivement présenté comme un “clone” du système Apple de par son attention toute particulière pour la finition et la cohérence de son interface utilisateur. 
Système libre, et en libre téléchargement, elementary OS repose sur un modèle original de type “payez ce que vous voulez” pour financer son développement et celui des logiciels présent sur son son centre des applications. Par ailleur le système est très largement compatible avec l'écosystème de GNOME, avec lequel elementary OS partage de nombreuses ressource, outre un environnement de développement commun (GTK, Vala, etc.

