---
nom: "Scrumblr"
date_creation: "Mardi, 20 novembre, 2018 - 13:39"
date_modification: "Mardi, 20 novembre, 2018 - 13:46"
logo:
    src: "images/logo/Scrumblr.png"
site_web: "https://github.com/aliasaria/scrumblr"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Un service en ligne collaboratif de prise de notes."
createurices: "Ali Asaria"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "kanban"
    - "gestion de projet"
    - "tableau interactif"
    - "memo"
    - "travail collaboratif"
    - "édition collaborative"
    - "décentralisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/scrumblr"
---

Scrumblr est un service en ligne libre et minimaliste qui permet d’éditer et d’organiser collaborativement des idées sous forme de notes.
Le service est volontairement très simple. Il permet de créer des post-it, de les disposer dans un tableau, d'y coller des gommettes de couleur. Les modifications sont sauvegardées en temps réel.
Le logiciel Scrumblr est décentralisé : différentes instances, hébergées par des organismes divers (développeur du logiciel, membres du collectif CHATONS, etc.), permettent de l'utiliser : scrumblr.ca ou Framemo.

