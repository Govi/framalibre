---
nom: "LibreTranslate"
date_creation: "Samedi, 12 novembre, 2022 - 13:35"
date_modification: "Samedi, 12 novembre, 2022 - 13:35"
logo:
    src: "images/logo/LibreTranslate.png"
site_web: "https://libretranslate.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "English"
    - "Autres langues"
description_courte: "API de traduction sous AGPL, basé sur des données sous license libre."
createurices: ""
alternative_a: "Google Translate"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "web"
    - "traduction"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/libretranslate"
---

LibreTranslate fournit une API de traduction open source.
Auto-hébergeable, fonctionne offline, facile à mettre en place.
LibreTranslate est écrit en python et utilise la librairie de traduction argos.

