---
nom: "RMS-RETAIL"
date_creation: "Mercredi, 31 octobre, 2018 - 12:16"
date_modification: "Mercredi, 31 octobre, 2018 - 13:16"
logo:
    src: "images/logo/RMS-RETAIL.png"
site_web: "http://rms-retail.net/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Organiser votre commerce, des outils ingénieux et inédits pour gérer votre point de vente."
createurices: "Pierre Doleans"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "commerce"
    - "point de vente"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rms-retail"
---

Gérer en quelques clics : commandes fournisseurs, stocks, checklists d'ouverture/fermeture, remises clients, plan de nettoyage, comptage automatique de la caisse, plan de compétences des équipiers, entretien des collaborateurs, réunions de staff et beaucoup d'autres choses au même endroit!

