---
nom: "marknotes"
date_creation: "Mercredi, 7 février, 2018 - 21:56"
date_modification: "Mercredi, 1 novembre, 2023 - 13:15"
logo:
    src: "images/logo/marknotes.png"
site_web: "https://github.com/cavo789/marknotes"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Autres langues"
description_courte: "marknotes, logiciel de conversion de notes en un site web de gestion de connaissances"
createurices: "Christophe Avonture"
alternative_a: "Evernote, Google Keep, OneNote"
licences:
    - "Licence MIT/X11"
tags:
    - "notes"
    - "prise de notes"
    - "markdown"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/marknotes"
---

marknotes  est un logiciel pour centraliser en un seul endroit et dans un format simple et ouvert des notes en conservant la maîtrise totale des données. Les notes, écritent en markdown, sont ainsi présentes sous la forme d'un site web HMTL5 et peuvent être exporté dans différents formats (.docx, .epub, .odt, .pdf, .txt, ...)
Des notes ?

des documentations techniques,
mode d’emploi de logiciels,
des comptes-rendu de réunions,
des notes prises sur le vif,
des trucs & astuces, pense-bêtes,
des informations sur des abonnements,
des factures, tickets de caisse, comptabilité,
des présentations,
des marque-pages (bookmarks),
…


