---
nom: "AliceVision Meshroom"
date_creation: "Samedi, 29 janvier, 2022 - 19:02"
date_modification: "Samedi, 29 janvier, 2022 - 19:02"
logo:
    src: "images/logo/AliceVision Meshroom.png"
site_web: "https://alicevision.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Meshroom est l'interface graphique construite autour de l"
createurices: ""
alternative_a: "Metashape, 3DF Zephyr, RealityCapture"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "création"
    - "3d"
lien_wikipedia: "https://fr.wikipedia.org/wiki/AliceVision_Meshroom"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/alicevision-meshroom"
---

Meshroom est l'interface graphique construite autour de la bibliothèque de vision par ordinateur AliceVision qui permet de générer un modèle 3D texturé à partir d'un ensemble non ordonné de photos. La principale caractéristique est son système nodal qui permet de modéliser le processus de reconstruction comme un pipeline de nœuds. Chaque nœud correspond à une étape du processus de reconstruction et le résultat de chaque nœud peut être utilisé comme entrée d'un autre nœud. Cela permet de personnaliser et d'adapter le processus à différents besoins selon le type d'applications.

