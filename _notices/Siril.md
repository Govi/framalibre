---
nom: "Siril"
date_creation: "Vendredi, 23 décembre, 2016 - 22:44"
date_modification: "Vendredi, 7 mai, 2021 - 11:14"
logo:
    src: "images/logo/Siril.png"
site_web: "http://free-astro.org/index.php?title=Siril/fr"
plateformes:
    - "BSD"
    - "Mac OS X"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Siril est un logiciel libre pour le traitement d'images en astronomie."
createurices: "Vincent Hourdin, Cyril Richard, François Meyer"
alternative_a: "Iris, Registax, DeepSkyStacker"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "astronomie"
    - "physique"
    - "algorithme"
    - "planète"
    - "astrophysique"
    - "télescope"
    - "lunette astronomique"
    - "image"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/siril"
---

Siril était supposé être Iris pour Linux (sirI-L). C'est un outil de traitement d'images pour l'astronomie, capable de convertir, pré-traiter, d'aligner manuellement ou automatiquement, d'empiler et d'améliorer le rendu final d'une séquence d'images.
Siril vise les astronomes amateurs qui ont acquis des images et qui veulent les traiter avec des moyens semi-automatiques.

