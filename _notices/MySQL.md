---
nom: "MySQL"
date_creation: "Jeudi, 21 mars, 2019 - 10:38"
date_modification: "Lundi, 10 mai, 2021 - 13:53"
logo:
    src: "images/logo/MySQL.png"
site_web: "https://dev.mysql.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "MySQL est un logiciel de bases de données s'appuyant sur différentes motorisations, comme MyISAM ou InnoDB."
createurices: "Michael Widenius"
alternative_a: "MariaDB, Oracle Database, Microsoft SQL Server"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "mysql"
    - "base de données"
lien_wikipedia: "https://fr.wikipedia.org/wiki/MySQL"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mysql"
---

Le logiciel de bases de données MySQL propose deux motorisations, à titre principal. Si MyISAM ne prend en charge ni les transactions, ni les contraintes d'intégrité référentielles, InnoDB fait de MySQL un réel moteur de bases de données relationnelles. Sa grande limitation, par rapport à ses concurrents commerciaux Oracle Databaseµ, Microsoft SQL Server et au moteur Open Source PostgreSQL, tient au fait que InnoDB - acquis par Oracle en 2005 - ne sait prendre pas en charge la concurrence en matière de verrous sur ligne.
Lors du rachat de MySQL par Oracle en 2009, Michael Widenius crée un fork du projet, MariaDB. A partir de la fin de l'année 2012, ce sont Wikipédia, puis Fedora, SUSE et Red Hat qui abandonneront MySQL au profit de MariaDB.

