---
nom: "InnerTune"
date_creation: "lundi, 12 février, 2024 - 22:13"
date_modification: "lundi, 12 février, 2024 - 22:13"
logo:
    src: "images/logo/InnerTune.webp"
site_web: "https://github.com/z-huang/InnerTune"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Écoutez de la musique en toute liberté, en streaming ou en ligne avec InnerTune."
createurices: "Zion Huang"
alternative_a: "Spotify, Deezer, YouTube Music"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "musique"
    - "lecteur musique"
    - "téléchargement de musique"
    - "streaming"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

**InnerTune** est un client libre pour YouTube Music qui permet de diffuser des playlists musicales en continu, en arrière-plan et sans interruptions publicitaires.

On peut constituer ses propres playlists, diffuser un album entier, gérer ses favoris (artistes, albums et chansons favorites).

Il propose le téléchargement des musiques pour une écoute hors-ligne, et enregistre par défaut les dernières musiques écoutées en cache pour économiser des données Internet. Un affichage dynamique des paroles est aussi possible sur les chansons compatibles.

Cette application est compatible avec Android Auto.
