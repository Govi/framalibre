---
nom: "MongooseIM"
date_creation: "Jeudi, 23 mars, 2017 - 17:51"
date_modification: "Mercredi, 12 mai, 2021 - 16:18"
logo:
    src: "images/logo/MongooseIM.png"
site_web: "https://www.erlang-solutions.com/products/mongooseim.html"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
langues:
    - "English"
description_courte: "Plateforme de chat/messaging pour mobile et web, massivement scalable"
createurices: ""
alternative_a: "Slack, HipChat, WhatsApp, Telegram, Skype"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "xmpp"
    - "jabber"
    - "chat"
    - "messagerie instantanée"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mongooseim"
---

MongooseIM est une plateforme libre et open source de chat/messaging. Elle permet le chat un à un, le group chat, le réseau social, les appels voix et vidéo. MongooseIM permet de créer des clients mobiles iOS et Android, ainsi que pour le web.
MongooseIM se repose sur le protocole XMPP/Jabber, et offre une alternative API REST.
Son différentiateur est son aspect massivement scalable et son approche cohérente de plateforme.

