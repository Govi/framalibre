---
nom: "Covid-25"
date_creation: "Mercredi, 21 octobre, 2020 - 17:08"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/Covid-25.png"
site_web: "http://www-ia.lip6.fr/~muratetm/covid25/"
plateformes:
    - "Autre"
langues:
    - "Français"
description_courte: "Et vous... qu'auriez-vous fait ?"
createurices: "Mathieu Muratet"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu sérieux"
    - "jeu vidéo"
    - "santé"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/covid-25"
---

Ce jeu a été réalisé durant le confinement lié à la Covid 19 de mars à juin 2020. A cette époque de nombreuses personnes dans les média semblaient visiblement savoir mieux que quiconque ce qu'il aurait fallu faire pour mieux gérer cette pandémie... Et si on pouvait toucher du doigt la complexité de la réalité et jouer avec...
Voici donc un petit jeu qui place le joueur dans la peau d'un décideur politique en 2025 lors d'une nouvelle pandémie. Le joueur doit définir des paramètres et les faire évoluer au cours du temps pour tenter de gérer la crise, fermera-t-il les frontières, payera-t-il le coup de la crise avec des vies humaines ou avec une dette colossale... Comment réagira-t-il face à la montée des mécontentements, écoutera-t-il ses conseillers... Son objectif ? Accompagner le pays jusqu'à atteindre l'immunité collective (60% de la population ayant développé des anticorps) mais éviter la crise sociale...

