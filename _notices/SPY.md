---
nom: "SPY"
date_creation: "Vendredi, 6 janvier, 2023 - 17:38"
date_modification: "Vendredi, 6 janvier, 2023 - 17:40"
logo:
    src: "images/logo/SPY.png"
site_web: "https://webia.lip6.fr/~muratetm/SPY/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
langues:
    - "Français"
description_courte: "A toi de t'initier à la programmation..."
createurices: "Sorbonne Université"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu sérieux"
    - "programmation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/spy"
---

SPY est un jeu sérieux conçu à destination d'élèves de cycle 3 (CM1, CM2, 6ème) pour s'initier aux bases de la programmation informatique. Tu devras programmer un robot pour l'aider à explorer une base secrète et s'en échapper sans être détecté. Tu apprendras à utiliser des actions, des structures de contrôle, des opérateurs et des capteurs. Sauras-tu les utiliser au moment opportun ? A toi de joueur...

