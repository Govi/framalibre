---
nom: "Pretix"
date_creation: "Vendredi, 4 mai, 2018 - 16:33"
date_modification: "Lundi, 28 octobre, 2019 - 20:44"
logo:
    src: "images/logo/Pretix.png"
site_web: "https://pretix.eu"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un logiciel pour créer une billetterie, également un service en ligne."
createurices: ""
alternative_a: "festik, tickboss"
licences:
    - "Licence Apache (Apache)"
tags:
    - "métiers"
    - "évènements"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pretix"
---

Pretix est un logiciel web qui permet de gérer la billetterie de son évènement, depuis la création des billets, leur personnalisation, en passant par un peu de marketing et l'analyse des données.
L'entreprise éditrice propose également un service en ligne et se paye avec un petit pourcentage sur les ventes de billets.

