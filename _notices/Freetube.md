---
nom: "Freetube"
date_creation: "Samedi, 17 avril, 2021 - 16:27"
date_modification: "Samedi, 7 août, 2021 - 14:11"
logo:
    src: "images/logo/Freetube.png"
site_web: "https://freetubeapp.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Le client Youtube pour PC."
createurices: ""
alternative_a: "Youtube"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "multimédia"
    - "téléchargement de youtube"
    - "client youtube"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/freetube"
---

Freetube est un client qui imite l'interface de Youtube et qui s'installe sur PC.
Caractéristiques du logiciel :
Toutes les données sont stockées localement que ce soit les playlists, les abonnements, l'historique de navigation.
Il possède une extension web du nom de Freetube Redirect qui permet de rediriger les liens Youtube à ce logiciel.
Il permet d'insérer un lien Youtube dans la barre de recherche.
Il est possible de rediriger la vidéo sur une autre plateforme comme Invidious ou Youtube.
Il permet de lire une vidéo au format audio.
Il permet d'utiliser l'API d'Invidious ou l'API locale.
Interface personnalisable.
Il permet d'utiliser un proxy provenant d'Invidious.

