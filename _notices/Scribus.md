---
nom: "Scribus"
date_creation: "Lundi, 1 mai, 2017 - 16:05"
date_modification: "Vendredi, 5 mai, 2017 - 02:36"
logo:
    src: "images/logo/Scribus.png"
site_web: "https://www.scribus.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Mise en page/PAO - Formulaires PDF - LaTeX"
createurices: "Christoph Schäfer, Andreas Vox, Gregory Pittman, Craig Bradney et Jean Ghali"
alternative_a: "Adobe InDesign, QuarkXPress, RagTime, Adobe Acrobat Pro"
licences:
    - "Multiples licences"
tags:
    - "création"
    - "mise en page"
    - "formulaire"
    - "pdf"
    - "latex"
    - "pao"
    - "dessin vectoriel"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Scribus"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/scribus"
---

Scribus est un logiciel de mise en page de niveau professionnel créé en 2001 qui est disponible pour de nombreux systèmes d'exploitation.
Il n'a pas grand-chose à envier à ses homologues commerciaux car il supporte toutes les fonctionnalités nécessaires au prépresse actuel (CMJN, Tons directs, ICC, PostScript, PDF, polices Open Type, etc.) y compris la création et l'importation d'illustrations vectorielles.
Scribus est aussi un des meilleurs concurrents d'Acrobat Pro en ce qui concerne la création de formulaires PDF interactifs grâce au support des calques et du JavaScript (scripts d'objets, scripts de document, etc.).
Scribus intègre également le rendu des langages balisés comme LaTeX ou Lilypond, et propose nativement la simulation du daltonisme (voir Color Oracle).

