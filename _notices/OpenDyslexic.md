---
nom: "OpenDyslexic"
date_creation: "Dimanche, 21 mai, 2017 - 21:47"
date_modification: "Dimanche, 21 mai, 2017 - 22:25"
logo:
    src: "images/logo/OpenDyslexic.jpg"
site_web: "https://opendyslexic.org/"
plateformes:

langues:

description_courte: "Open-Dyslexic est une police de caractères libre pour les personnes dyslexiques."
createurices: "Abelardo Gonzalez"
alternative_a: ""
licences:

tags:
    - "bureautique"
    - "police de caractères"
    - "dyslexie"
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/opendyslexic"
---

OpenDyslexic est une police de caractères libre de droits.
Elle a été conçue à partir d'une police libre "Bitstream Vera Sans Jim Lyles" puis modifiée pour l'adapter aux exigences et aux contraintes  inhérentes aux personnes dyslexiques.
OpenDyslexic améliore la lecture chez ces personnes :
en ajoutant des espaces plus larges
par un dessin particulier des lettres
En effet,  Les parties inférieures plus "grasses" des lettres aident l’œil à s’orienter et diminuent les confusions (b/d q/p...) en donnant un "poids" à la lettre. Ainsi, elles aident le cerveau à ne pas basculer/échanger ces lettres.
La police de caractères comprend des styles régulier, gras, italique et gras italique.
Elle est compatible Windows, Mac OS, Linux et Android.
Une nouvelle version OpenDylexic-Alta apporte certaines améliorations.
https://fr.wikipedia.org/wiki/OpenDyslexic

