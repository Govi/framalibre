---
nom: "Seafile"
date_creation: "Mercredi, 22 mars, 2017 - 02:28"
date_modification: "Samedi, 24 avril, 2021 - 09:50"
logo:
    src: "images/logo/Seafile.png"
site_web: "https://www.seafile.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "le web"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Synchronisation de fichier et bien plus !"
createurices: ""
alternative_a: "Google Drive, Dropbox, Microsoft OneDrive, Mega, Resilio Sync, time machine"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "synchronisation"
    - "sauvegarde"
    - "vie privée"
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Seafile"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/seafile"
---

Seafile est un logiciel libre permettant de construire un service de stockage et d'applications diverses en ligne.
Multi plateforme incluant les mobiles, partage de fichier avec gestion des droits, historique de version, co-édition en ligne de documents bureauté (via Collabora Online) et bien-sûr, chiffrement.

