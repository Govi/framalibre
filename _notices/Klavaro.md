---
nom: "Klavaro"
date_creation: "Mardi, 25 avril, 2017 - 13:44"
date_modification: "Lundi, 15 mai, 2017 - 14:05"
logo:
    src: "images/logo/Klavaro.png"
site_web: "http://klavaro.sourceforge.net/fr/index.html"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Klavaro est un logiciel d'apprentissage de la dactylographie"
createurices: "Felipe Castro"
alternative_a: "TypingMaster Pro, ProTrainer, MiracleType"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "clavier"
    - "apprentissage"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Klavaro"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/klavaro"
---

Klavaro est un logiciel d'apprentissage de la dactylographie.
Il possède la progression très pédagogique suivante :
Introduction : Comment taper correctement
Cours de base : Première étape - entraînement initial
Adaptabilité : Entraînement avec touches aléatoires
Vitesse : Entraînement avec des mots aléatoires
Fluidité : Entraînement avec des textes complets
Plusieurs dispositions de claviers sont supportées comme l'AZERTY, le QWERTY, … mais également le DVORAK et le BÉPO (disposition de clavier libre, francophone et ergonomique) qui est très utilisé par ceux qui veulent se protéger des TMS.

