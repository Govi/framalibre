---
nom: "Deluge"
date_creation: "Lundi, 16 janvier, 2017 - 19:27"
date_modification: "Mardi, 10 octobre, 2017 - 22:59"
logo:
    src: "images/logo/Deluge.png"
site_web: "http://www.deluge-torrent.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Deluge : Un client Torrent rapide et fonctionnel."
createurices: "Deluge Team"
alternative_a: "BitTorrent, BitComet, µTorrent, Xtorrent"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "torrent"
    - "p2p"
    - "téléchargement"
    - "client bittorrent"
    - "bittorrent"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Deluge_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/deluge"
---

Deluge est un client BiTorrent libre sous licence GNU/GPL v.3 avec de nombreuses fonctionnalités écrit en Python GTK+. Il utilise la librairie libtorrent et il est possible de créer vos Torrents.
Vous pouvez étendre les fonctionnalités grâce à de nombreux ajouts.
Liste des différentes fonctionnalités du client Deluge :
chiffrement du protocole BitTorrent
Table de hachage distribuée
Découvertes des pairs locaux
Prise en charge de l'extension FAST
Gestion de l'UPnP
NAT Port Mapping Protocol
Gestion des proxy
Réglage de la bande passante
Gestion des Plugins
Protection par mot de passe
Ajout de lien torrent par RSS avec le plugin flexget
Connexion à distance
Liens magnets

