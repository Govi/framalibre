---
nom: "Subdownloader"
date_creation: "Vendredi, 6 janvier, 2017 - 22:23"
date_modification: "Samedi, 7 janvier, 2017 - 14:54"
logo:
    src: "images/logo/Subdownloader.png"
site_web: "http://www.subdownloader.net/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Téléchargeur (automatique) de sous-titres."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "sous-titres"
    - "téléchargement de sous-titres"
    - "vidéo"
    - "film"
    - "traduction"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/subdownloader"
---

Avec ce logiciel, on peut classiquement chercher des sous-titres par nom de film, mais aussi, et c'est là qu'il brille, par dossier, et récursivement: si on sélectionne un ou plusieurs dossiers qui contiennent des films, Subdownloader va télécharger les sous-titres de chaque.
Il utilise la base de données de open-subtitles.org, et on peut également rendre la pareille et y téléverser des sous-titres.

