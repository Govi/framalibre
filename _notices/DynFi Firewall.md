---
nom: "DynFi Firewall"
date_creation: "Lundi, 7 février, 2022 - 15:34"
date_modification: "Mercredi, 12 avril, 2023 - 14:54"
logo:
    src: "images/logo/DynFi Firewall.png"
site_web: "https://dynfi.com/dynfi-firewall/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "DynFi® Firewall est le premier firewall Open Source Français."
createurices: "société DynFi"
alternative_a: "pfsense, stormshield, fortinet, ip fire, ip table"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "sécurité"
    - "firewall"
    - "cybersecurite"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dynfi-firewall"
---

DynFi® Firewall est un firewall Open Source Français. Le système intègre des fonctionnalités de filtrage et permet une gestion de vos appliances grâce au manager. Disponible gratuitement, DynFi Firewall dispose de deux images (avec retour VGA ou Série) compatibles avec la plupart des appliances du marché.
DynFi Firewall propose un ensemble d'outils vous permettant d'assurer un haut niveau de filtrage sur vos réseaux. Configurer simplement votre Firewall, IDS, Proxy. Bénéficiez de VPN avec IPSec et OpenVPN. Obtenez une vision complète et instantanée de vos traces de filtrage IP.
La version 3.0 de DynFi Firewall introduit un puissant filtre DNS basé sur le protocole RPZ et son intégration dans Unbound. Classé en 60 thématiques, il permet un filtrage DNS rapide portant sur plus de 14 millions d'URL. Intégration de Wireguard VPN et ajout de FreeRadius et d'autres packages. Correction de très nombreux bugs.

