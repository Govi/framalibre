---
nom: "Rambox"
date_creation: "Dimanche, 8 janvier, 2017 - 14:51"
date_modification: "Lundi, 24 octobre, 2022 - 18:18"
logo:
    src: "images/logo/Rambox.png"
site_web: "http://rambox.pro/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Rambox est un client de messagerie multi-réseaux (sociaux) gmail, twitter, mattermost, slack, rocketchat …"
createurices: ""
alternative_a: "hootsuite, tweetdeck, crowdfire"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "client mail"
    - "communication"
    - "réseau social"
    - "mastodon"
    - "diaspora"
    - "twitter"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rambox"
---

Rambox est devenu un Freemium, et la version du code source encore disponible sur GitHub est désormais archivé.
Rambox est un client de messagerie libre qui supporte plusieurs services de communication et réseaux sociaux comme :
WhatsApp, Messenger, Skype, Slack, Hangout, Telegram, WeChat, GMail, Inbox, HipChat, Steam, Outlook, Aim, Rocket.chat, Yahoo, TweetDeck, RoundCube, Horde, iCloudMail, RainLoop, OpenMailBox, Pushbullet et encore beaucoup d'autres.
Il permet aussi de se connecter via une application unique à ses réseaux libres préférés et en mode multicompte : Mastodon (framapiaf), Mattermost (Framateam), Kanboard (Framaboard), Diaspora, Riot, ProtonMail …
Il permet d'activer/désactiver les notifications, bloquer l'accès par un mot de passe, ajouter du code, ...
C'est un outil très pratique si vous devez gérer plusieurs comptes sur différents réseaux sociaux

