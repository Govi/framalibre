---
nom: "Movim"
date_creation: "Vendredi, 14 avril, 2017 - 01:49"
date_modification: "Mercredi, 12 mai, 2021 - 16:18"
logo:
    src: "images/logo/Movim.jpg"
site_web: "https://movim.eu/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Réseau social mélangé à du tchat"
createurices: "Timothée Jaussoin"
alternative_a: "Facebook, Facebook Messenger, msn"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "réseau social"
    - "vie privée"
    - "messagerie instantanée"
    - "visioconférence"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/movim"
---

Movim est un réseau social qui axe fortement son utilisation sur la messagerie instantanée. En effet, Movim se base sur les spécifications du protocole XMPP (parfois assimilé à Jabber). Ainsi, Movim est un réseau social décentralisé, comme Diaspora.
Cela ressemble aux courriels : il existe plusieurs fournisseurs de compte Movim (et tout un chacun peut s'en installer un), et tout le monde s'échange des messages sur le même réseau : un utilisateur de Gmail peut envoyer un courriel à un utilisateur de Laposte. Note : si ça vous semble évident, notez que ce n'est pas le cas de Facebook ou du tchat de Gmail, qui ont fermé leurs portes.
Sur Movim, on retrouve un mur, des cercles de contacts, des communautés (des groupes), et bien sûr le tchat (avec appels vidéo à venir). On peut également publier des articles sur un blog personnel automatiquement créé par Movim.
Un compte Movim ne nous restreint pas à son site. On peut communiquer avec d'autres gens qui ont une adresse XMPP/Jaber.

