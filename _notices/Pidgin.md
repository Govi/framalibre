---
nom: "Pidgin"
date_creation: "Jeudi, 22 décembre, 2016 - 23:42"
date_modification: "Mercredi, 12 mai, 2021 - 16:17"
logo:
    src: "images/logo/Pidgin.png"
site_web: "https://pidgin.im/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un client de messagerie instantanée multiprotocole pour discuter avec un maximum de correspondants."
createurices: "Pidgin Team, Mark Spencer, Sean Egan"
alternative_a: "Textual, Google Hangouts, icq"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "chat"
    - "messagerie instantanée"
    - "jabber"
    - "xmpp"
    - "irc"
    - "protocoles"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Pidgin_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pidgin"
---

Pidgin supporte de nombreux protocoles / réseaux de communication (AIM, Gadu-Gadu, Google Hangouts, Groupwise, ICQ, IRC, MSN, XMPP, etc.). Ce logiciel éclectique permet de communiquer avec plusieurs personnes sans pour autant changer de système de messagerie selon les services choisis par ces correspondants. Pidgin permet l'échange de fichiers, le multifenêtrage, la gestion des comptes selon les protocoles, l'enregistrement des conversations, etc. Mentionnons aussi un système de greffons qui permet d'ajouter de nombreuses fonctionnalités supplémentaires.

