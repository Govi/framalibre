---
nom: "vokoscreenNG"
date_creation: "Samedi, 21 mai, 2022 - 13:36"
date_modification: "Lundi, 23 mai, 2022 - 21:21"
logo:
    src: "images/logo/vokoscreenNG.png"
site_web: "https://linuxecke.volkoh.de/vokoscreen/vokoscreen.html"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Logiciel permettant de faire des captures d’écran vidéo."
createurices: "Volker Kohaupt"
alternative_a: "Screencast-o-matic, Snagit, TechSmith Capture, Wink"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "screencast"
    - "capture d'écran"
    - "capture vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/vokoscreenng"
---

Avec vokoscreenNG il est possible d'enregistrer soit l'écran complet, soit une zone choisie, soit une fenêtre (sous Linux uniquement).Il génère des vidéos en mkv, webm, avi, mp4 et mov.
Deux outils intéressant pour faire des tutoriels : le halo sur le curseur et l'indication des clics de souris. Les deux étant paramétrables (couleurs, taille).
Autres outils disponibles :
Personnalisation des raccourcis,
loupe
compte à rebours
minuteur

