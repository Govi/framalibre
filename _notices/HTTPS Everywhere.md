---
nom: "HTTPS Everywhere"
date_creation: "Samedi, 13 juin, 2020 - 18:16"
date_modification: "Samedi, 13 juin, 2020 - 18:16"
logo:
    src: "images/logo/HTTPS Everywhere.png"
site_web: "https://www.eff.org/https-everywhere"
plateformes:
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Une extension qui redirige automatiquement vers le protocole https pour plus de sécurité sur le Web."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "extension"
    - "add-on"
    - "sécurité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/https-everywhere"
---

HTTPS Everywhere est une extension pour les navigateurs Web qui chiffre vos communications. Elle redirige automatiquement vers les adresses utilisant le protocole HTTPS plutôt que HTTP, afin de renforcer la sécurité de votre navigation et éviter que des informations confidentielles puissent être interceptées.
Cette extension est disponible pour les navigateurs Firefox, Chrome et Opera. Elle est inclue par défaut dans les navigateurs Brave et Tor Browser.

