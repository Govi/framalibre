---
nom: "KDE Marble"
date_creation: "Jeudi, 29 décembre, 2016 - 16:39"
date_modification: "Mercredi, 12 mai, 2021 - 15:57"
logo:
    src: "images/logo/KDE Marble.png"
site_web: "https://marble.kde.org/"
plateformes:
    - "Autre"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Marble est un globe virtuel libre, léger et flexible qui permet de naviguer sur le globe librement."
createurices: ""
alternative_a: "Google Earth, Google Maps"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "internet"
    - "carte géographique"
    - "cartographie"
    - "planète"
    - "astronomie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Marble"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/kde-marble"
---

Marble permet d'observer notre planète Terre mais aussi la Lune, les planètes … par vue satellite, aérienne, cartographique, (globe virtuel, ou planisphère) ainsi que préparer des itinéraires, et consulter de cartes routières, cyclables,…. Très complet, vous y trouverez aussi les zones couvertes par les éclipses, les déplacements de la station spatiale internationale et une multitude d'autres choses diverses et variées en relation avec notre planète.

