---
nom: "LazPaint"
date_creation: "Jeudi, 29 décembre, 2016 - 23:06"
date_modification: "Mercredi, 12 mai, 2021 - 15:27"
logo:
    src: "images/logo/LazPaint.png"
site_web: "https://lazpaint.github.io/fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Éditeur d’image avec calques pixels et vectoriels, plus léger et plus simple que GIMP"
createurices: "Johann Elsass"
alternative_a: "Microsoft Paint, Paint.net"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "image"
    - "graphisme"
    - "photo"
    - "retouche"
    - "dessin vectoriel"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LazPaint"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lazpaint"
---

LazPaint offre les options les plus fréquemment utilisées dans un éditeur d’images :
Formats avec calques : LZP (LazPaint), ORA (OpenRaster), PhoXo, SVG, PDN (Paint.net, en lecture uniquement) et PSD (en lecture uniquement)
Formats multi-images : GIF, TIFF, ICO/CUR
Formats sans calques : BMP, JPEG, PCX, PNG, TGA, TIFF, WebP, XPM, XWD
Formats raw : DNG, CR2, NEF, ARW...
Formats 3D : OBJ
Différents outils de sélection
De nombreux filtres
Manipulation des couleurs
Gestion des calques
Formes vectorielles modifiables
Cerise sur le gâteau, LazPaint peut s’utiliser en ligne de commande.
Logiciel similaire : Pinta.

