---
nom: "Freeciv"
date_creation: "Dimanche, 2 avril, 2017 - 06:00"
date_modification: "Dimanche, 2 avril, 2017 - 06:01"
logo:
    src: "images/logo/Freeciv.png"
site_web: "http://www.freeciv.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Civilization avec 20 ans d'améliorations par ses fans."
createurices: "Peter Joachim Unold, Claus Leth Gregersen, Allan Ove Kjeldbjerg, Andreas Røsdal"
alternative_a: "Civilization"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "multi-joueur"
    - "stratégie"
    - "tour par tour"
    - "gestion"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Freeciv"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freeciv"
---

Développez votre civilisation de -4000 avant JC jusqu'au futur. Technologie, diplomatie, urbanisme, gestion économique et militaire... Jouez seul face à des intelligences artificielles ou en multi joueur : local, en ligne, par email, ou directement dans votre navigateur, selon vos préférences.
Il existe actuellement 5 interface pour jouer à freeciv. Je vous recommande de tester la version webgl dans le navigateur et son alternative 2D.

