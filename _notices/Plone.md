---
nom: "Plone"
date_creation: "Mercredi, 22 mars, 2017 - 20:43"
date_modification: "Jeudi, 23 mars, 2017 - 09:51"
logo:
    src: "images/logo/Plone.png"
site_web: "https://plone.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Le CMS opensource d'entreprise"
createurices: ""
alternative_a: "SharePoint"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "python"
    - "web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Plone"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/plone"
---

Plone est un CMS très complet orienté à la fois pour des sites publics et des intranets.
Il possède de nombreuses fonctionnalités :
processus de publication
nombreux types de contenus par défaut
gestion d'utilisateurs et de groupes
gestion fine des droits sur les contenus
multilingue
multisite
accessible
Les types de contenu existants sont extensibles, il est possible de créer facilement de nouveaux types de contenus. Les droits d'accès par utilisateurs ou par groupe peuvent être définis pour l'ensemble du site, mais également pour des sections du site.
Côté technique, il est basé sur le langage Python, et utilise une base de donnée objet (donc pas une base de données relationnelle).

