---
nom: "ViMusic"
date_creation: "mercredi, 1 novembre, 2023 - 13:25"
date_modification: "mardi, 26 décembre, 2023 - 14:10"
logo:
    src: "images/logo/ViMusic.png"
site_web: "https://github.com/vfsfitvnm/ViMusic#vimusic"
plateformes:
    - "Android"
langues:
    - "English"
description_courte: "Une application Android pour diffuser de la musique à partir de YouTube Music"
createurices: ""
alternative_a: "YouTube Music"
licences:
    - "Licence Apache (Apache)"
tags:
    - "musique"
    - "lecteur musique"
    - "client youtube"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/it.vfsfitvnm.vimusic/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"

---

Avec ViMusic, écoutez votre musique en utilisant les contenus de YouTube Music sans donner vos données à YouTube !

Vous pourrez :
- Lire (presque) n'importe quelle chanson ou vidéo de YouTube Music
- Avoir une lecture en arrière-plan et/ou hors-ligne
- Afficher les paroles des chansons
- Rechercher des chansons, albums, artistes, vidéos et listes de lecture
- Importer des listes de lecture
- Réorganiser les chansons dans la liste de lecture ou la file d'attente
- Avoir une minuterie de mise en veille
- Ouvrir des liens YouTube/YouTube Music (regarder, liste de lecture, chaîne)

