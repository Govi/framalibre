---
nom: "GanttProject"
date_creation: "Jeudi, 20 avril, 2017 - 22:17"
date_modification: "Vendredi, 21 avril, 2017 - 12:22"
logo:
    src: "images/logo/GanttProject.png"
site_web: "http://www.ganttproject.biz/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "GanttProject est un logiciel de gestion de projets très pédagogique pour une prise en main de ces méthodes."
createurices: "Alexandre Thomas, Dmitry Barashev"
alternative_a: "Microsoft Project"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "gantt"
    - "gestion de projet"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GanttProject"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/ganttproject"
---

Très bon logiciel pour se mettre à la gestion de projet. Permet de gérer des projets de tailles tout à fait raisonnables même s'il existe quelques limitations pour les très gros projets (voir page Wikipédia sur ce point).
On trouvera de très nombreux tutoriels concernant l'utilisation de GanttProject sur le web.

