---
nom: "Galette"
date_creation: "Samedi, 24 décembre, 2016 - 16:21"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/Galette.png"
site_web: "http://galette.eu"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Équipez votre association d'un outil de gestion des adhérents et des cotisations."
createurices: "Johan Cwiklinski"
alternative_a: "Citizen Adhésions, Ciel Associations"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "association"
    - "gestion"
    - "comptabilité"
    - "gestion de la relation client"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/galette"
---

Conçu pour les associations, Galette (Gestionnaire d’Adhérents en Ligne Extrêmement Tarabiscoté mais Tellement Efficace) permet la gestion des adhérents et des adhésions. Lancé initialement par le Groupe des Utilisateurs de Logiciels Libres (GULL) de Lyon, repris et désormais maintenu par Johan, ce logiciel correspond à des besoins concrets communs à toutes les associations (de petite taille) : fiches adhérents, mailing, échéancier, cotisations. Les fonctionnalités sont peu nombreuses mais claires. Galette fait peu de choses, mais il les fait bien. Galette s'installe assez facilement sur un serveur et nécessite PHP/Mysql pour fonctionner. L'installation est possible sur un serveur mutualisé (se référer au guide d'installation).

