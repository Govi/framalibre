---
nom: "Apache OFBiz"
date_creation: "Vendredi, 24 mars, 2017 - 17:35"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/Apache OFBiz.png"
site_web: "http://ofbiz.apache.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Apache OFBiz est un ERP construit autour d'un framework adapté à la montée en charge des Scale Up !"
createurices: "David E. Jones"
alternative_a: "SAP, Divalto"
licences:
    - "Licence Apache (Apache)"
tags:
    - "métiers"
    - "erp"
    - "crm"
    - "environnement de développement"
    - "gestion de la relation client"
    - "pgi"
    - "gestion du stock"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Apache_OFBiz"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/apache-ofbiz"
---

Apache OFBiz est un framework d'automatisation des processus d'entreprise. C'est un ERP/PGI riche en fonctionnalités métier qui se modèle pour une intégration au métier des utilisateurs.
Robuste, flexible et fiable, Apache OFBiz se démarque par le fait qu'il fut le premier ERP à être indépendant d'une société éditrice car maintenu par la fondation Apache.
Possédant une communauté très active, Apache OFBiz est particulièrement présent en Amérique du Nord, en Asie, en Inde et en Europe.

