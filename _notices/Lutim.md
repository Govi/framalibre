---
nom: "Lutim"
date_creation: "Dimanche, 22 février, 2015 - 12:09"
date_modification: "jeudi, 28 décembre, 2023 - 05:58"
logo:
    src: "images/logo/Lutim.png"
site_web: "https://lutim.fiat-tux.fr"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Partagez vos images en toute sécurité via cette application web rapide et anonyme."
createurices: "Luc Didry"
alternative_a: "Imgur, ImageShack"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "partage"
    - "image"
    - "anonymat"
    - "chiffrement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q112840372"
mis_en_avant: "oui"
redirect_from: "/content/lutim"
---

Lutim est un logiciel de partage d'image simple, rapide, anonyme et sécurisé. Il est destiné à être installé sur un serveur web.
Selon la configuration du serveur, les images seront chiffrées automatiquement ou le chiffrement sera à la discrétion de l'envoyeur.
À chaque image envoyée sur Lutim correspond plusieurs URLs :
une URL pouvant être utilisée comme n'importe quelle adresse d'image : elle peut être utilisée ainsi dans n'importe quel site;
une URL à utiliser sur les réseaux sociaux : l'image est alors inclue dans une page spécialement conçue pour une bonne intégration à Twitter et les sites comme Diaspora* et Facebook qui utilisent les données OpenGraph;
une URL de suppression;
une URL de modification.
Il est possible de choisir une durée de rétention des images : elles peuvent ainsi s'autodétruire après la première visite, au bout d'un jour, d'une semaine, d'un mois, d'un an ou jamais (les choix peuvent varier selon la configuration).
Attention ! Comme c'est anonyme, on ne peut pas retrouver une image dont on a perdu l'adresse.



