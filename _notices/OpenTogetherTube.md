---
nom: "OpenTogetherTube"
date_creation: "Lundi, 6 janvier, 2020 - 18:23"
date_modification: "Lundi, 10 mai, 2021 - 13:24"
logo:
    src: "images/logo/OpenTogetherTube.png"
site_web: "https://opentogethertube.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
description_courte: "Regardez des vidéos en même temps, chacun chez soi"
createurices: "Carson McManus"
alternative_a: "mycircle.tv"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "lecteur vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/opentogethertube"
---

Créer des conférences pour regarder des vidéos (possibilité d'en mettre plusieurs à la suite) à plusieurs.
On peut restreindre les droits, communiquer via le chat.
L'utilisation se fait sans inscription.

