---
nom: "OnionShare"
date_creation: "Jeudi, 5 janvier, 2017 - 17:33"
date_modification: "Vendredi, 7 mai, 2021 - 11:19"
logo:
    src: "images/logo/OnionShare.png"
site_web: "https://onionshare.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Partage de fichiers sensibles de toutes tailles, anonymement, sans tiers, et en sécurité grâce au réseau Tor."
createurices: "Micah Lee"
alternative_a: "WeTransfer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "partage de fichiers"
    - "partage"
    - "anonymat"
    - "chiffrement"
    - "tor"
    - "vie privée"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/onionshare"
---

OnionShare vous permet de partager des fichiers de n'importe quelle taille, anonymement et avec une très grande sécurité, en utilisant le réseau Tor. Il est adapté à des usages particulièrement sensibles, comme par exemple à des lanceurs d'alerte.
Concrètement, OnionShare crée, une fois vos fichiers sélectionnés, une adresse accessible depuis le réseau Tor. Les fichiers seront envoyés directement depuis votre ordinateur, mais le destinataire ne pourra pas connaître votre adresse IP (il doit utiliser le Tor Browser pour accéder aux fichiers). Vous n'avez pas à faire confiance à un hébergeur tiers, puisque vos fichiers ne sont hébergés que chez vous et de manière temporaire (dès que vous fermez OnionShare les fichiers ne sont plus sur Internet).
Vous devez faire attention d'utiliser une méthode sécurisée pour envoyer l'adresse générée.

