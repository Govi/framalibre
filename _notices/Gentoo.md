---
nom: "Gentoo"
date_creation: "Jeudi, 11 juillet, 2019 - 06:06"
date_modification: "Jeudi, 20 mai, 2021 - 16:27"
logo:
    src: "images/logo/Gentoo.png"
site_web: "https://www.gentoo.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Gentoo est une distribution GNU/Linux qui met l'accent sur le choix de ses utilisateurs."
createurices: "Daniel Robbins"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "gnu"
    - "linux"
    - "distribution gnu/linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Gentoo_Linux"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gentoo"
---

Gentoo est une distribution GNU/Linux créée en 1999 qui met le choix de ses utilisateurs au centre de son fonctionnement. Ces choix sont caractérisés par des "USE flags" que l'on peut définir de façon globale sur tout le système ou sur seulement certains paquets en particulier. Pour rendre ces choix possibles, la distribution est basée également sur la compilation des paquets sur les machines de ses utilisateurs. Cependant, ces derniers peuvent utiliser une machine pour compiler les paquets et ensuite distribuer les binaires résultants pour gagner du temps.

