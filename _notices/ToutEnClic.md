---
nom: "ToutEnClic"
date_creation: "Lundi, 30 avril, 2018 - 23:08"
date_modification: "Vendredi, 5 mai, 2023 - 00:21"
logo:
    src: "images/logo/ToutEnClic.png"
site_web: "http://bipede.fr/contrib/index.php/2018/02/20/nouvelle-version-majeure-de-touten…"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "A l'école, le handicap ne doit plus être un handicap. Gommer les différences."
createurices: "Alain DELGRANGE"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "handicap"
    - "école"
    - "accessibilité"
    - "apprentissage"
    - "géométrie"
    - "graphisme"
    - "écriture"
    - "python"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ToutEnClic"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/toutenclic"
---

ToutEnClic est un logiciel pour travailler à l'écran sur un document numérisé, de la même façon que sur un cahier,  pour palier le handicap des enfants incapables d’utiliser les outils usuels, tels que règles, crayons, compas. Une fois le document chargé dans la zone de travail, vous pouvez :

Tirer un trait
 Écrire à main levée
Tracer un pointeur
Dessiner une ellipse vide ou pleine
Dessiner un rectangle vide ou plein
 Gommer
Sélectionner une zone et la recopier à un autre endroit
Découper une zone et la recopier à un autre endroit
 Insérer une image externe dans une zone délimitée
Utiliser une règle, un rapporteur, une équerre, un compas et une loupe
Écrire un texte à l'aide d’un clavier virtuel propre à ToutEnClic ou tout autre dispositif de saisie


