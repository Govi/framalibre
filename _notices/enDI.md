---
nom: "enDI"
date_creation: "Lundi, 16 décembre, 2019 - 12:17"
date_modification: "Lundi, 16 décembre, 2019 - 12:17"
logo:
    src: "images/logo/enDI.png"
site_web: "https://endi.coop"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "enDI est un logiciel de gestion pour les collectifs d'entrepreneurs indépendants."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "gestion"
    - "saas"
    - "facturation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/endi"
---

enDI pour entreprendre DIFFEREMMENT est un logiciel de gestion simple, robuste qui vous permet d'entreprendre seul ou à plusieurs. Il s'occupe des devis, des factures. Si vous êtes un collectif d'entrepreneur.es indépendant.es, il s'occupe aussi de la gestion sociale et d'organiser l'accompagnement à l'entrepreneuriat.
Logiciel de gestion des CAE (Coopératives d'Activité et d'Emploi), il s'adapte parfaitement à la multi-activité tant dans les filières métier couvertes que dans l'organisation du collectif.

