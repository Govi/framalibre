---
nom: "Spip"
date_creation: "Samedi, 16 janvier, 2016 - 18:11"
date_modification: "Mardi, 28 novembre, 2017 - 13:57"
logo:
    src: "images/logo/Spip.png"
site_web: "http://www.spip.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Très puissant, SPIP peut gérer un petit blog comme un gros site, grâce à son système de « squelettes »..."
createurices: "Communauté spip"
alternative_a: "blogger, coldfusion, wix"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "blog"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_publication_pour_l%27Internet"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/spip"
---

SPIP est un gestionnaire de contenu qui a été développé par une équipe francophone avant de s'internationaliser.
Très puissant, il peut gérer un petit blog comme un gros site, grâce à son système de « squelettes » qui permet aux webmestres de fabriquer des modèles de pages à la demande. Des fonctionnalités supplémentaires s'ajoutent en quelques clics pour ne pas alourdir le cœur de l'application. Cette extrême modularité effraie les webmestres amateurs, du fait de son coût d'apprentissage, mais SPIP dispose de squelettes "clés en main" pour une mise en place rapide.
SPIP est reconnu pour son grand respect des principes de typographie et d'accessibilité, ce qui en fait un outil prisé des organes de presse.
Il permet le travail collaboratif, la proposition des articles avant publication, la séparation des tâches, l'attribution de mots de passe aux lecteurs et la publication préprogrammée.

