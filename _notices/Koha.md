---
nom: "Koha"
date_creation: "Mercredi, 22 mars, 2017 - 10:41"
date_modification: "Vendredi, 5 août, 2022 - 13:36"
logo:
    src: "images/logo/Koha.jpg"
site_web: "https://koha-community.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de gestion de bibliothèque avec catalogue en ligne intégré."
createurices: "Katipo Communications"
alternative_a: "Syracuse, Orphée, Décalog, Flora, BCDI, Co-libris"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "bibliothèque"
    - "sigb"
    - "centre de documentation"
    - "gestion documentaire"
    - "catalogue"
    - "opac"
    - "livres"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Koha"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/koha"
---

Koha est un système intégré de gestion de bibliothèque (SIGB) écrit en Perl. Il nécessite d'être installé sur un serveur GNU/Linux Debian ou d'autres distributions (OpenSUSE, Fedora, Red Hat…) et nécessite une base de données MySQL.
Il permet d'assurer toutes les tâches indispensables d'une bibliothèque :
Catalogage,
Bulletinage des revues et quotidiens,
Gestion des emprunteurs,
Emprunts, retours, réservations, prolongations,
Gestion des autorités,
Recherche dans les documents,
Création de listes,
Imports (Z39.50 notamment) et exports,
Envoi de mails et Diffusion sélective de l'information (DSI)
etc.
Il comporte un catalogue en ligne permettant aux usagers de faire des recherches, consulter les notices des documents, accéder à leur compte lecteur, créer des alertes, ajouter des commentaires, etc.
Koha peut être interfacé avec un CMS et intégrer des services externes.

