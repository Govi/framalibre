---
nom: "Yosys"
date_creation: "Dimanche, 26 décembre, 2021 - 00:44"
date_modification: "Lundi, 27 décembre, 2021 - 14:03"
logo:
    src: "images/logo/Yosys.png"
site_web: "https://yosyshq.net/yosys/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Suite d'outils de synthèse logique RTL libres et ouverts"
createurices: "Claire Xenia Wolf"
alternative_a: "synthèse logique RTL"
licences:
    - "Licence ISC"
tags:
    - "métiers"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Yosys"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/yosys"
---

Suite Logiciele de synthèse logique RTL permettant de créer les circuits logiques pour différents FPGA (Achronix, Altera, Anlogic, Gowin, Lattice, MicroChip/Microsemi, Xilinx) à partir de leur description en HDL (Verilog-2005, peut convertir BLIF / EDIF/ BTOR / SMT-LIB / simple RTL, GHDL peut s'interfacer avec pour le VHDL, supporté également par (n)Migen et la modélisation par schéma IceStorm) Le projet agglomère également des outils d’analyse de « bitstream » des principaux fabricants de FPGA afin de bénéficier d'outils complètement libres et ouverts en la matière.

