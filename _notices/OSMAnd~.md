---
nom: "OSMAnd~"
date_creation: "mercredi, 27 décembre, 2023 - 13:55"
date_modification: "mercredi, 27 décembre, 2023 - 13:55"
logo:
    src: "images/logo/OSMAnd~.svg"
site_web: "https://osmand.net/"
plateformes:
    - "Apple iOS"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Application libre de cartes hors ligne, navigation et enregistrement de traces GPS, basée sur les données OpenStreetMap"
createurices: "OsmAnd BV"
alternative_a: "GoogleMaps"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cartographie"
    - "android"
    - "gps"
    - "OpenStreetMap"
    - "routing"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OsmAnd"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/net.osmand.plus/latest/"
identifiant_wikidata: "Q3087512"
mis_en_avant: "non"

---

OSMAnd~ est une application complète pour la cartographie sur mobile, ses cartes sont consultables hors-ligne une fois téléchargées. Elle permet également la navigation et l'affichage des points d'intérêts par thématique. Il est également possible d'enregistrer ses traces GPS et les exporter dans divers formats.

Elle dispose de nombreuses extensions, notamment celle pour l'édition des données d'OpenStreetMap.

Payante sur Google Playstore ou Apple store, elle est gratuite via F-droid.
