---
nom: "LibraZiK"
date_creation: "Mardi, 21 avril, 2020 - 22:21"
date_modification: "Jeudi, 17 août, 2023 - 14:46"
logo:
    src: "images/logo/LibraZiK.png"
site_web: "https://librazik.tuxfamily.org/"
plateformes:

langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "LibraZiK - studio audio
Faire de la musique, librement !"
createurices: "Olivier Humbert"
alternative_a: ""
licences:
    - "Multiples licences"
tags:
    - "création"
    - "éditeur audio"
    - "retouche audio"
    - "logiciel audio"
    - "audio"
    - "midi"
    - "daw"
    - "lecteur audio"
    - "mao"
    - "partition de musique"
    - "musique"
    - "programmation audio"
    - "éditeur vidéo"
    - "ogg vorbis"
    - "flac"
    - "création musicale"
    - "gravure musicale"
    - "créativité"
    - "création multimédia"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/librazik"
---

LibraZiK est un projet francophone de suite logicielle pour studio de création. Ce système GNU/Linux basé sur Debian permet d'installer et d'utiliser tous les outils logiciels dont vous avez besoin pour travailler le son, la vidéo, l'image, etc.
Les logiciels suivants sont installés par défaut :
- Ardour (enregistrement, mixage, mastering...)
- Audacity (enregistrement, édition)
- Kdenlive (montage vidéo)
- JACK (connexion audio et midi entre n'importe quel logiciel compatible)
- Contrôleur QLC+ (pupitre lumières)
- GIMP (éditeur d'images)
- MuseScore (créateur de partitions)
- LMMS (création)

