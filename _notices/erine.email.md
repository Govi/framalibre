---
nom: "erine.email"
date_creation: "Dimanche, 31 juillet, 2022 - 20:35"
date_modification: "Dimanche, 31 juillet, 2022 - 20:35"
logo:
    src: "images/logo/erine.email.png"
site_web: "https://erine.email/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Vous souhaitez utiliser un service comme spamgourmet, qui"
createurices: "Mikael Davranche"
alternative_a: "jetable.org, crazymailing.com, yopmail.com, tempomail, spamgourmet"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "client mail"
    - "email jetable"
    - "auto-hébergement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/erineemail"
---

Vous souhaitez utiliser un service comme spamgourmet, qui permet d'avoir des adresses uniques et qui sont désactivables en cas de compromission, de spam, de diffusion et autres ?
Erine.mail le permet, et vous propose de l'héberger vous-même ou d'utiliser leur service en ligne.

