---
nom: "LanguageTool"
date_creation: "Mercredi, 8 avril, 2015 - 17:06"
date_modification: "Mercredi, 8 avril, 2015 - 17:14"
logo:
    src: "images/logo/LanguageTool.png"
site_web: "https://languagetool.org/fr/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "LanguageTool permet de corriger des textes."
createurices: "Daniel Naber"
alternative_a: "BonPatron, BonPatron Pro"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "bureautique"
    - "correcteur"
    - "grammaire"
    - "orthographe"
    - "java"
lien_wikipedia: "https://en.wikipedia.org/wiki/LanguageTool"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/languagetool"
---

LanguageTool permet de corriger des textes. Cet outil peut s'utiliser en ligne via la page principale du site officiel (ou la page de la langue désirée) dans un cadre réduit ou en plein écran, via le logiciel autonome de taille redimensionnable ou via les modules complémentaires pour LibreOffice, Apache OpenOffice et Firefox. La communauté peut améliorer la qualité de la vérification en proposant des règles XML ou en Java.

