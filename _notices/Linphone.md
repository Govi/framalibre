---
nom: "Linphone"
date_creation: "Samedi, 21 janvier, 2017 - 18:27"
date_modification: "Lundi, 10 mai, 2021 - 13:25"
logo:
    src: "images/logo/Linphone.png"
site_web: "https://www.linphone.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Windows Mobile"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Conférences audio et vidéo, compatible téléphonie."
createurices: "Belledonne Communications"
alternative_a: "Skype"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "appels audio"
    - "téléphonie sur ip"
    - "visioconférence"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Linphone"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/linphone"
---

Linphone permet de passer des appels audio et vidéo (en HD), de tchater, de s'envoyer des fichiers, etc. Il fonctionne sur une multitude de plateformes, y compris depuis un navigateur web ou une console linux. On peut aussi passer de vrais appels téléphoniques.
Il est basé sur le protocole SIP et est développé par la société Belledonne Communications qui propose des services commerciaux autour de ce logiciel.

