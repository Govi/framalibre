---
nom: "Shaarli"
date_creation: "Mardi, 18 septembre, 2018 - 23:13"
date_modification: "Jeudi, 13 mai, 2021 - 15:31"
logo:
    src: "images/logo/Shaarli.png"
site_web: "https://github.com/shaarli/Shaarli"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Un logiciel léger pour sauvegarder, trier et partager des adresses web."
createurices: "Sébastien Sauvage"
alternative_a: "delicious, Diigo"
licences:
    - "Licence Zlib"
tags:
    - "marque-pages"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/shaarli"
---

Shaarli est un logiciel libre permettant de sauvegarder, trier, synchroniser et partager des adresses web. Il est à la fois léger et simple d'utilisation.
Chaque adresse enregistrée est accompagnée d'un titre, d'une description et de tags. Shaarli peut être utilisé pour prendre des notes : il suffit de laisser le champ de l'adresse vide.
Une instance de démonstration est disponible (nom d'utilisateur·rice : demo, mot de passe : demo).

