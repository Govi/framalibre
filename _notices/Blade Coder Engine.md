---
nom: "Blade Coder Engine"
date_creation: "Lundi, 24 avril, 2017 - 15:02"
date_modification: "Lundi, 4 mars, 2019 - 01:31"
logo:
    src: "images/logo/Blade Coder Engine.png"
site_web: "https://github.com/bladecoder/bladecoder-adventure-engine"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Créateur de jeu d'aventure et d'enquête en point&clic (pointe et clique)."
createurices: "BladeCoder (Rafael Garcia)"
alternative_a: "Adventure Game Studio"
licences:
    - "Licence Apache (Apache)"
tags:
    - "jeu"
    - "jeu vidéo"
    - "développement de jeu vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/blade-coder-engine"
---

On créera une ou plusieurs scènes contenant des objets, des personnages avec leurs animations et leurs dialogues.
Il est alors possible de créer des scénarios complexes où pour résoudre l'énigme, le joueur devra dialoguer avec d'autres personnages, trouver des objets, les assembler entre eux pour résoudre des engimes et trouver de nouveaux indices.
Le jeu sera exportable dans un format exécutable dans les environnements GNU/Linux, Windows, MacOS, IOs ou encore Android (possibilité de créer des jeux pour interfaces tactiles).
Ne nécessite pas d'être codeur, mais nécessite un peu de logique et pour l'instant peu de documentation en français.
Exemple de jeu : http://bladecoder.com/bonasera/
(d'autres jeux démos sont téléchargeables depuis le site source)

