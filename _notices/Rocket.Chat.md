---
nom: "Rocket.Chat"
date_creation: "Vendredi, 12 avril, 2019 - 13:04"
date_modification: "Vendredi, 12 avril, 2019 - 13:04"
logo:
    src: "images/logo/Rocket.Chat.png"
site_web: "https://rocket.chat/"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "le web"
langues:
    - "English"
description_courte: "Un logiciel de communication en équipe !"
createurices: ""
alternative_a: "Slack"
licences:
    - "Licence MIT/X11"
tags:
    - "communication"
    - "chat"
    - "travail collaboratif"
    - "décentralisation"
    - "markdown"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/69680/"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/rocketchat"
---

Rocket.Chat est un logiciel de communication en équipe. Le fonctionnement de l'outil est très proche de celui de Mattermost : vous pouvez échanger avec les autres membres sur de fils publics (accessibles à tou·te·s), des groupes de discussion privés (accessibles sur invitation d'un·e membre) ou par messages privés.
La mise en forme des messages se fait avec le langage Markdown. De nombreuses fonctionnalités sont proposées : partage de fichiers, messages vocaux, échange par vidéo.
Comme tous les logiciels décentralisés, il est possible de l'installer sur son propre serveur.

