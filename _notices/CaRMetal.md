---
nom: "CaRMetal"
date_creation: "Dimanche, 27 octobre, 2019 - 19:09"
date_modification: "Lundi, 10 mai, 2021 - 14:44"
logo:
    src: "images/logo/CaRMetal.png"
site_web: "http://carmetal.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de géometrie dynamique"
createurices: "Éric Hakenholz"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "géométrie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/CaRMetal"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/carmetal"
---

CaRMetal est un logiciel de géométrie dynamique permettant de contruire tout ce dont vous avez besoin en quelques clic. Il dispose d'une interface simple, de macros, d'un outils de création de script javascript, d'exercices, ... Bref, si vous voulez faire de la géometrie simplement c'est le logiciel qu'il vous faut!
Si vous voulez apprendre à vous en servir un grand nombre de tuto sont disponible alors n'attendez pas!

