---
nom: "GrayLog2"
date_creation: "Lundi, 13 février, 2017 - 00:07"
date_modification: "Jeudi, 16 février, 2017 - 16:43"
logo:
    src: "images/logo/GrayLog2.jpg"
site_web: "https://www.graylog.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Graylog2, un outil open source pour centraliser les logs de plusieurs serveurs Linux et Windows."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "sécurité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/graylog2"
---

La solution GrayLog2 récupère les logs de serveurs Linux et windows. La solution fonctionne aussi sur les postes de travail. Elle centralise les logs sur un collecteur (un serveur) voire sur plusieurs machines en architecture distribuée.
Une fois collectés, les logs peuvent être analysés et en cas d'anomalies, des alertes peuvent être déclenchées par mail.
Graylog2 repose sur le moteur de recherche ElasticSearch et utilise une base de données MongoDB. Il possède une interface web dédiée à la gestion des logs.
L'installation de base est correctement documentée. En dehors de la récupération des logs systèmes, les procédures d'alimentation des logs applicatifs restent malheureusement encore assez nébuleuses.
Un bon produit cependant qui rend bien service pour ceux qui gèrent un parc de serveurs.

