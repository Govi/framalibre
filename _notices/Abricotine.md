---
nom: "Abricotine"
date_creation: "Lundi, 26 décembre, 2016 - 19:24"
date_modification: "Lundi, 4 mars, 2019 - 01:15"
logo:
    src: "images/logo/Abricotine.png"
site_web: "http://abricotine.brrd.fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Un éditeur markdown avec affichage direct du rendu."
createurices: "Thomas Brouard"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "markdown"
    - "éditeur"
    - "éditeur html"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/abricotine"
---

Avec Abricotine, il est possible d'écrire en utilisant la syntaxe Markdown avec un rendu direct HTML. Cet éditeur permet une écriture sans distraction pour mieux se concentrer sur le contenu. Abricotine propose l'export de document en HTML, dispose de raccourcis pour la mise en page, possède un éditeur de tableaux (avec une fonction « beautifier »), affiche une coloration syntaxique, utilise un panneau latéral pour afficher la table des matières, permet l'affichage de fonctions mathématiques (LaTeX)... L'insertion d'images ou des fichiers multimédia peut se faire depuis un dossier local ou depuis une adresse web.

