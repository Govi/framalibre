---
nom: "ArangoDB"
date_creation: "Jeudi, 19 novembre, 2020 - 20:15"
date_modification: "Lundi, 10 mai, 2021 - 13:55"
logo:
    src: "images/logo/ArangoDB.png"
site_web: "https://www.arangodb.com"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Base de données multi-model (graph / Document / Key/value) très performante."
createurices: ""
alternative_a: "MongoDB, Neo4j"
licences:
    - "Licence Apache (Apache)"
tags:
    - "système"
    - "base de données"
    - "gestionnaire de base de données"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ArangoDB"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/arangodb"
---

ArangoDB est un système de gestion de base de données NOSQL "multi-modèle". Il permet de créer des bases de données de type "Graphe", "Document" et "Key/value". Arangodb est écrit en C++, ce qui lui donne une performance bien supérieure à des alternatives telles que Neo4j ou MongoDB. 
L'installation de ArangoDB intègre :
- le moteur de base de données,
- une interface d'administration conviviale via un navigateur Internet,
- l'interpréteur javascript V8 de Google
- un serveur web permettant de créer des webservices en javascript 
ArangoDB possède son propre langage de requête : AQL (Arangodb Query Language).
Arangodb est Open-Source, et la version "Community" est gratuite.

