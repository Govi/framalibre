---
nom: "Pitivi"
date_creation: "Jeudi, 29 décembre, 2016 - 22:20"
date_modification: "Vendredi, 5 août, 2022 - 12:16"
logo:
    src: "images/logo/Pitivi.png"
site_web: "http://www.pitivi.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Un logiciel de montage vidéo simple pour faire des petites vidéos ou diaporamas."
createurices: ""
alternative_a: "iMovie"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "montage vidéo"
    - "diaporama"
    - "vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Pitivi"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pitivi"
---

Pitivi est un logiciel libre pour le montage vidéo. Il peut être également pour la réalisation de diaporamas grâce à plusieurs transitions. Basé sur Gstreamer, il accepte une multitude de formats multimédia. Son utilisation se veut simple et sa prise en main rapide. Il est adapté au bureau Gnome mais peut être utilisé sur l'ensemble des distributions LInux, quelque soit l'environnement de bureau. L'interface est simple et épuré, rappelant le logiciel privateur d'Apple : iMovie.

