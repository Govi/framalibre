---
nom: "Grisbi"
date_creation: "Dimanche, 5 novembre, 2017 - 00:42"
date_modification: "mardi, 26 décembre, 2023 - 18:43"
logo:
    src: "images/logo/Grisbi.png"
site_web: "https://www.grisbi.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de comptabilité personnelle intuitif et efficace."
createurices: "The Grisbi Team"
alternative_a: "CIEL"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "comptabilité"
    - "association"
    - "gestion"
    - "finances"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Grisbi"
lien_exodus: ""
identifiant_wikidata: "Q1992065"
mis_en_avant: "non"
redirect_from: "/content/grisbi"
---

Logiciel de comptabilité personnelle.
Intuitif et efficace, il permets de découvrir les bases de la comptabilité (comptes bancaires, pointage, catégories, emprunts, etc.)
Facile a prendre en main avec une version adaptée pour les associations, entreprises ou particuliers.
Il permet d'utiliser également un plan comptable français et d'éditer facilement des états (bilan).


