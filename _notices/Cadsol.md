---
nom: "Cadsol"
date_creation: "Mercredi, 2 juin, 2021 - 18:58"
date_modification: "Jeudi, 28 juillet, 2022 - 17:11"
logo:
    src: "images/logo/Cadsol.jpg"
site_web: "https://cadsol.web-pages.fr"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Logiciel de gnomonique
Tracés de cadrans et visualisation en 3D
Impression en 2D (svg) ou 3D (stl ou obj)"
createurices: "Astre Jean-Luc"
alternative_a: "Shadows"
licences:
    - "Licence CECILL (Inria)"
tags:
    - "science"
    - "gnomonique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Cadran_solaire"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cadsol"
---

Cadsol est un logiciel de tracé de cadrans solaires.
Sous licence GNU GPL (Cadsol est donc un logiciel libre).
Le logiciel, et les sources sont disponibles sur SourceForge
Version en ligne disponible (pas de téléchargement, pas d'installation)
 Types de cadran
Cadran classique à gnomon, ou à style polaire
Cadran analemmatique
Cadran vertical bifilaire
Cadran de hauteur (ou cadran de berger)
Cadran multiple plan
Cadran multiple cubique

