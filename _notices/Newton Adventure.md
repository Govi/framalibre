---
nom: "Newton Adventure"
date_creation: "Mercredi, 1 janvier, 2020 - 17:07"
date_modification: "Mercredi, 12 mai, 2021 - 16:52"
logo:
    src: "images/logo/Newton Adventure.jpg"
site_web: "https://play.devnewton.fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "Un jeu de plate-forme 2D avec une variation: le joueur peut faire tourner la gravité"
createurices: ""
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "jeu"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/newton-adventure"
---

C'est à priori un jeu de plateforme en 2D normal, sauf que: on peut faire tourner tout le décor à 360°, avec la conséquence que la gravité change de même. La gravité peut donc être utilisée pour éviter les pièges, tuer les ennemis, résoudre des énigmes, et amener la clef du niveau jusqu'à la porte de sortie.
On peut y jouer en ligne: Newton Adventure en ligne.
Il est disponible dans les dépôts Debian/Ubuntu.

