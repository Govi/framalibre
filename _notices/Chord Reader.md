---
nom: "Chord Reader"
date_creation: "Jeudi, 23 février, 2017 - 15:51"
date_modification: "Jeudi, 23 février, 2017 - 15:51"
logo:
    src: "images/logo/Chord Reader.png"
site_web: "https://nolanlawson.com/apps/#chordreader"
plateformes:
    - "Android"
langues:
    - "English"
description_courte: "Chercher et afficher des paroles de chansons, des accords et des tablatures."
createurices: "Nolan Lawson"
alternative_a: ""
licences:
    - "Licence publique f***-en ce que vous voulez (WTFPL)"
tags:
    - "multimédia"
    - "musique"
    - "partition de musique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/chord-reader"
---

Chord Reader est une application Android qui permet de chercher sur Internet et d'afficher des paroles de chansons, des accords et des tablatures. L'application permet également de transposer les accords. Les fichiers sont éditables et enregistrés au format .txt dans un dossier du téléphone.

