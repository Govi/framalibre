---
nom: "KeePass"
date_creation: "Dimanche, 11 décembre, 2016 - 19:51"
date_modification: "Lundi, 10 mai, 2021 - 13:39"
logo:
    src: "images/logo/KeePass.png"
site_web: "http://keepass.info/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "KeePass est un logiciel de gestion des mots de passe."
createurices: "Dominik Reichl"
alternative_a: "lastpass, 1password"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "gestionnaire de mots de passe"
    - "chiffrement"
    - "mot de passe"
    - "authentification"
lien_wikipedia: "https://fr.wikipedia.org/wiki/KeePass"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/keepass"
---

KeePass est un logiciel de gestion des mots de passe. Il permet de sauvegarder l'ensemble des mots de passe dans un fichier chiffré, accessible uniquement par le mot de passe principal.
Avec ce logiciel, il est possible de respecter les préconisations de sécurité, en choisissant des mots de passe robustes, et des couples utilisateur/mot de passe uniques pour chaque site, puisque le logiciel les retiendra pour vous.
KeePass est en version 2, sous le nom KeePass2.
La version 2.1 a été « Certifiée de Sécurité de Premier Niveau » par l'ANSSI.

