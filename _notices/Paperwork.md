---
nom: "Paperwork"
date_creation: "Mercredi, 4 janvier, 2017 - 11:33"
date_modification: "Mercredi, 12 mai, 2021 - 15:21"
logo:
    src: "images/logo/Paperwork.png"
site_web: "https://openpaper.work/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Trier les documents est un travail de machine."
createurices: "FLESCH Jérôme - Développeur principal de Paperwork"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "reconnaissance optique de caractères"
    - "scanner"
    - "pdf"
    - "ged"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/paperwork"
---

Paperwork est un gestionnaire de documents personnels. Il gère les documents scannés et les PDFs.
Il est conçu pour être facile et rapide à utiliser. L'idée derrière Paperwork est "scanner et oublier" : Vous pouvez juste scanner un nouveau document et tranquillement l'oublier jusqu'au jour où vous en aurez de nouveau besoin.
En d'autres termes, laissez la machine faire l'essentiel du travail pour vous.
Les principales fonctionnalités sont :
Numérisation
Détection automatique de l'orientation des pages
OCR, indexation, recherche et suggestions de mot-clés
Labels sur les documents
Labelisation automatique des nouveaux documents
Édition rapide des scans
Support des PDFs

