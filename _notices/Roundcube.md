---
nom: "Roundcube"
date_creation: "Mercredi, 6 décembre, 2017 - 21:13"
date_modification: "Vendredi, 21 mai, 2021 - 10:05"
logo:
    src: "images/logo/Roundcube.png"
site_web: "https://roundcube.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Roundcube est un client de messagerie pour le protocole IMAP écrit en PHP et JavaScript."
createurices: "Roundcube"
alternative_a: "Adresse mail fournie par votre FAI, Gmail, Windows Live Mail, Microsoft Outlook, Hotmail, Yahoo! Mail"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "client mail"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Roundcube"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/roundcube"
---

Roundcube est un client de messagerie pour le protocole IMAP écrit en PHP et JavaScript.
Cette application libre est publiée sous licence GPL. L'interface utilisateur est disponible en 81 langues, dont l'espéranto.
Roundcube peut être installé sur une plateforme LAMP. Il est compatible avec les serveurs web Apache, Nginx, Lighttpd, Hiawatha ou Cherokee, et les bases de données MySQL, PostgreSQL et SQLite sont supportées. La gestion des thèmes se base sur les standards du web comme XHTML et CSS2.
Roundcube se base sur d'autre composant open source de PEAR et sur la bibliothèque IMAP IlohaMail, l'éditeur TinyMCE et correcteur d'orthographe et grammaire GoogieSpell.

