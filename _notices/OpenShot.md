---
nom: "OpenShot"
date_creation: "Jeudi, 29 décembre, 2016 - 22:09"
date_modification: "Mardi, 1 septembre, 2020 - 11:30"
logo:
    src: "images/logo/OpenShot.png"
site_web: "http://www.openshot.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Le logiciel de montage vidéo simple et puissant à la fois."
createurices: "Jonathan Thomas"
alternative_a: "iMovie, Windows Movie Maker, Camtasia"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "éditeur vidéo"
    - "montage vidéo"
    - "animation"
    - "vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenShot_Video_Editor"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openshot"
---

OpenShot Vidéo Editor est un logiciel de montage vidéo développé à la base pour GNU/LInux mais porté depuis la version 2.0 sur Windows et MacOS, grâce à l'utilisation de la bibliothèque Qt.
Il propose des fonctionnalités simples comme l'ajout de transition ou de "cuts" mais aussi des fonctionnalités plus avancées comme l'incrustation par calques, les fonds verts ou l'export en 4k.

