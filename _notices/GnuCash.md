---
nom: "GnuCash"
date_creation: "Samedi, 29 juillet, 2017 - 12:16"
date_modification: "Samedi, 29 juillet, 2017 - 12:16"
logo:
    src: "images/logo/GnuCash.png"
site_web: "http://www.gnucash.org/index.phtml?lang=fr_FR"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de suivi de finances."
createurices: ""
alternative_a: "Microsoft Money, Quicken"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "comptabilité"
    - "gestion"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GnuCash"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gnucash"
---

GnuCash est un logiciel de suivi de finances personnelles ou professionnelles.
L'import de fichiers de banque aux formats QIF, OFX/QFX… permet de lancer rapidement sa gestion de finances personnelles.
Des options «Affaires» permettent d'utiliser GnuCash pour la comptabilité d'une entreprise ou d'une association (gestion des clients, factures, fournisseurs, employés…
L'application pour Android permet en outre d'exporter ses données mobiles vers l'application PC (en retour de vacances par exemple).

