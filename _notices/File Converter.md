---
nom: "File Converter"
date_creation: "Vendredi, 5 octobre, 2018 - 15:30"
date_modification: "dimanche, 11 février, 2024 - 01:16"
logo:
    src: "images/logo/File Converter.png"
site_web: "https://file-converter.io"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Convertit et compresse n'importe quoi en 2 clics !"
createurices: "Adrien Allard"
alternative_a: "FreemakeVideoConverter, MediaCoder"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "convertisseur"
    - "vidéo"
    - "audio"
    - "image"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/file-converter"
---

File Converter est un outil très simple qui permet de convertir et compresser un ou plusieurs fichier(s) en utilisant le menu contextuel de l'explorateur Windows.
Il supporte de nombreux formats de fichier et vous permet si besoin d'ajouter de nouvelles configurations ou de personnaliser les configurations existantes.
