---
nom: "Kdenlive"
date_creation: "Jeudi, 29 décembre, 2016 - 16:10"
date_modification: "Jeudi, 19 octobre, 2023 - 16:18"
logo:
    src: "images/logo/Kdenlive.png"
site_web: "https://kdenlive.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Le logiciel de montage vidéo simple et puissant par KDE !"
createurices: "Jean-Baptiste Mardelle, et al."
alternative_a: "Vegas Pro, Adobe Premiere, Final Cut"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "montage vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Kdenlive"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/kdenlive"
---

Kdenlive est un logiciel de montage vidéo permettant la création de films amateurs et professionnels.
Basé sur les librairies MLT et FFmpeg, il autorise une infinité de pistes, propose un grand nombre d'effets, et prend en charge presque tous les formats connus. Suivant les principes de la communauté KDE, son interface est "simple par défaut, puissante quand il y a besoin" : rapide à prendre en main pour les débutants, les habitués de ce type de logiciels peuvent retrouver nombre d'outils qu'ils attendent (composition, titrage, colorimétrie, options d'export)...
Le logiciel est soutenu par une communauté internationale d'artistes qui guide ses évolutions pour une utilisation toujours plus plaisante.

