---
nom: "balenaEtcher"
date_creation: "Samedi, 14 janvier, 2017 - 14:34"
date_modification: "Samedi, 6 juillet, 2019 - 16:57"
logo:
    src: "images/logo/balenaEtcher.png"
site_web: "https://www.balena.io/etcher/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Etcher pour graver vos distributions sans prise de tête"
createurices: ""
alternative_a: "Nero"
licences:
    - "Licence Apache (Apache)"
tags:
    - "système"
    - "utilitaire"
    - "gravure"
    - "iso"
    - "usb"
    - "live usb"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Etcher"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/balenaetcher"
---

Etcher est un utilitaire de gravure d'images (zip, img, iso) sur différents supports (clé usb,carte sd,microsd).Etcher est vraiment très simple d'utilisation et assure une gravure efficace grâce à sa fonction de vérification de l'image gravée.
Il a de plus l'avantage d'être portable et peut donc être transporté sur une clé usb.
https://debian-facile.org/doc:install:usb-boot:etcher
Point de vigilance : d’après la page Wikipédia dédiée à ce logiciel, ce dernier intègre des outils visant à capter les données des utilisateurs (type Google Analytics, GoSquared et Mixpanel), même sans leur consentement ! Nous vous conseillons de chercher une alternative parmi les autres logiciels de gravure recensés dans Framalibre.

