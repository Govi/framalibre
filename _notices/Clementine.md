---
nom: "Clementine"
date_creation: "Lundi, 9 novembre, 2015 - 22:59"
date_modification: "Dimanche, 12 décembre, 2021 - 16:13"
logo:
    src: "images/logo/Clementine.png"
site_web: "https://www.clementine-player.org/fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "Autres langues"
description_courte: "C'est un excellent lecteur de musique qui fonctionne même sur des configurations assez légères."
createurices: "David Sansome, John Maguire, Arnaud Bienner"
alternative_a: "iTunes, winamp"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur musique"
    - "lecteur audio"
    - "bibliothèque musicale"
    - "musique"
    - "audio"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Clementine_%28logiciel%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/clementine"
---

C'est un excellent lecteur de musique qui fonctionne même sur des configurations assez légères.  Sa liste de fonctionnalités est impressionnante, on retiendra principalement sa gestion très efficace d'une bibliothèque de morceaux de musique, la gestion de playlists multiples ou bien son accès à toutes sortes de webradio.
La possibilité de commander à distance Amarok depuis un smartphone Android est un net avantage pour faire de ce logiciel le pilier de son audiothèque. L'installation d'Amarok sur un RaspberryPi transformera immédiatement ce minuscule ordinateur en console audio.
Clémentine est un fork d'Amarok.

