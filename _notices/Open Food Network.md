---
nom: "Open Food Network"
date_creation: "Jeudi, 6 août, 2020 - 23:26"
date_modification: "Vendredi, 5 mars, 2021 - 20:54"
logo:
    src: "images/logo/Open Food Network.png"
site_web: "https://www.openfoodnetwork.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Permet de mettre en route un service pour passer des commandes de nourriture de façon locale"
createurices: "Open Food Foundation"
alternative_a: "La ruche qui dit oui"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "circuit court"
    - "système de commande de nourriture en ligne"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/open-food-network-0"
---

C'est un logiciel utilisable pour mettre en service un site web de mise en relation entre des producteurs de nourriture, des boutiques et de consommateurs afin de pouvoir passer des commandes et organiser toute la logistique entre deux (prendre la commande, faire la livraison, garder le contact, ...).
Principale plate-forme en France : https://coopcircuits.fr/

