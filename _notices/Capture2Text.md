---
nom: "Capture2Text"
date_creation: "Jeudi, 4 octobre, 2018 - 23:20"
date_modification: "Mercredi, 12 mai, 2021 - 15:21"
logo:
    src: "images/logo/Capture2Text.png"
site_web: "http://capture2text.sourceforge.net/"
plateformes:
    - "Windows"
langues:
    - "English"
description_courte: "Reconnaissance de caractères. Pour copier les textes qu'on ne peut sélectionner."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "reconnaissance optique de caractères"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/capture2text"
---

Il arrive parfois qu'on veuille copier un texte qu'on ne peut sélectionner à la souris. Par exemple:
- On vous a envoyé le scan d'un relevé d'identité bancaire (RIB) et vous voulez faire un virement en ligne sur ce compte. Vous aimeriez bien pouvoir copier-coller l'identifiant du compte plutôt que le recopier à la main;
- Un logiciel vous affiche un message d'erreur incompréhensible. Vous aimeriez copier-coller ce message dans un moteur de recherche pour essayer de comprendre ce qui se passe.
C'est là où Capture2Text peut vous aider et vous éviter une fastidieuse recopie. Capture2Text permet de sélectionner une portion rectangulaire de votre écran, et tente de générer le texte contenu dans ce rectangle en utilisant le procédé de reconnaissance de caractères (OCR). Quand l'opération de reconnaissance est terminée, le texte généré peut être copié dans le presse-papier ou être envoyé directement à une autre fenêtre.

