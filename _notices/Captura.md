---
nom: "Captura"
date_creation: "Dimanche, 21 juillet, 2019 - 19:52"
date_modification: "Lundi, 24 août, 2020 - 07:13"
logo:
    src: "images/logo/Captura.png"
site_web: "https://mathewsachin.github.io/Captura/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Capture d'écran, en images, gif animés ou vidéos, avec des tas d'options pratiques"
createurices: "Mathew Sachin"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "multimédia"
    - "capture d'écran"
    - "capture vidéo"
    - "screencast"
    - "webcam"
    - "gif"
    - "utilitaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/captura"
---

Parmi les logiciels open source de capture d'écran que j'ai essayé jusqu'ici, Captura se démarque par ses fonctionnalités :
Capture d'images fixes (screenshots)
Éditeur d'image pour retravailler et enrichir les captures
Enregistrement d'écran (screencast) créant des fichiers vidéo ou des gif animés
Enregistrement simultané du son du micro
Enregistrement d'une zone fixe ou qui suit le curseur de la souris
Incrustation de textes, d'images, ou même de la webcam dans les captures
Mise en évidence des clics de la souris pendant les enregistrements
Incrustation des noms des touches pressées
Possibilité de dessiner au-dessus de la capture, en temps réel pendant les enregistrements
Pause pendant les enregistrement.
Captura peut aussi servir pour enregistrer seule la vidéo prise par la webcam.
Au final, un outil très complet qui pourra servir à enregistrer des  tutoriels vidéo.

