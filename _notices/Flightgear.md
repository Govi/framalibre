---
nom: "Flightgear"
date_creation: "Dimanche, 10 mars, 2019 - 10:25"
date_modification: "Jeudi, 17 août, 2023 - 21:08"
logo:
    src: "images/logo/Flightgear.png"
site_web: "http://home.flightgear.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Pilotez des avions et autres véhicules et partez à la découverte du ciel !"
createurices: "Curt Olson"
alternative_a: "Microsoft Flight Simulator X, X-Plane"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "simulation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FlightGear"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/flightgear"
---

Flightgear Flight Simulator (FGFS) est un simulateur de vol permettant de piloter avions civils et militaires mais aussi hélicoptères, OVNI, fusées et autres engins spatiaux. Il est également possible de télécharger d'autres modèles d'aéronefs ainsi que des scènes depuis un site appelé FGAddon (mais également sur d'autres sites).
Flightgear supporte les joysticks, plusieurs écrans, des pédales, des manettes de poussée, etc.

