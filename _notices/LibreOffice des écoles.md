---
nom: "LibreOffice des écoles"
date_creation: "Jeudi, 28 mars, 2019 - 22:04"
date_modification: "Lundi, 10 mai, 2021 - 14:22"
logo:
    src: "images/logo/LibreOffice des écoles.png"
site_web: "https://primtux.fr/libreoffice-des-ecoles/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Une interface pour s'initier au traitement de texte à l'école."
createurices: "Franck Rouillé et Philippe Hénaff"
alternative_a: "Microsoft Office"
licences:
    - "Creative Commons -By-Sa"
tags:
    - "éducation"
    - "suite bureautique"
    - "bureautique"
    - "enfant"
    - "école"
    - "écriture"
    - "apprentissage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/libreoffice-des-%C3%A9coles"
---

LibreOffice des écoles est une interface de LibreOffice permettant d’offrir aux utilisateurs une prise en main simplifiée du traitement de texte et adaptée aux jeunes élèves (et aux autres !!!).
On peut également y insérer simplement des éléments multimédias.
Elle intègre les outils de la voix de synthèse PicoSvoxOOo afin de permettre à « l’écrivain » d’entendre sa production ainsi que les plugins LireCouleur (un ensemble d’outils destiné à aider les lecteurs débutants ou en difficulté) et Grammalecte (un correcteur grammatical et typographique).
LibreOffice des écoles offre quatre niveaux d’utilisation (1, 2 ,3 et « standard ») proposant une approche progressive de l’apprentissage du traitement de texte.

