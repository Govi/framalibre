---
nom: "CotEditor"
date_creation: "Vendredi, 5 août, 2022 - 11:51"
date_modification: "Vendredi, 5 août, 2022 - 11:51"
logo:
    src: "images/logo/CotEditor.png"
site_web: "https://coteditor.com"
plateformes:
    - "Mac OS X"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "CotEditor est un éditeur de texte léger pour macOS."
createurices: "Nakamuxu"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "développement"
    - "code"
    - "traitement de texte"
    - "texte"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/coteditor"
---

CotEditor est un éditeur de texte léger pour macOS.
Il supporte la mise en évidence de la syntaxe sur plus de 50 langages tels que HTML, PHP, Python, Ruby or Markdown. Mais il est également possible de créer ses propres réglages.

