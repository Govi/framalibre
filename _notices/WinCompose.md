---
nom: "WinCompose"
date_creation: "Lundi, 8 avril, 2019 - 15:33"
date_modification: "Mercredi, 12 mai, 2021 - 14:44"
logo:
    src: "images/logo/WinCompose.png"
site_web: "https://github.com/samhocevar/wincompose"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Taper facilement des caractères qui ne figurent pas sur le clavier, tels que À, Ç, É, œ, ¼ ou ·"
createurices: "Sam Hocevar"
alternative_a: ""
licences:
    - "Licence publique f***-en ce que vous voulez (WTFPL)"
tags:
    - "système"
    - "clavier"
    - "utilitaire"
    - "raccourci"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wincompose"
---

Ce logiciel permet de taper facilement des caractères qui ne figurent pas sur le clavier, tels que œ, les majuscules accentuées À, Ç, É, ou encore le point médian de l'écriture inclusive ·
Exemples:
- Taper [Alt droit] suivie de [o] puis [e] pour écrire œ
- Taper [Alt droit] suivie de [
