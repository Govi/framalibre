---
nom: "Todo.txt"
date_creation: "Mercredi, 14 février, 2018 - 20:03"
date_modification: "Vendredi, 2 juin, 2023 - 12:57"
logo:
    src: "images/logo/Todo.txt.png"
site_web: "http://todotxt.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Suivez vos tâches et projets dans un fichier texte simple, todo.txt"
createurices: "Gina Trapani"
alternative_a: "Microsoft To-Do, Wunderlist, Todoist, Google Keep"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "tâche"
    - "productivité"
    - "todo-list"
    - "prise de notes"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/todotxt"
---

Todo.txt est un script shell permettant de gérer toutes ses tâches quotidiennes en ligne de commande. L'un des principaux avantages de cet outil est de ne fonctionner qu'avec deux fichiers texte: un fichier todo.txt qui contient la liste des tâches en cours ou à faire, et un fichier done.txt qui contient les tâches qui sont terminées. On peut ainsi synchroniser ou sauvegarder très facilement le suivi de nos tâches, ouvrir le fichier todo.txt pour en modifier le contenu très facilement ou l'éditer sur un PC qui n'a pas le script installé, etc...
Un grand nombre d'outils complémentaires permettent d'utiliser ce fichier sur de multiples plateformes.

