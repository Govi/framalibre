---
nom: "DVDStyler"
date_creation: "Samedi, 18 août, 2018 - 18:30"
date_modification: "Mercredi, 12 mai, 2021 - 14:44"
logo:
    src: "images/logo/DVDStyler.png"
site_web: "https://www.dvdstyler.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "DVDStyler intègre tous les outils nécessaires pour créer des menus personnalisés pour les DVD vidéo."
createurices: "Alex Thüring"
alternative_a: "Adobe Encore"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "dvd"
    - "vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dvdstyler"
---

DVDStyler intègre tous les outils nécessaires pour créer des menus personnalisés pour les DVD vidéo. Plusieurs maquettes prédéfinies y sont proposées, et l'utilisateur pourra concevoir ses propres modèles. Plusieurs éléments des menus peuvent être personnalisés. L'utilisateur pourra choisir une photo dans sa bibliothèque et l'utiliser comme image de fond. La police des titres est aussi personnalisable. Les menus offrent un accès rapide aux différents chapitres d'un film. Le logiciel permet d'afficher un aperçu de chacun d'eux à l'aide d'extraits des vidéos contenues dans le disque. Les menus créés avec DVDStyler peuvent être agrémentés de divers éléments qui rendent la navigation plus aisée. Il est par exemple possible d'ajouter des boutons de déplacement entre les différents chapitres.

