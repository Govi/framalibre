---
nom: "AFWall+"
date_creation: "Samedi, 4 février, 2017 - 09:26"
date_modification: "Mercredi, 12 mai, 2021 - 15:41"
logo:
    src: "images/logo/AFWall+.png"
site_web: "https://github.com/ukanth/afwall/wiki"
plateformes:
    - "Android"
langues:
    - "English"
    - "Autres langues"
description_courte: "AFWall+ est un parefeu pour Android qui vous permettra de"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "pare-feu"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/afwall"
---

AFWall+ est un parefeu pour Android qui vous permettra de décider quelles applications auront accès à Internet. Cela peut être utile pour votre vie privée, en empêchant certaines applications de communiquer dans votre dos, mais également pour mieux contrôler l'utilisation de votre forfait data.
Le mode root est nécessaire pour utiliser AFWall+

