---
nom: "cirrus : Cloud Intègre, Responsable et Résilient pour Utilisateurs Sagaces"
date_creation: "Mercredi, 15 décembre, 2021 - 18:19"
date_modification: "Mercredi, 15 décembre, 2021 - 18:19"
logo:
    src: "images/logo/cirrus : Cloud Intègre, Responsable et Résilient pour Utilisateurs Sagaces.png"
site_web: "https://getcirrus.awebsome.fr/"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Android"
    - "Autre"
    - "le web"
langues:
    - "Français"
description_courte: "cirrus : Cloud Intègre, Responsable et Résilient pour Utilisateurs Sagaces"
createurices: "Julien Wilhelm"
alternative_a: "Google Drive, Box, Dropbox, Mega"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "écoresponsable"
    - "flat-file"
    - "responsive"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cirrus-cloud-int%C3%A8gre-responsable-et-r%C3%A9silient-pour-utilisateurs-sagaces"
---

Un cirrus est un type de nuage ayant hérité son nom latin de la forme qu'il rappelle, celle d'une boucle de cheveux. Au-delà de l'aspect poétique, c.i.r.r.u.s. est l'acronyme idéal du Cloud Intègre, Responsable et Résilient pour Utilisateurs Sagaces porté par Awebsome.

