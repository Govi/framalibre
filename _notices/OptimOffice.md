---
nom: "OptimOffice"
date_creation: "Vendredi, 21 avril, 2017 - 13:54"
date_modification: "Jeudi, 5 janvier, 2023 - 19:12"
logo:
    src: "images/logo/OptimOffice.png"
site_web: "https://doc.scenari.software/Optim"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Optim est une chaîne éditoriale dite «généraliste», c'est à dire qu'elle est adaptée à tou⋅te⋅s."
createurices: ""
alternative_a: "Microsoft Office, Microsoft Word, Microsoft PowerPoint"
licences:
    - "Licence CECILL (Inria)"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "bureautique"
    - "chaîne éditoriale"
    - "création de site web"
    - "diaporama"
    - "traitement de texte"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/optimoffice"
---

OptimOffice est une chaîne éditoriale simple d'utilisation pour réaliser des rapports, dossiers, site web, diaporama, documents PDF…
De nombreux formats de fichiers peuvent être insérés (tableaux, images, vidéos…)
Des outils variés permettent de créer un blog avec flux RSS pour la génération de site Web, d'insérer des galeries d'images / photos / arbre synoptique …
Des extensions permettent de créer des sites avec navigation par tuiles, ou des sites long-scrolling d'une seule page.
OptimOffice est un des modèles documentaires de Scenari.

