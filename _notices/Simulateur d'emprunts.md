---
nom: "Simulateur d'emprunts"
date_creation: "Lundi, 26 mars, 2018 - 11:40"
date_modification: "Mercredi, 12 mai, 2021 - 15:01"
logo:
    src: "images/logo/Simulateur d'emprunts.png"
site_web: "https://play.google.com/store/apps/details?id=org.gipilab.simulateuremprunts"
plateformes:
    - "Android"
langues:
    - "Français"
description_courte: "Simulateur d'emprunts Android avec calcul de l'inconnue et plusieurs profils de tableaux d'amortissements"
createurices: "Thibault Mondary, Gilbert Mondary"
alternative_a: ""
licences:
    - "Licence CECILL (Inria)"
tags:
    - "bureautique"
    - "simulation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/simulateur-demprunts"
---

Le simulateur d'emprunts conçu par le Laboratoire de Recherche pour le Développement Local assiste le candidat emprunteur dès la phase de recherche.
D'une utilisation simple et intuitive, il s'appuie sur le principe du « calcul de l'inconnue » : l'utilisateur renseigne la périodicité de remboursement ainsi que trois des quatre paramètres de l'emprunt (taux annuel, échéance, capital, durée) et le simulateur calcule le paramètre manquant, ainsi que les tableaux d'amortissements.
« si je rembourse telle somme pendant telle durée, combien puis-je emprunter ? »
« pour que je puisse emprunter cette somme, sur quelle durée dois-je m'endetter ? »
« quel sera le taux de mon emprunt si je limite les échéances à telle somme ? »
« à combien s'élèvent mes échéances si je réduis la durée de mon emprunt ? »
« quelle somme supplémentaire puis-je emprunter si j'allonge la durée de mon emprunt ? »

