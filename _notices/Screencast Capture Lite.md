---
nom: "Screencast Capture Lite"
date_creation: "Lundi, 8 mai, 2017 - 19:42"
date_modification: "Dimanche, 21 juillet, 2019 - 20:48"
logo:
    src: "images/logo/Screencast Capture Lite.png"
site_web: "http://cesarsouza.github.io/screencast-capture/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Screencast Capture Lite permet d'enregistrer l'écran du bureau Windows dans un fichier vidéo encodé en H264."
createurices: "César Roberto de Souza"
alternative_a: "Camstudio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "screencast"
    - "capture d'écran"
    - "capture vidéo"
    - "vidéographie"
    - "utilitaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/screencast-capture-lite"
---

Screencast Capture Lite est un outil permettant d'enregistrer l'écran du bureau Windows et de le sauvegarder dans un fichier vidéo, avec la meilleure  qualité possible. L'application encode son flux vidéo uniquement en H624 avec un réglage presque sans perte.
L'application permet l'enregistrement de tout l'écran, d'une fenêtre sélectionnée ou une zone définie de l'écran.

