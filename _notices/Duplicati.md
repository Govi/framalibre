---
nom: "Duplicati"
date_creation: "Mercredi, 22 mars, 2017 - 03:05"
date_modification: "Vendredi, 16 octobre, 2020 - 08:28"
logo:
    src: "images/logo/Duplicati.png"
site_web: "https://www.duplicati.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Synchronisation et sauvegarde chiffrées, multi-plateforme et interopérable."
createurices: "Kenneth Skovhede"
alternative_a: "Google Drive, Microsoft OneDrive, Dropbox, time machine, Mega"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "système"
    - "synchronisation"
    - "sauvegarde"
    - "chiffrement"
    - "vie privée"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Duplicati"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/duplicati"
---

Duplicati est un logiciel de sauvegarde qui planifie et stocke une copie complète ou incrémentielle des fichiers à conserver. La copie de sauvegarde peut être compressée, chiffrée et stockée localement, en ligne, depuis des services de cloud computing ou vers des serveurs de fichiers distants. Duplicati supporte les différents services de sauvegarde en ligne comme OneDrive, Amazon S3, Rackspace, Cloud Files, Tahoe LAFS, Google Documents et Tardigrade (via le réseau de stockage distribué Storj), mais aussi tous les serveurs qui prennent en charge SFTP, WebDAV ou FTP.
Duplicati utilise des composants standards tels que rdiff, la compression ZIP avec Deflate, le chiffrement de type AES avec AES Crypt et GnuPG. Cela permet de récupérer des fichiers de sauvegarde, même si Duplicati n'est plus disponible.

