---
nom: "ProxMox Virtual Environnement"
date_creation: "Lundi, 30 janvier, 2017 - 00:35"
date_modification: "Vendredi, 5 avril, 2019 - 16:16"
logo:
    src: "images/logo/ProxMox Virtual Environnement.png"
site_web: "https://www.proxmox.com/en/proxmox-ve"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Une solution incontournable pour la gestion de machines virtuelles, conteneurs LXC 'auto hébergement."
createurices: ""
alternative_a: "VMWARE VSPHERE, MICROSOFT HYPER-V"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "système"
    - "serveur"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Proxmox_VE"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/proxmox-virtual-environnement"
---

ProxMox VE - Virtual Environnement - est un hyperviseur permettant la gestion de machines virtuelles (KVM) et conteneurs Linux (LXC) sur une seule machine physique. Les ressources (CPU, mémoire, disques, etc.) sont alors partagées entre les différentes VM. C'est un logiciel libre sous licence AGP.
La gestion des VM s'effectue simplement à travers un navigateur web permettant l'accès console ou grpahique à chaque VM, le contrôle de ses ressources, etc. Le système est basé sur la distribution Debian GNU/Linux. Les machines virtuelles peuvent être en 32 ou 64 bits, Windows, GN/Linux et autres.
Plusieurs machines physiques peuvent être jumelées en grappe pour créer un "datacenter" et offrir la haute disponibilité avec la possibilité de déplacer à chaud une VM d'un serveur physique à un autre, de gérer la sauvegarde des VM, de les cloner, etc.
Le soutien technique commercial payant est disponible en option.

