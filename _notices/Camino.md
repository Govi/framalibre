---
nom: "Camino"
date_creation: "Vendredi, 25 novembre, 2022 - 16:39"
date_modification: "mardi, 2 janvier, 2024 - 10:13"
logo:
    src: "images/logo/Camino.png"
site_web: "https://www.philnoug.com/camino"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
description_courte: "Gérez vos projets en équipe et en mobilité"
createurices: "Philippe NOUGAILLON"
alternative_a: "Redmine, Todoist, Basecamp, Asaana, Microsoft Project, OpenProject"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "gestion de projet"
    - "gestion de tâches"
    - "planning"
    - "todo-list"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/camino"
---

Plateforme Web (PC/Smartphone) de gestion de projets, en équipe et en mobilité, qui simplifie votre suivi d'activité.
Découpez vos projets en listes de choses à faire, invitez vos contacts à participer, assignez aux participants certaines choses à faire et fixez les échéances.
Choix du type de déroulement (Workflow) : libre ou linéaire.
Suivez l'activité des participants (commentaires, travaux terminés, etc.).
Les participants peuvent mener des discussions autour de choses à faire.
Une notification (via courriel) peut être envoyée aux participants du projet après chaque modification, ajout ou suppression d'information, lors de l'assignation d'une tâche à un participant et avant chaque échéance.
Créez des formulaires pour collecter des données complémentaires à vos projets (inventaire, rapport d'intervention, consommation fournitures, etc.).
Domaine d'utilisation : GMAO (suivi des interventions), Planification de grands projets ou de projets longs et complexes.


