---
nom: "Smplayer"
date_creation: "Mardi, 27 décembre, 2016 - 22:46"
date_modification: "Jeudi, 19 août, 2021 - 13:29"
logo:
    src: "images/logo/Smplayer.png"
site_web: "http://smplayer.sourceforge.net/en/info"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Lecteur multimédia avec téléchargement de sous-titres, codecs inclus, etc."
createurices: ""
alternative_a: "Windows Media Player"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur multimédia"
    - "lecteur vidéo"
    - "lecteur musique"
    - "téléchargement de sous-titres"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/smplayer"
---

Smplayer est un lecteur multimédia. Comme VLC, il inclut les codecs multimédias donc quand on l'installe, il peut tout lire. Il se démarque par certaines fonctionnalités :
téléchargement de sous-titres depuis open-subtitles.org,
enregistrement de la position du film où on s'était arrêté,
lecteur pour YouTube,
options d'édition vidéo,
thèmes graphiques.

