---
nom: "Transmission"
date_creation: "Mercredi, 28 décembre, 2016 - 23:24"
date_modification: "Mercredi, 29 mars, 2017 - 23:45"
logo:
    src: "images/logo/Transmission.png"
site_web: "https://transmissionbt.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
langues:
    - "Autres langues"
description_courte: "Un client bittorrent léger et efficace."
createurices: ""
alternative_a: "µTorrent"
licences:
    - "Multiples licences"
tags:
    - "internet"
    - "torrent"
    - "téléchargement"
    - "bittorrent"
    - "client bittorrent"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Transmission_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/transmission"
---

Transmission est un client BitTorrent léger et efficace. Il ne demande que peu de ressources processeur, et prend peu de place en mémoire vive. 
Transmission permet le téléchargement et la création de torrents. Ce logiciel supporte les technologies décentralisées sans tracker, tel que PEX, DHT et les liens magnets.

