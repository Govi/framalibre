---
nom: "bludit"
date_creation: "Vendredi, 6 janvier, 2023 - 18:12"
date_modification: "Vendredi, 6 janvier, 2023 - 18:12"
logo:
    src: "images/logo/bludit.jpg"
site_web: "https://www.bludit.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Bludit est un CMS qui utilise des fichiers au format JSON"
createurices: ""
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "cms"
    - "php"
    - "json"
    - "blog"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/bludit"
---

Bludit est un CMS qui utilise des fichiers au format JSON pour stocker le contenu, vous n'avez pas besoin d'installer ou de configurer une base de données. Il intègre tous les outils SEO pour améliorer votre classement dans tous les moteurs de recherche et réseaux sociaux.
Bludit prend en charge le code Markdown et HTML pour le contenu, fournit également l'éditeur Markdown et l'éditeur WYSIWYG Conforme au RGPD
Bludit prend soin de la sécurité et de la confidentialité des utilisateurs. Bludit ne suit pas ou n'utilise pas de bibliothèques externes, de frameworks et d'autres ressources.Vous pouvez télécharger des thèmes, des plugins depuis le site officiel.

