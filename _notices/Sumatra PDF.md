---
nom: "Sumatra PDF"
date_creation: "Mardi, 10 janvier, 2017 - 11:50"
date_modification: "Mardi, 21 mai, 2019 - 22:15"
logo:
    src: "images/logo/Sumatra PDF.png"
site_web: "https://www.sumatrapdfreader.org/free-pdf-reader.html"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Visionneur de PDF (entre autre) simple d'utilisation et léger."
createurices: "Krzysztof Kowalczyk"
alternative_a: "Adobe Acrobat Reader"
licences:
    - "Licence Publique Générale Affero (AGPL)"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Multiples licences"
tags:
    - "bureautique"
    - "pdf"
    - "lecteur pdf"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Sumatra_PDF"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/sumatra-pdf"
---

Sumatra PDF permet de lire des fichiers PDF, eBook (ePub, Mobi), XPS, DjVu, CHM, bandes dessinées (CBZ et CBR) pour Windows.
Le logiciel est léger et son interface est très simple, ce qui rend cet outil terriblement efficace.
Il est aussi disponible en version portable (sans installation).

