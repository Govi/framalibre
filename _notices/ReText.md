---
nom: "ReText"
date_creation: "Samedi, 19 novembre, 2016 - 22:21"
date_modification: "Samedi, 4 août, 2018 - 17:06"
logo:
    src: "images/logo/ReText.png"
site_web: "https://github.com/retext-project/retext"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un éditeur Markdown et ReStructuredText simple et efficace."
createurices: "Dmitry Shachnev, Maurice van der Pot"
alternative_a: "TextMate"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "markdown"
    - "éditeur"
    - "traitement de texte"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/retext"
---

ReText est multiplateforme et écrit en Python. Il permet d'éditer des documents au balisage léger, en particulier le Markdown, et peut afficher le rendu HTML, en écran partagé, immédiatement à l'arrêt de la saisie (ce qui permet de continuer à travailler sur un document volumineux sans que l'application ne gèle). Ce logiciel offre également la possibilité d'afficher le code HTML correspondant et d'exporter le document final aux formats HTML, ODT et PDF.
Son interface est notamment traduite en français, et il incorpore un correcteur orthographique dont on peut choisir la langue, comme fr_FR par exemple.
De nombreuses autres fonctionnalités sont disponibles, telles que :
* la sauvegarde automatique
* l'affichage des numéros de ligne
* le surlignage de la ligne courante
* les coordonnées du curseur de clavier
* …
Présent dans de nombreux dépôts de distributions GNU+Linux (Debian, Trisquel, etc.), l'installation s'en trouve simplifiée.
Une version portable de ReText existe.

