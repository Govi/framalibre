---
nom: "Z-Anatomy"
date_creation: "Vendredi, 18 février, 2022 - 17:55"
date_modification: "Mardi, 22 février, 2022 - 08:28"
logo:
    src: "images/logo/Z-Anatomy.png"
site_web: "https://www.z-anatomy.com/"
plateformes:

langues:

description_courte: "No ad, no fee, no cookie, no subscription; only anatomy."
createurices: "'BodyParts3D', 'Z-Anatomy'"
alternative_a: "Biodigital human, 3D4Medical, Visible Body"
licences:
    - "Creative Commons -By-Sa"
tags:
    - "éducation"
    - "education"
    - "science"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/z-anatomy"
---

Z-Anatomy, ce sont d'abord les meilleurs fichiers 3D open-source d'anatomie humaine, réparés et organisés dans Blender, selon la dernière édition de la 'Terminologia anatomica' (TA2-2019) avec des fonctionnalités sur-mesure créées par un script python:https://www.z-anatomy.com/
C'est aussi une application mobile intuitive, créée avec Unity, à partir de ce contenu:https://play.google.com/store/apps/details?id=com.LluisVinent.ZAnatomy

