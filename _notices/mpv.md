---
nom: "mpv"
date_creation: "Mardi, 27 décembre, 2016 - 10:34"
date_modification: "Mardi, 25 avril, 2017 - 21:43"
logo:
    src: "images/logo/mpv.png"
site_web: "https://mpv.io"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "English"
description_courte: "Un fork de mplayer2 et MPlayer. Mpv est moins austère, et est un excellent lecteur multimédia."
createurices: ""
alternative_a: "Windows Media Player"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur vidéo"
    - "lecteur multimédia"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mpv"
---

Un fork de mplayer2 et MPlayer avec fonctionnalités revues et améliorées.
Mpv est tout aussi minimaliste que ses frères, mais un peu moins austère et plus facile à utiliser : options en ligne de commande repensées, panneau de contrôle amélioré, divers filtres de sortie video... et au développement actif !

