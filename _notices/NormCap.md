---
nom: "NormCap"
date_creation: "Samedi, 4 juin, 2022 - 13:28"
date_modification: "Samedi, 4 juin, 2022 - 13:28"
logo:
    src: "images/logo/NormCap.png"
site_web: "https://dynobo.github.io/normcap/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "NormCap est un logiciel de Reconnaissance optique de caractères (OCR)."
createurices: "Dynobo"
alternative_a: "ABBYY FineReader"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/normcap"
---

Il permet d'extraire du texte depuis une image, un livre numérique, une fenêtre affichée sur l'écran, un PDF etc.
Il suffit de sélectionner, sur l'écran de l'ordinateur, le texte à extraire, pour qu'il soit copié dans le presse-papier. Ensuite il faut le coller dans un logiciel de traitement de texte.

