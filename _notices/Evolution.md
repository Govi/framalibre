---
nom: "Evolution"
date_creation: "Lundi, 16 janvier, 2017 - 20:08"
date_modification: "jeudi, 28 décembre, 2023 - 14:56"
logo:
    src: "images/logo/Evolution.png"
site_web: "https://wiki.gnome.org/Apps/Evolution/"
plateformes:
    - "GNU/Linux"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Evolution : Le client de messagerie qui n'a rien a envier aux autres."
createurices: "Ximian, le projet GNOME"
alternative_a: "Microsoft Outlook, IBM LotusNote"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "internet"
    - "gnome"
    - "client mail"
    - "agenda"
    - "groupware"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Evolution_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/evolution"
---

Evolution est un logiciel de groupe de travail qui inclut un client de messagerie, la gestion des contacts, d'un ou plusieurs agendas, de prise de notes et de tâches. Il n'a rien a envier à Microsoft Outlook.

Il est compatible avec Exchange Server avec un connecteur en utilisant le protocole privateur de Microsoft mais également avec celui de Novell.
Il permet également d’importer les fichiers .pst si sensible en entreprise.

Fonctionnalités principales d'Evolution :
- connectivité intégrée avec Novell GroupWise et Microsoft Exchange;
- fonctionnement hors-ligne amélioré pour les comptes IMAP ;
- nombreuses évolutions du calendrier ;
- support de S/MIME, gestion des contacts amélioré ;
- intégration de Pidgin et de Gajim ;
- intégration améliorée au Bureau ;
- conformité accrue avec les directives d'interface humaines de GNOME ;
- fonctionnalité SpamAssassin intégrée (avec niveau de spam et règles définis par l'utilisateur dans ~/.spamass


