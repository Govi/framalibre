---
nom: "Spacebar"
date_creation: "Mardi, 27 juillet, 2021 - 19:12"
date_modification: "mardi, 13 février, 2024 - 16:17"
logo:
    src: "images/logo/Spacebar.png"
site_web: "https://spacebar.chat/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Un logiciel libre, open source et compatible avec le chat Discord."
createurices: "Flam3rboy, xnacly"
alternative_a: "Discord"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "internet"
    - "chat"
    - "visioconférence"
    - "vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fosscord"
---

Spacebar est un logiciel open source libre compatible avec Discord. Il s'agit d'une plateforme de chat, voix et vidéo similaire à Slack, Rocket.chat et compatible avec Discord.
Il est :
Auto-hébergeable
Configurable
Sécurisé
Décentralisé
Extensible
Thématisable
