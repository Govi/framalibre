---
nom: "Fortran Package Manager (fpm)"
date_creation: "Mercredi, 8 mars, 2023 - 10:56"
date_modification: "Mercredi, 8 mars, 2023 - 10:56"
logo:
    src: "images/logo/Fortran Package Manager (fpm).png"
site_web: "https://fpm.fortran-lang.org/fr/index.html"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "Construisez et diffusez votre projet Fortran avec fpm"
createurices: "Communauté Fortran-lang"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "science"
    - "fortran"
    - "compilation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fortran-package-manager-fpm"
---

Le gestionnaire de paquets Fortran fpm (Fortran Package Manager) est à la fois un gestionnaire de paquets et un système de construction de projet. Son objectif principal est d’améliorer l’expérience utilisateur des programmeurs Fortran. Il facilite la construction de votre programme ou de votre bibliothèque Fortran, l’exécution des exécutables, des tests et des exemples, et la distribution en tant que dépendance à d’autres projets Fortran. L’interface utilisateur de fpm s’inspire de Cargo, le gestionnaire de paquets de Rust. L’objectif à long terme est de favoriser et développer l’écosystème des applications et bibliothèques écrites en Fortran moderne.

