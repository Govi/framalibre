---
nom: "To Do"
date_creation: "Lundi, 24 avril, 2017 - 16:53"
date_modification: "Lundi, 10 mai, 2021 - 14:57"
logo:
    src: "images/logo/To Do.png"
site_web: "https://wiki.gnome.org/Apps/Todo"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "Autres langues"
description_courte: "Gestionnaire de tâches personnelles"
createurices: ""
alternative_a: "Microsoft To-Do, Wunderlist, Todoist"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "tâche"
    - "productivité"
    - "prise de notes"
    - "todo-list"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/do"
---

Gestionnaire de tâches qui offre la possibilité de créer des sous-tâches, de définir différentes priorités, d'ajouter des notes... Il est également extensible par le biais de greffons (comme la prise en charge des fichiers Todo.txt) et permet une synchronisation sur des services en ligne tels que Google ou Nextcloud.

