---
nom: "OpenStreetMap"
date_creation: "mercredi, 27 décembre, 2023 - 13:40"
date_modification: "mercredi, 27 décembre, 2023 - 13:40"
logo:
    src: "images/logo/OpenStreetMap.svg"
site_web: "https://www.openstreetmap.org/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "OpenStreetMap est une carte du monde, créée par des gens comme vous et libre d’utilisation sous licence libre."
createurices: ""
alternative_a: "GoogleMaps"
licences:
    - "Licence de base de données ouverte (OdbL)"
tags:
    - "cartographie"
    - "carte géographique"
    - "géographie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenStreetMap"
lien_exodus: ""
identifiant_wikidata: "Q936"
mis_en_avant: "non"

---

OpenStreetMap est une base de données cartographiques sous licence libre, réutilisable pour des milliers de sites web, d’applications mobiles et d’appareils, par exemple [OrganicMaps](https://framalibre.org/notices/organic-maps.html).

Tout le monde, quelles que soient ses compétences, peut contribuer aux données de routes, sentiers, cafés, stations ferroviaires et bien plus encore, partout dans le monde.
