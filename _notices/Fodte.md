---
nom: "Fodte"
date_creation: "Vendredi, 26 mars, 2021 - 23:45"
date_modification: "Vendredi, 26 mars, 2021 - 23:46"
logo:
    src: "images/logo/Fodte.png"
site_web: "https://github.com/DegrangeM/Fodte/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Fodte est un outil permettant d'extraire le contenu des champs de formulaires de fichiers Libreoffice Writer."
createurices: "Mathieu Degrange"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
    - "formulaire"
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fodte"
---

Fodte (Form Odt Exporter) est une application web permettant d'extraire le contenu des champs de formulaires de documents Libreoffice Writer et d'en générer un fichier tableur (csv).
L'application peut être utilisée via sa version en ligne ou bien être téléchargée pour une utilisation hors-ligne.

