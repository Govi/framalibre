---
nom: "LibreCAD"
date_creation: "Lundi, 2 janvier, 2017 - 07:51"
date_modification: "Lundi, 2 janvier, 2017 - 07:51"
logo:
    src: "images/logo/LibreCAD.png"
site_web: "http://librecad.org/cms/home.html"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "LibreCAD est un logiciel de dessin assisté par ordinateur ( DAO ) qui permet de faire des plans 2D."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "dao"
    - "2d"
    - "dessin technique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/librecad"
---

LibreCAD permet de modéliser des dessins techniques dans les domaines de la mécanique, de l'architecture, etc. Il permet entre autres l'utilisation de calques ainsi que de groupes de différents éléments, la création de formes complexes et de chanfreins et possède un système d'accroche complet.
Son format natif est le format DXF (R12/2007) inventé par Autodesk (éditeur d'AutoCAD)1). Concernant le format DWG, il est accessible de manière expérimentale en lecture et l'écriture est en cours d’implémentation.

