---
nom: "Pyradio"
date_creation: "Dimanche, 3 mai, 2020 - 15:55"
date_modification: "Dimanche, 3 mai, 2020 - 16:00"
logo:
    src: "images/logo/Pyradio.png"
site_web: "https://github.com/coderholic/pyradio"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Lecteur Radio en Flux , simple a utilisé et a configurer , le tout en Console !"
createurices: ""
alternative_a: ""
licences:
    - "Domaine public"
tags:
    - "multimédia"
    - "radio"
    - "cli"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pyradio"
---

Lecteur Radio en Flux , simple a utilisé et a configurer , le tout en Console !
Consomme peu de ressource , il suffit simplement d'ajouter les adresse flux dans le dossier
/home/user/.config/pyradio/stations.csv
exemple d'adresse : http://fluxradios.blogspot.com/p/flux-radios-francaise.html

