---
nom: "MuPhyN"
date_creation: "Lundi, 21 février, 2022 - 10:04"
date_modification: "Lundi, 21 février, 2022 - 10:23"
logo:
    src: "images/logo/MuPhyN.png"
site_web: "https://gitlab.com/Cerisic/muphyn"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "MuPhyN - Multi Physical Nexus - est un logiciel de simula"
createurices: "François Demoulin"
alternative_a: "Simulink"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "science"
    - "simulation"
    - "education"
    - "mathématiques"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/muphyn"
---

MuPhyN - Multi Physical Nexus - est un logiciel de simulation écrit en python.
Il permet à l'utilisateur de placer des boxes dans une interfaces et de les lier entre elles dans le but de visionner des résultats.
Il est encore en développement mais la version est assez stable que pour l'utiliser correctement.
Le système de librairies est un plus par rapport à ses alternatives. Effectivement, il est aisé de rajouter une box étant donné qu'elles sont développé en python. De plus, le système de librairies est étendu aux planificateurs. Il est donc possible de changer la manière et l'ordre dont les boxes sont appelée facilement.
Dans une version futur, il sera possible de créer des macro box (contenant une partie de la simulation) et de coder ses boxes ainsi que ses planificateurs directement dans l'application.

