---
nom: "Yotter"
date_creation: "Mercredi, 13 janvier, 2021 - 22:14"
date_modification: "Jeudi, 17 août, 2023 - 21:08"
logo:
    src: "images/logo/Yotter.png"
site_web: "https://github.com/ytorg/Yotter"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Un frontend pour YouTube et Twitter basé sur Invidious et Nitter."
createurices: "PLUJA"
alternative_a: "Youtube, Twitter"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "internet"
    - "twitter"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/yotter"
---

Yotter est un frontend à YouTube et Twitter écrit en Python conçu pour respecter la vie privée et être minimaliste. Il permet de faire des recherches de contenus en provenance de YouTube ainsi que de Twitter et permet également de s'abonner à des comptes de ces 2 sites (ce que ne permet pat Nitter). Toutes les publicités sont bloquées, le client ne fait pas de requêtes vers les serveurs de YouTube/Twitter et le service peut fonctionner sans JavaScript.
Ce service est également conçu pour fonctionner correctement avec des navigateurs web en ligne de commande et est hébergeable sur des services cachés Tor.

