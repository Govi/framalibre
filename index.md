---
layout: welcome
styles:
    - href: "style/welcome.css"
scripts:
    - src: "build/welcome.js"
      defer: true
---

{% assign welcomeTags = "association,bloqueur de publicité,client mail,création de site web,dessin,fediverse,formulaire,gestion de projet,jeu,texte,travail collaboratif,vidéo,visioconférence" | split: "," %}

<main id="f-page" class="container">
  <h1>
    <span class="fl-animated-title">
      <span class="fl-animated-title__fixed-item">Trouvez</span>
      <span class="fl-animated-title__fixed-item">
        <span class="fl-animated-title__moving-item">un outil</span>
        <span aria-hidden="true">,</span>
        <span class="fl-animated-title__moving-item">une appli</span>
        <span aria-hidden="true">,</span>
        <span class="fl-animated-title__moving-item">du matériel</span>
        <span aria-hidden="true">,</span>
        <span class="fl-animated-title__moving-item">un logiciel</span>
      </span>
      <span class="fl-animated-title__fixed-item">libre</span>
    </span>
  </h1>

  <div class="fl-accueil row mb-2">
    <div class="col-lg-8">
      <h2 class="text-center">Via les étiquettes…</h2>

      <ul class="fl-tags">
        {% for tag in welcomeTags -%}
          <li class="fl-tags__items">{%- include tag-link.html tag=tag -%}</li>
        {%- endfor -%}
      </ul>
      <!-- Pour changer la liste d'étiquettes, ça se passe tout en haut de la page -->

      <h2 class="text-center">Ou plutôt la recherche !</h2>

      <section
        class="mx-auto my-0 rounded fb-g2 py-3 px-2 col-sm-8 col-lg-8 col-10"
      >
        <div class="row mx-0">
          <div class="px-1 col-10">
            <input
              id="services-search"
              type="search"
              placeholder="Rechercher"
              class="fl-search form-control"
            />
          </div>
          <label class="fc-g7 pt-1 pl-2 col-2" for="services-search">
            <i class="fl-magnifying-icon fas fa-lg"></i>
            <span class="sr-only">Rechercher</span>
          </label>
        </div>
      </section>
    </div>
    <div class="col-lg-4">
      <img src="{{'images/gnu-tux.png' | relative_url}}" alt="">
    </div>
  </div>

  <output></output>

  <!--
    Et genre aussi y'a
     - [une page avec tous les mots-clefs](./all-tags)
     - [une page avec tous les mots-clefs pour lesquels il n'y a qu'une seule page](./tag-une-notice)
  -->
</main>

<section class="mini-sites-phares welcome-section">
  <div class="welcome-section__container row">
    <div class="w-100">
      <h2 class="welcome-section__title">Par où commencer ?</h2>
    </div>
    <div class="welcome-section__content"> 
      <section id="">
        <div class="px-md-0 container">  
          <div class="row m-0">
            <div class="row mx-0">
              <div class="bloc1 mb-4 col-lg-4">
                <div class="p-4 h-100 d-flex flex-column">
                  <div class="my-2">
                    <h3 id="lémancipation-numérique">Protéger ma vie privée</h3>
                    <p><strong>Pour découvrir des logiciels respectueux de vos libertés</strong></p>
                  </div> 
                  <p class="mt-auto mb-2 text-right">
                    <a href="https://gavy.monpetitsite.org/proteger-sa-vie-privee/" target="_self" class="btn btn-outline-warning"><span>Voir le mini-site</span> <i class="fas fa-chevron-right"></i></a>
                  </p>
                </div>
              </div>
              <div class="bloc2 mb-4 col-lg-4">
                <div class="p-4 h-100 d-flex flex-column">
                  <div class="my-2">
                    <h3 id="léducation-populaire">Outiller ma famille</h3>
                    <p><strong>Pour partager des outils utiles à vos proches</strong></p>
                  </div> 
                  <p class="mt-auto mb-2 text-right">
                    <a href="https://maiwann.monpetitsite.org/des-outils-pour-ma-famille/" target="_self" class="btn btn-outline-warning"><span>Voir le mini-site</span> <i class="fas fa-chevron-right"></i></a>
                  </p>
                </div>
              </div>
              <div class="bloc3 mb-4 col-lg-4">
                <div class="p-4 h-100 d-flex flex-column">
                  <div class="my-2">
                    <h3 id="larchipélisation">Libérer votre association</h3>
                    <p><strong>Pour collaborer, communiquer, s'organiser au sein de votre collectif</strong></p>
                  </div> 
                  <p class="mt-auto mb-2 text-right"><a href="https://maiwann.monpetitsite.org/logiciels-et-services-libres-pour-une-association/" target="_self" class="btn btn-outline-warning"><span>Voir le mini-site</span> <i class="fas fa-chevron-right"></i></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
  </div>
</section>

<section class="creer-mini-site welcome-section">
  <div class="welcome-section__container row">
    <div class="w-100">
      <h2 class="welcome-section__title">Recommandez vos outils préférés !</h2>
    </div>
    <div class="welcome-section__content">
      <div class="col-lg-4">
        <img src="{{'images/construire_mini_site.png' | relative_url}}" alt="">
      </div>
      <div class="col-lg-6">
        <h2 class="mt-2 mb-2">Et maintenant, c'est à vous de jouer&nbsp;!</h2>
        <p class="my-2">Nous avons besoin de vous pour continuer à faire savoir que oui, il existe <strong>un numérique respectueux de ses utilisateurs et utilisatrices.</strong></p>
        <p>Pour cela, vous pouvez recommander autour de vous les outils libres que vous aimez utiliser à l'aide <strong>d'un petit outil que nous vous avons concocté !</strong></p>
        <p class="my-5 mb-2 text-center">
          <a href="https://framalibre.org/mini-site" target="_self" class="btn btn-outline-warning"><span>Créer votre mini-site</span> <i class="fas fa-chevron-right"></i></a>
        </p>
      </div>
    </div>
  </div>
</section>



<section class="liste-mini-sites welcome-section">
  <div class="welcome-section__container row">
    <div class="w-100">
      <h2 class="welcome-section__title">Si vous en voulez encore…</h2>
    </div>
    <div class="welcome-section__content">
      <section id="">
        <ul class="row list-unstyled my-0">
          <li class="my-2 col-md-3">
            <div class="p-4 text-center">
              <a href="https://numahell.gitlab.io/applications-libres-sur-android">Des applications libres pour Android</a>
            </div>
          </li>
          <li class="my-2 col-md-3">
            <div class="p-4 text-center">
              <a href="https://berumuron.monpetitsite.org/lecteurs-de-flux/">Des lecteurs de flux Web</a>
            </div>
          </li>
          <li class="my-2 col-md-3">
            <div class="p-4 text-center">
              <a href="https://numahell.gitlab.io/cartographie-libre/">Outils libres pour la cartographie</a>
            </div>
          </li>
          <li class="my-2 col-md-3">
            <div class="p-4 text-center">
              <a href="https://framatophe.github.io/les-logiciels-preferes-de-framatophe/">Les logiciels préférés de Framatophe</a>
            </div>
          </li>
          <li class="my-2 col-md-3">
            <div class="p-4 text-center">
              <a href="https://lareinedeselfes.monpetitsite.org/lareinettescribouille">Les logiciels qui m'ont fait grandir</a>
            </div>
          </li>
          <li class="my-2 col-md-3">
            <div class="p-4 text-center">
              <a href="https://maiwann.monpetitsite.org/libre-sous-mac/">Libre sous Mac</a>
            </div>
          </li>
          <li class="my-2 col-md-3">
            <div class="p-4 text-center">
              <a href="https://maiwann.monpetitsite.org/outils-pour-les-designers/">Outils pour les designers</a>
            </div>
          </li>
          <!--
          <li class="my-2 col-md-3">
            <div class="p-4 text-center">
              <a href="#">Bientôt là</a>
            </div>
          </li>
          -->
        </ul>
      </section>
    </div>
  </div>
</section>
