import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';

const production = !process.env.ROLLUP_WATCH;

const plugins = [
	// If you have external dependencies installed from
	// npm, you'll most likely need these plugins. In
	// some cases you'll need additional configuration -
	// consult the documentation for details:
	// https://github.com/rollup/plugins/tree/master/packages/commonjs
	resolve({
		browser: true,
	}),
	commonjs(),

	// Watch the `public` directory and refresh the
	// browser on changes when not in production
	//!production && livereload('public'),

	// If we're building for production (npm run build
	// instead of npm run dev), minify
	production && terser()
]

export default [
	{
		input: 'front-end/welcome.js',
		output: {
			format: 'iife',
			file: 'build/welcome.js'
		},
		plugins,
		watch: {
			clearScreen: true
		}
	},
	{
		input: 'front-end/contribute.js',
		output: {
			format: 'iife',
			file: 'build/contribute.js'
		},
		plugins,
		watch: {
			clearScreen: true
		}
	},
	{
		input: 'front-end/notice.js',
		output: {
			format: 'iife',
			file: 'build/notice.js'
		},
		plugins,
		watch: {
			clearScreen: true
		}
	}
]
