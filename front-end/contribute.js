//@ts-check

import { text, json } from "d3-fetch";
import { format } from "date-fns";
import { fr } from "date-fns/locale";

import createNoticeContent from "../shared/createNoticeContent.js";

//const CONTRIBUTION_SERVER_ORIGIN = 'http://localhost:8080' // dev
const CONTRIBUTION_SERVER_ORIGIN = "https://contribuer.framalibre.org"; // prod

const baseUrl = document.documentElement.getAttribute("data-base-relative");

const TAG_INPUT_ELEMENTS_SELECTOR = `[name="tag"]`;

const form = document.querySelector(`form.contribute`);
if (!form)
  throw new Error(
    "form.contribute element could not be found, cannot contribute",
  );

/** @type {HTMLInputElement | null} */
const dateCreationEl = form.querySelector(`[name="date_creation"]`);
/** @type {HTMLInputElement | null} */
const logoSrcEl = form.querySelector(`[name="logo_src"]`);
/** @type {HTMLInputElement | null} */
const nomEl = form.querySelector(`[name="nom"]`);
/** @type {HTMLInputElement | null} */
const logoEl = form.querySelector(`[name="logo"]`);
/** @type {HTMLInputElement | null} */
const siteWebEl = form.querySelector(`[name="site_web"]`);
/** @type {NodeListOf<HTMLOptionElement>} */
const plateformesEls = form.querySelectorAll(`[name="plateformes"]`);
/** @type {NodeListOf<HTMLOptionElement>} */
const languesEls = form.querySelectorAll(`[name="langues"]`);
/** @type {HTMLInputElement | null} */
const descriptionCourteEl = form.querySelector(`[name="description_courte"]`);
/** @type {HTMLInputElement | null} */
const createuricesEl = form.querySelector(`[name="createurices"]`);
/** @type {HTMLInputElement | null} */
const alternativeAEl = form.querySelector(`[name="alternative_a"]`);
/** @type {HTMLInputElement | null} */
const licencesEl = form.querySelector(`[name="licences"]`);
/** @type {HTMLInputElement | null} */
const lienWikipediaEl = form.querySelector(`[name="lien_wikipedia"]`);
/** @type {HTMLInputElement | null} */
const lienExodusEl = form.querySelector(`[name="lien_exodus"]`);
/** @type {HTMLInputElement | null} */
const identifiantWikidataEl = form.querySelector(
  `[name="identifiant_wikidata"]`,
);
/** @type {HTMLInputElement | null} */
const descriptionLongueEl = form.querySelector(`[name="description_longue"]`);
/** @type {HTMLInputElement | null} */
const contributeuriceEl = form.querySelector(`[name="contributeur.rice"]`);
/** @type {HTMLInputElement | null} */
const submitButton = document.querySelector('button[type="submit"]');

if (
  !dateCreationEl ||
  !logoSrcEl ||
  !nomEl ||
  !logoEl ||
  !siteWebEl ||
  !descriptionCourteEl ||
  !createuricesEl ||
  !alternativeAEl ||
  !licencesEl ||
  !lienWikipediaEl ||
  !lienExodusEl ||
  !identifiantWikidataEl ||
  !descriptionLongueEl ||
  !contributeuriceEl ||
  !submitButton
) {
  throw new TypeError("Un élément du formulaire est manquant");
}

/**
 *
 * @param {string} title
 * @returns
 */
function titleToFilePath(title) {
  return title.replace(/\//g, "-").replace(/#/g, "-").replace(/\?/g, "-");
}

const paramsSearch = new URLSearchParams(window.location.search);
const noticeToEdit = paramsSearch.get("notice");

/** @type {JekyllNotice} */
let notice;

let action = "create";

if (noticeToEdit) {
  const noticeURL = `${baseUrl}notices/${encodeURIComponent(noticeToEdit)}.json`;

  json(noticeURL)
    .then((_notice) => {
      //@ts-ignore
      notice = _notice;
      // normalize notice in case some values weren't filled
      notice.plateformes = notice.plateformes || [];
      notice.langues = notice.langues || [];
      notice.licences = notice.licences || [];
      notice.tags = notice.tags || [];

      action = "update";
      dateCreationEl.value = notice.date_creation;
      logoSrcEl.value = notice.logo?.src || "";
      nomEl.value = notice.nom;
      siteWebEl.value = notice.site_web || "";

      // @ts-ignore
      for (const plateformEl of plateformesEls) {
        if (notice.plateformes.includes(plateformEl.value)) {
          plateformEl.checked = true;
        }
      }

      // @ts-ignore
      for (const langueEl of languesEls) {
        if (notice.langues.includes(langueEl.value)) {
          langueEl.checked = true;
        }
      }

      descriptionCourteEl.value = notice.description_courte || "";
      createuricesEl.value = notice.createurices || "";
      alternativeAEl.value = notice.alternative_a || "";
      // @ts-ignore
      const licencesOptions = [...licencesEl.options];
      licencesOptions.forEach((o) => {
        if (notice.licences.includes(o.value)) {
          o.selected = true;
        }
      });
      lienWikipediaEl.value = notice.lien_wikipedia || "";
      lienExodusEl.value = notice.lien_exodus || "";
      descriptionLongueEl.value = notice.raw_content || "";
      identifiantWikidataEl.value = notice.identifiant_wikidata || "";

      notice.tags.forEach((tag, i) => {
        /** @type {NodeListOf<HTMLInputElement>} */
        const tagsEls = form.querySelectorAll(TAG_INPUT_ELEMENTS_SELECTOR);

        /** @type {HTMLInputElement?} */
        let tagInputElement = tagsEls[i];

        if (tagInputElement === undefined) {
          /** @type {HTMLInputElement} */
          // @ts-ignore
          const clone = tagsEls[0].cloneNode();
          tagInputElement = clone;
          // @ts-ignore
          [...tagsEls].at(-1).after(tagInputElement);
        }

        tagInputElement.value = tag;
      });

      submitButton.textContent = "Modifier la notice";
    })
    .catch((err) => {
      console.error("TODO: handle error case (404, etc.)", err);
    });
}

form.addEventListener("submit", async (e) => {
  e.preventDefault();

  submitButton.disabled = true;
  submitButton.textContent = "Envoi en cours...";

  const nom = nomEl.value;
  const site_web = siteWebEl.value;
  // @ts-ignore
  const plateformes = [...plateformesEls]
    .filter((el) => el.checked)
    .map((o) => o.value);
  // @ts-ignore
  const langues = [...languesEls]
    .filter((el) => el.checked)
    .map((o) => o.value);
  const description_courte = descriptionCourteEl.value;
  const createurices = createuricesEl.value;
  const alternative_a = alternativeAEl.value;
  // @ts-ignore
  const licences = [...licencesEl.selectedOptions].map((o) => o.value);

  const tagsEls = form.querySelectorAll(TAG_INPUT_ELEMENTS_SELECTOR);

  const tags = new Set(
    // @ts-ignore
    [...tagsEls].map((el) => el.value).filter((t) => t.length >= 1),
  );

  const lien_wikipedia = lienWikipediaEl.value;
  const lien_exodus = lienExodusEl.value;
  const identifiant_wikidata = identifiantWikidataEl.value;
  const description_longue = descriptionLongueEl.value;
  const contributeurice = contributeuriceEl.value;
  const date_modification = format(new Date(), "EEEE, d MMMM, yyyy - HH:mm", {
    locale: fr,
  });
  const date_creation = noticeToEdit ? dateCreationEl.value : date_modification;
  const hasLogo = logoEl.files && logoEl.files.length > 0;
  /** @type {string} */
  let imageFilePath = "";
  /** @type {string} */
  let base64Logo = "";
  if (hasLogo) {
    const file = logoEl.files && logoEl.files[0];
    if (!file) {
      throw new TypeError("Fichier manquant");
    }
    const extension = file.name.split(".").pop();
    imageFilePath = `images/logo/${titleToFilePath(nom)}.${extension}`;

    const logo = await file.arrayBuffer();

    base64Logo = btoa(
      new Uint8Array(logo).reduce(
        (data, byte) => data + String.fromCharCode(byte),
        "",
      ),
    );
  } else if (noticeToEdit && logoSrcEl.value) {
    imageFilePath = logoSrcEl.value;
  }

  const contenu = createNoticeContent({
    nom,
    date_creation,
    date_modification,
    logo: { src: imageFilePath },
    site_web,
    plateformes,
    langues,
    description_courte,
    createurices,
    alternative_a,
    licences,
    tags,
    lien_wikipedia,
    lien_exodus,
    identifiant_wikidata,
    description_longue,
    mis_en_avant: (notice && notice.mis_en_avant === "oui") || false,
    redirect_from: (notice && notice.redirect_from) || undefined,
  });

  const file_path = `_notices/${titleToFilePath(nom)}.md`;
  const previous_path = notice && `_notices/${titleToFilePath(notice.nom)}.md`;

  const differentContentFile =
    notice && titleToFilePath(nom) !== titleToFilePath(notice.nom);

  const actions = [
    {
      action: differentContentFile ? "move" : notice ? "update" : "create",
      file_path,
      previous_path,
      content: contenu,
      encoding: "text",
    },
  ];

  if (base64Logo && imageFilePath) {
    const previousImageFilePath = notice && notice.logo && notice.logo.src;

    actions.push({
      action:
        previousImageFilePath && imageFilePath !== previousImageFilePath
          ? "move"
          : previousImageFilePath
            ? "update"
            : "create",
      file_path: imageFilePath,
      // @ts-ignore
      previous_path: previousImageFilePath,
      content: base64Logo,
      encoding: "base64",
    });
  }

  text(`${CONTRIBUTION_SERVER_ORIGIN}/contribute`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      contributor: contributeurice,
      actions,
    }),
  })
    .then(() => {
      window.location.href = noticeToEdit
        ? `${baseUrl}contribuer/confirmation_de_modification`
        : `${baseUrl}contribuer/confirmation_de_creation`;
    })
    .catch((err) => {
      console.error(
        `Erreur lors de l'envoi de la contribution au serveur de contribution`,
        err,
      );

      submitButton.disabled = false;

      submitButton.textContent = noticeToEdit
        ? "Modifier la notice"
        : "Créer la notice !";

      /** @type {HTMLElement | null} */
      const alertEl = form.querySelector(`#alert`);
      if (!alertEl) {
        throw new TypeError("Élement #alert manquant");
      }
      alertEl.hidden = false;

      alertEl.innerHTML = `
        <i class="fas fa-circle-xmark"></i>
        <strong>Il y a un souci de notre côté.</strong>
        Vous pouvez réessayer
        dans une heure ou demain. Si le problème persiste, vous
        pouvez
        <a href="https://contact.framasoft.org/fr/#framalibre">contacter
        l'équipe de Framalibre</a>.
        `;
    });
});
