//@ts-check

import lunr from "lunr";
import stemmerSupport from "lunr-languages/lunr.stemmer.support";
import lunrfr from "lunr-languages/lunr.fr";
import { json } from "d3-fetch";
import debounce from "just-debounce-it";

import "../shared/types.js";

stemmerSupport(lunr);
lunrfr(lunr);

/***********/
/* Helpers */
/***********/

/**
 *
 * @param {string} str
 * @returns {string}
 */
const removeAccents = (str) => {
  return typeof str === "string"
    ? str.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    : "";
};

/**
 * PPP insufficient, but enough for now
 * @param {any} x
 * @returns {x is JekyllNotice}
 */
function isJekyllNotice(x) {
  return (
    typeof x.id === "string" &&
    typeof x.url === "string" &&
    typeof x.nom === "string" &&
    typeof x.description_courte === "string"
  );
}

/**
 * @param {any} x
 * @returns {x is JekyllNotice[]}
 */
function isJekyllNoticeArray(x) {
  return Array.isArray(x) && x.every(isJekyllNotice);
}

/********/
/* Data */
/********/
const baseUrl = document.documentElement.getAttribute("data-base-relative");
const noticesURL = `${baseUrl}notices.json`;
const noticesP = json(noticesURL).then((notices) => {
  if (isJekyllNoticeArray(notices)) {
    return notices;
  } else {
    throw new TypeError("Erreur de type dans les notices récupérées");
  }
});
const welcomeTagsString = removeAccents(
  "association,bloqueur de publicité,création de site web,dessin,fediverse,formulaire,gestion de projet,jeu,mail,texte,travail collaboratif,vidéo,visioconférence",
);
const welcomeTags = welcomeTagsString.split(",");

// from https://github.com/MihaiValentin/lunr-languages/blob/master/lunr.fr.js
const frStopWordsString = removeAccents(
  "ai aie aient aies ait as au aura aurai auraient aurais aurait auras aurez auriez aurions aurons auront aux avaient avais avait avec avez aviez avions avons ayant ayez ayons c ce ceci celà ces cet cette d dans de des du elle en es est et eu eue eues eurent eus eusse eussent eusses eussiez eussions eut eux eûmes eût eûtes furent fus fusse fussent fusses fussiez fussions fut fûmes fût fûtes ici il ils j je l la le les leur leurs lui m ma mais me mes moi mon même n ne nos notre nous on ont ou par pas pour qu que quel quelle quelles quels qui s sa sans se sera serai seraient serais serait seras serez seriez serions serons seront ses soi soient sois soit sommes son sont soyez soyons suis sur t ta te tes toi ton tu un une vos votre vous y à étaient étais était étant étiez étions été étée étées étés êtes",
);
const frStopWords = frStopWordsString.split(" ");

/**********/
/* Output */
/**********/
const output = document.querySelector("output");

if (!output) {
  throw new TypeError(
    `<output> element not found. Cannot display search results`,
  );
}

/**
 *
 * @param {JekyllNotice} notice
 * @returns
 */
const appendListItemForNotice = (notice) => {
  const resultEl = document.createElement("li");
  resultEl.classList.add("liste-notices__item");

  const cardEl = document.createElement("div");
  cardEl.classList.add("card", "h-100");

  const rowEl = document.createElement("div");
  rowEl.classList.add("row", "h-100", "no-gutters");

  const imageColEl = document.createElement("div");
  imageColEl.classList.add(
    "d-flex",
    "align-items-center",
    "justify-content-center",
    "col-lg-3",
  );

  const textColEl = document.createElement("div");
  textColEl.classList.add("col-lg-9");

  const cardBodyEl = document.createElement("div");
  cardBodyEl.classList.add("card-body", "h-100", "d-flex", "flex-column");

  const img = document.createElement("img");
  img.src =
    notice.logo && notice.logo.src ? `${baseUrl}${notice.logo.src}` : "data:,";
  img.classList.add("liste-notices__image");
  img.setAttribute("alt", `logo ${notice.nom}`);

  const resultA = document.createElement("a");
  resultA.classList.add("result-link");
  resultA.href = notice.url;
  resultA.append(img);

  const titleA = document.createElement("a");
  titleA.classList.add("result-link");
  titleA.href = notice.url;
  titleA.textContent = notice.nom;

  const title = document.createElement("h3");
  title.classList.add("card-title");
  title.classList.add("h4");
  title.append(titleA);

  const desc = document.createElement("div");
  desc.classList.add("card-text");
  desc.textContent = notice.description_courte;

  imageColEl.append(resultA);
  cardBodyEl.append(title, desc);
  textColEl.append(cardBodyEl);
  rowEl.append(imageColEl, textColEl);
  cardEl.append(rowEl);
  resultEl.append(cardEl);

  return resultEl.outerHTML;
};

/**
 *
 * @param {JekyllNotice[]} foundNotices
 * @returns {void}
 */
const printResults = (foundNotices) => {
  for (const notice of foundNotices) {
    const resultEl = document.createElement("a");
    resultEl.href = notice.url;
    resultEl.textContent = notice.nom;

    output.append(resultEl);
  }

  // @ts-ignore PPP : créer 2 types. Un avec mis_en_avant booléen, l'autre avec 'oui'|'non'
  const featuredNotices = foundNotices.filter(
    (notice) => notice.mis_en_avant === "oui",
  );
  // @ts-ignore
  const otherNotices = foundNotices.filter(
    (notice) => notice.mis_en_avant !== "oui",
  );

  output.innerHTML = `<h2 class="mb-5 text-center">Résultats (${foundNotices.length})</h2>`;

  if (featuredNotices.length > 0) {
    output.innerHTML += `
        <div class="mis-en-avant">
          <h3 class="mis-en-avant__titre">
            <em class="mis-en-avant__titre__encart">
              Conseillé par Framasoft
            </em>
          </h3>
          <ul class="liste-notices liste-notices--mis-en-avant">
            ${featuredNotices.map(appendListItemForNotice).join("")}
          </ul>
        </div>
      `;
  }

  output.innerHTML += `
      <ul class="liste-notices">
        ${otherNotices.map(appendListItemForNotice).join("")}
      </ul>
  `;
};

/**********/
/* Search */
/**********/

/**
 *
 * @param {JekyllNotice} notice
 * @returns { {nom: string, description_courte: string, tags: string, id: string, alternative_a: string} }
 */
// This processing is a workaround to a limitation of lunr-languages
// https://github.com/MihaiValentin/lunr-languages/issues/71
const makeIndexableNotice = (notice) => {
  const { nom, description_courte, tags, alternative_a, id } = notice;

  return {
    nom: removeAccents(nom),
    description_courte: removeAccents(description_courte),
    alternative_a: removeAccents(alternative_a),
    tags: [...tags].map(removeAccents).join(", "),
    id,
  };
};

/**
 *
 * @param {JekyllNotice[]} notices
 * @returns { Promise<{index: lunr.Index, notices: JekyllNotice[]}> }
 */
const createIndexFromNotices = (notices) => {
  const index = lunr(function () {
    // @ts-ignore
    this.use(lunr.fr);

    this.ref("id");
    this.field("tags", { boost: 10 });
    this.field("nom", { boost: 5 });
    this.field("alternative_a");
    this.field("description_courte");

    for (const notice of notices) {
      this.add(makeIndexableNotice(notice));
    }
  });

  return new Promise((resolve) => resolve({ index, notices }));
};

const indexWithNoticesP = noticesP.then(createIndexFromNotices);

/**
 *
 * @param {string} searchTerms
 * @returns {string}
 */
const refineSearchTermsWithPrefixes = (searchTerms) => {
  if (!welcomeTags.includes(searchTerms)) {
    return searchTerms;
  }

  const requiredSearchTerms = searchTerms
    .split(" ")
    .map((term) => {
      if (frStopWords.includes(term)) {
        return term;
      }
      return `+${term}`;
    })
    .join(" ");

  return requiredSearchTerms;
};

/**
 *
 * @param {string} searchTerms
 * @returns { (param: {index: lunr.Index, notices: JekyllNotice[]}) => JekyllNotice[] }
 */
const searchForNotices =
  (searchTerms) =>
  ({ index: searchIndex, notices }) => {
    const newSearchTerms = refineSearchTermsWithPrefixes(searchTerms);
    const lunrSearchResults = searchIndex.search(newSearchTerms);
    const foundNotices = lunrSearchResults.map(({ ref }) => {
      const foundNotice = notices.find((n) => n.id === ref);
      if (!foundNotice) {
        throw TypeError(`Il manque la notice avec l'id ${ref}`);
      }
      return foundNotice;
    });

    return foundNotices;
  };

const search = debounce((/** @type {Event} */ event) => {
  /** @type {HTMLInputElement | null} */
  // @ts-ignore
  const target = event.target;
  if (!target) {
    throw new TypeError("Missing event.target");
  }

  const searchTerms = removeAccents(target.value);

  if (searchTerms.trim() === "") {
    output.innerHTML = "";
    return;
  }

  indexWithNoticesP
    .then(searchForNotices(searchTerms))
    .then((foundNotices) => printResults(foundNotices));
});

const input = document.querySelector("input");
if (!input) {
  throw new TypeError("Élément <input> manquant");
}

input.addEventListener("input", search);
